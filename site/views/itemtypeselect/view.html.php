<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined( '_JEXEC' ) or die();

jimport( 'joomla.application.component.view');

class PagesAndItemsViewItemtypeselect extends JView{

	function display( $tpl = null ){
	
		//get backend language files
		$lang = JFactory::getLanguage();
		$lang->load('com_pagesanditems', JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR, null, false) || $lang->load('com_pagesanditems', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
		$path = PagesAndItemsHelper::getDirCSS();
		JHtml::stylesheet($path.'/pagesanditems3.css');
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
		parent::display($tpl);
	}
}

?>