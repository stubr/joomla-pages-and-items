<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die();

jimport('joomla.application.component.controller');

class pagesanditemsController extends JController{

	function display($cachable = false, $urlparams = false)
	{
		parent::display();
	}
	
	function __construct()
	{
		parent::__construct();
	}
}
?>