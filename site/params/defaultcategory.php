<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// No direct access
defined('_JEXEC') or die;

class JFormFieldDefaultcategory extends JFormField{

	var $type = 'defaultcategory';
	
	 protected function getInput() {		
				 
		$db = JFactory::getDBO();	
		$array_categories = array();		

		//get categories		
		$query = $db->getQuery(true);
		$query->select('id, title, level');
		$query->from('#__categories');
		$query->where('parent_id > 0');
		$query->where('extension='.$db->q('com_content'));				
		$query->order('lft');
		$rows = $db->setQuery($query);				
		$rows = $db->loadObjectList();	
				
		//build category select options
		$category_select_new = '<select name="'.$this->name.'">';
		$category_select_new .= '<option value="0">'.JText::_('JNO').' '.JText::_('JDEFAULT').'</option>';
		foreach($rows as $row){							
			$title = str_repeat('- ',$row->level).$row->title;
			$category_select_new .= '<option value="'.$row->id.'"';
			if($row->id == $this->value){
				$category_select_new .= ' selected="selected"';
			}
			$category_select_new .= '>'.$title.'</option>';
		}	
		$category_select_new .= '</select>';
			
		return $category_select_new;
	}	

}

?>