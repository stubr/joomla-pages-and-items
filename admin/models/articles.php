<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class pagesanditemsModelArticles extends JModelList{

	protected $option = 'com_pagesanditems';
	
	public function __construct($config = array()){
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(				
				'id', 'c.id',
				'title', 'c.title',
				'alias', 'c.alias',				
				'catid', 'd.catid', 'category_title',
				'state', 'c.state',
				'itemtype', 't.itemtype', 'itemtype',
				'access', 'c.access', 'access_level',
				'created', 'c.created',
				'created_by', 'c.created_by',
				'created_by_alias', 'c.created_by_alias',
				'ordering', 'c.ordering',
				'featured', 'c.featured',
				'language', 'c.language',
				'hits', 'c.hits'					
			);
		}
		parent::__construct($config);
	}		

	protected function populateState($ordering = NULL, $direction = NULL){
		
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.		
		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'items_search', '', 'string');					
		$this->setState('filter.search', $search);	
		
		// filter access
		$access = $app->getUserStateFromRequest($this->context.'.filter.access', 'filter_access', '0', 'int');					
		$this->setState('filter.access', $access);	
		
		// filter access
		$author = $app->getUserStateFromRequest($this->context.'.filter.author', 'filter_author', '0', 'int');					
		$this->setState('filter.author', $author);	
		
		// filter published
		$published = $app->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '');					
		$this->setState('filter.published', $published);	
		
		// filter category
		$category = $app->getUserStateFromRequest($this->context.'.filter.category', 'items_category_filter', '', 'int');					
		$this->setState('filter.category', $category);	

		// filter language
		$language = $app->getUserStateFromRequest($this->context.'.filter.language', 'filter_language', '');					
		$this->setState('filter.language', $language);	
		
		// filter type
		$type = $app->getUserStateFromRequest($this->context.'.filter.type', 'filter_type', '');					
		$this->setState('filter.type', $type);	
				
		parent::populateState('c.title', 'asc');
	}
	
	protected function getStoreId($id = ''){			
		
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.access');
		$id	.= ':'.$this->getState('filter.published');
		$id	.= ':'.$this->getState('filter.author');
		$id	.= ':'.$this->getState('filter.category');
		$id	.= ':'.$this->getState('filter.language');

		return parent::getStoreId($id);
	}
	
	protected function getListQuery(){
	
		// Create a new query object.
		$db	= $this->getDbo();
		$query = $db->getQuery(true);
		
		$this->update_item_index();
		
		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'c.* '				
			)
		);
		$query->from('`#__content` AS c');
		
		// join level titles
		$query->select('l.title AS leveltitle');	
		$query->join('LEFT', '#__viewlevels AS l ON c.access=l.id ');
		
		// Join over the asset groups.
		$query->select('ag.title AS access_level');
		$query->join('LEFT', '#__viewlevels AS ag ON ag.id = c.access');
		
		// Join over for type
		$query->select('t.itemtype AS itemtype');
		$query->join('LEFT', '#__pi_item_index AS t ON t.item_id = c.id');
		
		// join categories for category titles
		$query->select('d.title AS categorytitle');	
		$query->join('LEFT', '#__categories AS d ON d.id=c.catid ');
		
		// Join over the users for the author.
		$query->select('ua.name AS author_name');
		$query->join('LEFT', '#__users AS ua ON ua.id = c.created_by');
		
		// Join over the language
		$query->select('la.title AS language_title');
		$query->join('LEFT', '#__languages AS la ON la.lang_code = c.language');
		
		// Filter the items over the search string if set.
		if ($this->getState('filter.search') !== '') {
			// Escape the search token.
			$token	= $db->Quote('%'.$db->escape($this->getState('filter.search')).'%');
			$search_id = intval($this->getState('filter.search'));
			// Compile the different search clauses.
			$searches	= array();
			$searches[]	= 'c.title LIKE '.$token;
			$searches[]	= 'c.id = '.$search_id;	
			$searches[]	= 'ua.name LIKE '.$token;		

			// Add the clauses to the query.
			$query->where('('.implode(' OR ', $searches).')');
		}
		
		// filter access
		if($this->getState('filter.access')){	
			$query->where("(c.access='".$this->getState('filter.access')."')");
		}
		
		// Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			$query->where('c.state = ' . (int) $published);
		} else if ($published === '') {
			$query->where('(c.state = 0 OR c.state = 1)');
		}
		
		// filter categories
		if($this->getState('filter.category')){	
			$query->where("(c.catid='".$this->getState('filter.category')."')");
		}
		
		// Filter on the language.
		if ($language = $this->getState('filter.language')) {
			$query->where('c.language = '.$db->quote($language));
		}
		
		// Filter on author
		if ($author = $this->getState('filter.author')) {
			$query->where('c.created_by = '.$db->quote($author));
		}	
		
		// Filter on type
		if ($type = $this->getState('filter.type')) {
			$query->where('t.itemtype = '.$db->quote($type));
		}	
		
		$orderCol	= $this->state->get('list.ordering', 'a.title');		
		$orderDirn	= $this->state->get('list.direction', 'asc');
		if ($orderCol == 'c.ordering' || $orderCol == 'category_title') {
			$orderCol = 'd.title '.$orderDirn.', c.ordering';
		}
		
		if($orderCol == 'access_level'){
			$orderCol = 'ag.title';
		}
		if($orderCol == 'language'){
			$orderCol = 'la.title';
		}
		
		// Add the list ordering clause.		
		$query->order($db->escape($orderCol.' '.$orderDirn));
		
		//echo nl2br(str_replace('#__','jos_',$query));
		return $query;
		
	}
	
	function update_item_index(){
	
		$db = JFactory::getDBO();
		
		//get all article id's as array
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__content');		
		$article_ids = $db->setQuery($query);				
		$article_ids = $db->loadColumn(); //loadResultArray();
		
		//get all index item_id's as array
		$query = $db->getQuery(true);
		$query->select('item_id');
		$query->from('#__pi_item_index');		
		$index_ids = $db->setQuery($query);				
		$index_ids = $db->loadColumn(); //loadResultArray();		
		
		//loop content id's, if not in index yet, do insert as text	
		for($n = 0; $n < count($article_ids); $n++){
			if(!in_array($article_ids[$n], $index_ids)){				
				$query = $db->getQuery(true);
				$query->insert('#__pi_item_index');
				$query->set('item_id='.$article_ids[$n]);
				$query->set('itemtype='.$db->q('text'));
				$query->set('show_title=1');			
				$db->setQuery((string)$query);
				$db->query();
			}
		}	
	}
	
}
?>