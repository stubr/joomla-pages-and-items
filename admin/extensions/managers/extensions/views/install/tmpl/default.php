<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}
//$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_SITE,'',JPATH_COMPONENT_ADMINISTRATOR));

if ($this->showMessage)
{
	echo $this->loadTemplate('message');
}
//echo '<div id="page_content" class="width-100 fltlft">';
echo $this->loadTemplate('form');
//echo '</div>';
echo '<div class="width-100 fltlft">';
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
echo '</div>';
//
?>
