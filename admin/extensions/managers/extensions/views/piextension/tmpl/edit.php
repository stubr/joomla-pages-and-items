<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php

JHTML::_('behavior.tooltip');
// clean item data
JFilterOutput::objectHTMLSafe( $this->item, ENT_QUOTES, '' );
?>
<?php
	JHTML::_('behavior.tooltip');
	JHtml::_('behavior.formvalidation');
if(version_compare(JVERSION, '3', '>')){
	JHtml::_('formbehavior.chosen', 'select');
}
?>

<?php
	$tmpl = JFactory::getApplication()->input->get('tmpl', 0 ); 
?>

<script language="javascript" type="text/javascript">
	Joomla.submitbutton = function(pressbutton)
	{
		if (pressbutton == "piextension.cancel")
		{
			document.getElementById('extensionTask').value = pressbutton;
			document.adminForm.submit();
			return;
		}
		if (pressbutton == "piextension.save")
		{
			document.getElementById('extensionTask').value = pressbutton;
			document.adminForm.submit();
		}
		else if (pressbutton == "piextension.apply")
		{
			document.getElementById('extensionTask').value = pressbutton;
			document.adminForm.submit();
		}
	}
</script>
<div id="page_content">
<!-- begin id="form_content" need for css-->
<div id="form_content">
<form class="form-horizontal" action="index.php" method="post" name="adminForm" id="adminForm">
<?php

$hasTipType = JText::_('COM_PAGESANDITEMS_MANAGE_EXTENSIONS_TIP_'.strtoupper ($this->form->getValue('type')).'_1').'::'.JText::_('COM_PAGESANDITEMS_MANAGE_EXTENSIONS_TIP_'.strtoupper ($this->form->getValue('type')).'_2');
?>
<div class="col width-60" style=" float: left;">
	<fieldset class="adminform">
	<legend class="hasTip" title="<?php echo $hasTipType;?>"><?php echo JText::_('JDETAILS') ?>
	</legend>
		<?php if(version_compare(JVERSION, '3', '>=')) : ?>
		<div class="row-fluid">
			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('name'); ?>
				</div>
				<div class="controls">
					<?php $this->form->setFieldAttribute('name', 'type', 'hidden'); ?>
					<?php echo $this->form->getInput('name'); ?>
					<?php echo '<span class="readonly plg-name">'.JText::_($this->item->name).'</span>'; ?>
				</div>
			</div>
			<?php
			if( ($this->item->type == 'itemtype' && ($this->item->element == 'content' || $this->item->element == 'text')) || ($this->item->type == 'pagetype' && $this->item->version == 'integrated')  || ($this->item->type == 'manager' && $this->item->element == 'extensions' && $this->item->version == 'integrated') || $this->item->type == 'language' )
			{
				$this->form->setFieldAttribute('enabled', 'readonly','true');
			}
			?>
			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('enabled'); ?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('enabled'); ?>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('version'); ?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('version'); ?>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('ordering'); ?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('ordering'); ?>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('folder'); ?>
				</div>
				<div class="controls">
					<?php
					if($this->form->getValue('folder') != '')
					{
						$view = '';
						if(strpos($this->form->getValue('folder'), '_') !== false)
						{
							list($view,$type) = explode('_',$this->form->getValue('folder'));
						}
						$image = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'view_'.$view.'.png');
						if($image)
						{
							JHTML::_( 'behavior.modal' );
							$image = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',$image));
							$value = '<span class="editlinktip readonly plg-name"><a class="modal hasTip" title="'.JText::_('COM_PAGESANDITEMS_EXTENSION_FOLDER_TIP').'" href="'.$image.'" >';
								$value .= $this->form->getValue('folder');
							$value .= '</a></span>';
							echo $value;
							$this->form->setFieldAttribute('folder', 'type', 'hidden');
						}
					}
					else
					{
						$this->form->setFieldAttribute('folder', 'type', 'hidden');
						echo '<input type="text" readonly="readonly" size="20" class="readonly" value="'.JText::_('JOPTION_UNASSIGNED').'" >';
					}
					echo $this->form->getInput('folder');
				?>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('type'); ?>
				</div>
				<div class="controls">
					<?php
					$value = '<span class="hasTip editlinktip readonly plg-name" title="'.$hasTipType.'" href="#">';
					$value .= $this->form->getValue('type');
					$value .= '</span>';
					echo $value;
					$this->form->setFieldAttribute('type', 'type', 'hidden');
					echo $this->form->getInput('type');
				?>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('element'); ?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('element'); ?>
				</div>
			</div>

			<!--<div class="control-group">
				<div class="control-label"> -->
					<?php echo $this->form->getLabel('access'); ?>
			<!--	</div>
				<div class="controls"> -->
					<?php echo $this->form->getInput('access'); ?>
			<!--	</div>
			</div>
			
			<div class="control-group">
				<div class="control-label"> -->
					<?php echo $this->form->getLabel('ordering'); ?>
				<!--</div>
				<div class="controls"> -->
					<?php echo $this->form->getInput('ordering'); ?>
			<!--	</div>
			</div>

			<div class="control-group">
				<div class="control-label"> -->
					<?php echo $this->form->getLabel('state'); ?>
			<!--	</div>
				<div class="controls"> -->
					<?php echo $this->form->getInput('state'); ?>
			<!--	</div>
			</div> -->

			<?php if ($this->item->extension_id) : ?>
			<div class="control-group">
				<div class="control-label">
					<?php echo $this->form->getLabel('extension_id'); ?>
				</div>
				<div class="controls">
					<?php echo $this->form->getInput('extension_id'); ?>
				</div>
			</div>
			<?php endif; ?>
			
			<!-- Plugin metadata -->
			<?php if ($this->item->xml) : ?>
			<div class="control-group">
				<div class="control-label">
				<?php if ($text = trim($this->item->xml->description)) : ?>
					<label id="jform_extdescription-lbl">
						<?php echo JText::_('JGLOBAL_DESCRIPTION'); ?>
						</label>
						<!-- <div class="clr"></div> -->
				</div>
				<div class="controls">
					<span class="readonly plg-desc"><?php echo JText::_($text); ?></span>
				</div>
				<?php endif; ?>
			</div>

			<?php else : ?>
				<?php if ($this->item->type != 'language') : ?>
					<div class="control-group">
					<?php echo JText::_('COM_PLUGINS_XML_ERR'); ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<!-- version_compare else -->
		<?php else : ?>
			<ul class="adminformlist">
			<li>
			<?php
				echo $this->form->getLabel('name');
				$this->form->setFieldAttribute('name', 'type', 'hidden');
				echo $this->form->getInput('name');
				echo '<span class="readonly plg-name">'.JText::_($this->item->name).'</span>';
			?>
			</li>

			<?php
			if( ($this->item->type == 'itemtype' && ($this->item->element == 'content' || $this->item->element == 'text')) || ($this->item->type == 'pagetype' && $this->item->version == 'integrated')  || ($this->item->type == 'manager' && $this->item->element == 'extensions' && $this->item->version == 'integrated') || $this->item->type == 'language' )
			{
				$this->form->setFieldAttribute('enabled', 'readonly','true');
			}
			?>

			<li><?php echo $this->form->getLabel('enabled'); ?>
			<?php echo $this->form->getInput('enabled'); ?></li>

			<li><?php echo $this->form->getLabel('version'); ?>
			<?php echo $this->form->getInput('version'); ?></li>

			<li><?php echo $this->form->getLabel('ordering'); ?>
			<?php echo $this->form->getInput('ordering'); ?></li>

			<li>
				<?php echo $this->form->getLabel('folder'); ?>
				<?php
					if($this->form->getValue('folder') != '')
					{
						$view = '';
						if(strpos($this->form->getValue('folder'), '_') !== false)
						{
							list($view,$type) = explode('_',$this->form->getValue('folder'));
						}
						$image = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'view_'.$view.'.png');
						if($image)
						{
							JHTML::_( 'behavior.modal' );
							$image = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',$image));
							$value = '<span class="editlinktip readonly plg-name"><a class="modal hasTip" title="'.JText::_('COM_PAGESANDITEMS_EXTENSION_FOLDER_TIP').'" href="'.$image.'" >';
								$value .= $this->form->getValue('folder');
							$value .= '</a></span>';
							echo $value;
							$this->form->setFieldAttribute('folder', 'type', 'hidden');
						}
					}
					else
					{
						echo '<input type="text" readonly="readonly" size="20" class="readonly" value="'.JText::_('JOPTION_UNASSIGNED').'" >';
					}
					echo $this->form->getInput('folder');
				?>
			</li>

			<li>
				<?php
					echo $this->form->getLabel('type');
				?>
				<?php
					$value = '<span class="hasTip editlinktip readonly plg-name" title="'.$hasTipType.'" href="#">';
					$value .= $this->form->getValue('type');
					$value .= '</span>';
					echo $value;
					$this->form->setFieldAttribute('type', 'type', 'hidden');
					echo $this->form->getInput('type');
				?>
			</li>

			<li><?php echo $this->form->getLabel('element'); ?>
			<?php echo $this->form->getInput('element'); ?></li>

			<li><?php echo $this->form->getLabel('access'); ?>
			<?php echo $this->form->getInput('access'); ?></li>

			<li><?php echo $this->form->getLabel('ordering'); ?>
			<?php echo $this->form->getInput('ordering'); ?></li>

			<li><?php echo $this->form->getLabel('state'); ?>
			<?php echo $this->form->getInput('state'); ?></li>


			<?php if ($this->item->extension_id) : ?>
				<li><?php echo $this->form->getLabel('extension_id'); ?>
				<?php echo $this->form->getInput('extension_id'); ?></li>
			<?php endif; ?>
			</ul>
			<!-- Plugin metadata -->
			<?php if ($this->item->xml) : ?>
				<?php if ($text = trim($this->item->xml->description)) : ?>
					<label id="jform_extdescription-lbl">
						<?php echo JText::_('JGLOBAL_DESCRIPTION'); ?>
						</label>
						<div class="clr"></div>
						<span class="readonly plg-desc"><?php echo JText::_($text); ?></span>

				<?php endif; ?>
			<?php else : ?>
				<?php if ($this->item->type != 'language') : ?>
					<?php echo JText::_('COM_PLUGINS_XML_ERR'); ?>
				<?php endif; ?>
			<?php endif; ?>

		<!-- version_compare else end-->
		<?php endif; ?>
	</fieldset>
</div>


<div class="col width-40" style=" float: right;">
<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<fieldset class="adminform3options">
<?php endif; ?>
	<?php echo JHtml::_('sliders.start','plugin-sliders-'.$this->item->extension_id); ?>
	<?php echo $this->loadTemplate('options'); ?>
	<?php echo JHtml::_('sliders.end'); ?>
<?php if(version_compare(JVERSION, '3', '>=')) : ?>
</fieldset>
<?php endif; ?>
</div>

<div class="clr"></div>

	<input type="hidden" name="option" value="com_pagesanditems" />
	<input type="hidden" id="task" name="task" value="extension.doExecute" />
	<input type="hidden" id="extensionName" name="extensionName" value="extensions" />
	<input type="hidden" id="extensionTask" name="extensionTask" value="display" />
	<input type="hidden" id="extensionType" name="extensionType" value="manager" />
	<input type="hidden" id="extensionFolder" name="extensionFolder" value="" />
	<input type="hidden" id="view" name="view" value="piextension" />

	<input type="hidden" name="id" value="<?php echo $this->item->extension_id; ?>" />
	<input type="hidden" name="extension_id" value="<?php echo $this->item->extension_id; ?>" />
	<input type="hidden" name="type" id="type" value="<?php echo $this->item->type; ?>" />
	<input type="hidden" name="folder" id="folder" value="<?php echo $this->item->folder; ?>" />

	<input type="hidden" name="cid[]" value="<?php echo $this->item->extension_id; ?>" />
	<input type="hidden" name="client" value="<?php echo $this->item->client_id; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<!-- end id="form_content" need for css-->
</div>
<!-- end id="page_content"-->
</div>
<?php
if(!$tmpl)
{
	echo '<div class="width-100 fltlft">';
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
	echo '</div>';
}
?>
