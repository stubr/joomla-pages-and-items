<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}

require_once(dirname(__FILE__).'/../../../includes/extensions/manager.php');

/**
*********************************
* Manager Extensions           *
*********************************
*/
class PagesAndItemsExtensionManagerExtensions extends PagesAndItemsExtensionManager
{

	/**
	@param $name string
	@param $type string
	*/
	function XonToolbarButton($name,$type)
	{
		if($name != 'extensions')
		{
			return false;
		}

		return true;
	}

	function onGetManager(&$managers)
	{
		$link = 'index.php?option=com_pagesanditems';
		$link .= '&task=manager.doExecute'; //display';
		$link .= '&extensionName=extensions'; //the name
		$link .= '&extensionType=manager'; //the type
		$link .= '&extensionFolder='; //the folder
		$link .= '&extensionTask=display';
		$link .= '&view=install'; //
		//$link .= '&view=install';
		$manager = new StdClass;
		$manager->link = $link;
		$manager->tooltip = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_INSTALL_NAME');
		$manager->text = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_INSTALL_NAME');
		$manager->alt = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_INSTALL_NAME');
		$path = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).DIRECTORY_SEPARATOR)));
		$manager->image = $path.'/media/images/icon-48-extension_install.png';
		$managers[] = $manager;
		$manager = null;

		/*

		*/

		$link = 'index.php?option=com_pagesanditems';
		$link .= '&task=manager.doExecute'; //display';
		$link .= '&extensionName=extensions'; //the name
		$link .= '&extensionType=manager'; //the type
		$link .= '&extensionFolder='; //the folder
		$link .= '&extensionTask=display';
		$link .= '&view=piextensions'; //		
		$manager = new StdClass;
		$manager->link = $link;
		$manager->tooltip = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_EXTENSIONS_NAME');
		$manager->text = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_EXTENSIONS_NAME');
		$manager->alt = JText::_('PI_EXTENSION_MANAGER_EXTENSIONS_VIEW_EXTENSIONS_NAME');
		$path = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).DIRECTORY_SEPARATOR)));
		$manager->image = $path.'/media/images/icon-48-extension_edit.png';
		$managers[] = $manager;
		
		return true;
	}

	function XonDisplayContent(&$content,$extension,$sub_task) //,$model)
	{
		//here we set also Toolbar?

		$content->text = 'extensions';
		return true;
	}

	function XonGetModelName(&$models)
	{

		jimport( 'joomla.application.component.model' );
		$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
		self::addModelPath($path,'pagesanditemsModel'); //JModel::addIncludePath($path);
		$models[] = 'managerextensions';
		return true;
	}
}

?>