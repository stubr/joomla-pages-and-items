<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.controller' );
jimport('joomla.environment.response');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'controller.php');
/**
 *menuitemtypeselect Controller
 *
 */


class PagesAndItemsControllerExtensionManagerExtensions extends PagesAndItemsController //JController
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Display the view
	 */
	function display($cachable = false, $urlparams = false)
	{
		$modelName = array();
		$vName = strtolower(JFactory::getApplication()->input->get('view', 'piextensions'));

		switch ($vName)
		{
			case 'manage':
				$vLayout = 'default';
				jimport( 'joomla.application.component.model' );
				$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
				self::addModelPath($path,'pagesanditemsModel'); //JModelLegacy::addIncludePath($path);
				//$modelName[] = 'manage';
				$modelName[] = 'manage';
			break;
			case 'uninstall':
				$vLayout = 'default';
				jimport( 'joomla.application.component.model' );
				$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
				self::addModelPath($path,'pagesanditemsModel'); //JModelLegacy::addIncludePath($path);
				//$modelName[] = 'manage';
				$modelName[] = 'uninstall';
			break;
			case 'piextensions':
				$vLayout = 'default';
				jimport( 'joomla.application.component.model' );
				$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
				self::addModelPath($path,'pagesanditemsModel'); //JModelLegacy::addIncludePath($path);
				$modelName[] = 'piextensions';
				//$modelName[] = 'ExtensionManageExtensionsextensions';
			break;

			case 'piextension':
				$vLayout = 'edit';
				jimport( 'joomla.application.component.model' );
				$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
				self::addModelPath($path,'pagesanditemsModel'); //JModelLegacy::addIncludePath($path);
				$modelName[] = 'piextension';
				//$modelName[] = 'ExtensionManageExtensionsextension';
			break;

			case 'install':
				$vLayout = 'default';
				jimport( 'joomla.application.component.model' );
				$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
				self::addModelPath($path,'pagesanditemsModel'); //JModelLegacy::addIncludePath($path);
				$modelName[] = 'install';
				//$modelName[] = 'ExtensionManageExtensionsInstall';
			break;
		}
		$document = JFactory::getDocument();
		$vType = $document->getType();

		$this->addViewPath(realpath(dirname(__FILE__).'/views'));

		// Get/Create the view
		$view =$this->getView( $vName, $vType);

		$view->addTemplatePath(realpath(dirname(__FILE__).'/views'.DIRECTORY_SEPARATOR.$vName.DIRECTORY_SEPARATOR.'tmpl'));
		// Set the layout
		$view->setLayout($vLayout);
		// Display the view
		if(is_array($modelName))
		{
			for($mn = 0; $mn < count($modelName); $mn++)
			{
				//if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModel'))
				//if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModelExtensionManageExtensions'))

				//if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModel'))
				//if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModelExtensionManageExtensions'))
				//if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModel'))
				if($model[$mn] = $this->getModel($modelName[$mn],'PagesAndItemsModel'))
				{
					// Push the model into the view (as default)
					$view->setModel($model[$mn], true);
				}

			}
		}
		parent::display();
		//$view->display();
	}
}
