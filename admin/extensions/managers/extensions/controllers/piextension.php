<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// No direct access.
defined('_JEXEC') or die;
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controller.php');
/**
 * @package		PagesAndItems
*/

class PagesAndItemsControllerExtensionManagerExtensionsPiextension extends PagesAndItemsController
{
	/**
	 * Constructor.
	 *
	 * @param	array An optional associative array of configuration settings.
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		$task	= JFactory::getApplication()->input->get('task'); 

		$this->registerTask( 'apply', 'save');
	}

	function cancel()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );
		$client  = JFactory::getApplication()->input->get( 'filter_client', 'site', 'word' );
		$db = JFactory::getDBO();
		JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'tables');
		$row = JTable::getInstance('piextension','PagesAndItemsTable');
		
		$row->bind(JRequest::get('post'));
		$row->checkin();
		
		$app = JFactory::getApplication();
		$option = JFactory::getApplication()->input->get('option'); 
		$refer = $app->getUserState( $option.'.refer');
		if($refer != '' && $refer)
		{
			$url = 'index.php?option=com_pagesanditems'.$refer;
		}
		else
		{
			$url =  			'index.php?option=com_pagesanditems&task=manager.doExecute&extensionName=extensions&extensionType=manager&extensionFolder=&extensionTask=display&view=piextensions';
		}
		$this->setRedirect( $url );
	}




	function save()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );
		$db = JFactory::getDBO();
		JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'tables');
		$row = JTable::getInstance('piextension','PagesAndItemsTable');

		$task = JFactory::getApplication()->input->get('extensionTask','save'); 
		$client = JFactory::getApplication()->input->get( 'filter_client', 'site', 'word' );

		//$data = JFactory::getApplication()->input->get('jform', array(), 'array'); 
		$post  = JFactory::getApplication()->input->post->get('jform', array(), 'array');
		/*
		if($data && count($data))
		{
			$post = $data;
		}
		else
		{
			//$post = JRequest::get('post');
			$post = JFactory::getApplication()->input->getArray($_POST);
		}
		*/
		if (!$row->bind($post))
		{
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}

		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}
		$row->checkin();

		if ($client == 'admin') {
			$where = "client_id=1";
		} else {
			$where = "client_id=0";
		}

		$row->reorder( 'type = '.$db->Quote($row->type).' AND folder = '.$db->Quote($row->folder).' AND ordering > -10000 AND ordering < 10000 AND ( '.$where.' )' );
		/*
		we will trigger onAfterParamsSave
		*/
		$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..');
		require_once($path.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.$row->type.'helper.php');

		$extensionHelper = 'Extension'.ucfirst($row->type).'Helper';
		$extensionHelper = new $extensionHelper();
		$extensionHelper->importExtension($row->folder, $row->element,true,null,true);

		$dispatcher =JDispatcher::getInstance();
		$dispatcher->trigger('onAfterParamsSave',array(&$row->params,$row->element));
		switch ( $task )
		{
			case 'apply':
				$msg = JText::sprintf( 'PI_EXTENSION_MANAGER_EXTENSIONS_APPLY_EXTENSION', $row->name );
				$this->setRedirect( 'index.php?option=com_pagesanditems&task=manager.doExecute&extensionName=extensions&extensionFolder=&extensionType=manager&extensionTask=display&view=piextension&client='. $client .'&sub_task=edit&cid[]='. $row->extension_id.'&layout=edit&extension_id='. $row->extension_id, $msg );
				break;

			case 'save':
			default:
				$msg = JText::sprintf( 'PI_EXTENSION_MANAGER_EXTENSIONS_SAVE_EXTENSION', $row->name );
				$app = JFactory::getApplication();
				$option = JFactory::getApplication()->input->get('option'); 
				$refer = $app->getUserState( $option.'.refer');
				if($refer != '' && $refer)
				{
					$url = 'index.php?option=com_pagesanditems'.$refer;
				}
				else
				{
					$url =  'index.php?option=com_pagesanditems&task=manager.doExecute&extensionName=extensions&extensionFolder=&extensionType=manager&extensionTask=display&view=piextensions';
				}
				$this->setRedirect( $url , $msg );
				break;
		}
	}
}