<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}

require_once(dirname(__FILE__).'/../../../includes/extensions/manager.php');

/**
*********************************
* Manager Archive           *
*********************************
*/
class PagesAndItemsExtensionManagerArchiveTrash extends PagesAndItemsExtensionManager
{

	/**
	@param $name string
	@param $type string
	*/
	function onToolbarButton($name,$type)
	{
		if($name != 'archivetrash')
		{
			return false;
		}

		return true;
	}

	function onGetManager(&$managers)
	{
		$link = 'index.php?option=com_pagesanditems';
		$link .= '&task=manager.doExecute'; //display';
		$link .= '&extensionName=archivetrash'; //the name
		$link .= '&extensionType=manager'; //the type
		$link .= '&extensionFolder='; //the folder
		$link .= '&extensionSubTask=display';
		$link .= '&view=archivetrash'; //

		$manager = new StdClass;
		$manager->link = $link;
		$manager->tooltip = JText::_('PI_EXTENSION_MANAGER_ARCHIVETRASH_NAME');
		$manager->text = JText::_('PI_EXTENSION_MANAGER_ARCHIVETRASH_NAME');
		$manager->alt = JText::_('PI_EXTENSION_MANAGER_ARCHIVETRASH_NAME');
		//$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,JUri::root(),realpath(dirname(__FILE__).DIRECTORY_SEPARATOR)));
		//$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_BASE.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).DIRECTORY_SEPARATOR)));
		$path = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).DIRECTORY_SEPARATOR)));
		$manager->image = $path.'/media/images/icon-48-archivetrash.png';
		//$lang =
		$managers[] = $manager;
		return true;
	}

	function onDisplayContent(&$content,$extension,$sub_task) //,$model)
	{
		//here we set also Toolbar?

		$content->text = 'archivetrash';
		return true;
	}

	//????
	function onGetModelName(&$models)
	{
		jimport( 'joomla.application.component.model' );
		$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'models');
		self::addModelPath($path,'pagesanditemsModel'); //JModel::addIncludePath($path);
		$models[] = 'archivetrash';
		return true;
	}
	/*
	jimport( 'joomla.application.component.model' );
		JModel::addIncludePath($path);
	*/

}

?>