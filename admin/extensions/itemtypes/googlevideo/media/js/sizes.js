/**
* @package 
* @version 1.5.0
* @copyright Copyright (C) 2009-2010 Michael Struller. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author http://gecko.struller.de
*/


function newDefault(id,nr)
{
	alert(id+nr);
	default_value = document.getElementById(id+nr).checked ? 1 : 0;
	sizes_value = document.getElementById(id+nr).value;
	//for (k = 0; k < counter; k++)
	for (k = 1; k <= counter; k++)
	{
		document.getElementById(id+k).checked = 0;
		document.getElementById(id+k).value = 0;
	}
	
	document.getElementById(id+nr).checked = default_value;
	document.getElementById(id+nr).value = default_value;
}
