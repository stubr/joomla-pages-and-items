<?php
/**
* @version		2.1.1
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2012 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}

function itemtype_install()
{
	//get database

	$database = JFactory::getDBO();

	//create table for subitem_googlevideo
		$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_subitem_googlevideo (
	  `id` int(11) NOT NULL auto_increment,
	  `item_id` int(11) NOT NULL,
	 `title` varchar(255) NOT NULL,
	  `url` tinytext NOT NULL,
	  `size` tinytext NOT NULL,
	  PRIMARY KEY  (`id`)
	)");
		$database->query();


	//check if title in table
	$table = '#__pi_subitem_googlevideo ';
	$query = "show columns from $table ";
	$database->setQuery( $query );
	$database->query();
	$columns = $database->loadObjectList();
	$title_exists=false;
	if($columns )
	{
		foreach($columns as $column)
		{
			if($column->Field == 'title')
			{
				$title_exists=true;
			}
		}
		if(!$title_exists)
		{
			$title = 'title';
			$query = "ALTER table $table ADD `title` varchar(255) NOT NULL";
			$database->setQuery( $query );
			$database->query();
			/*
			//here we will add the title in tho row
			$query = "SELECT * FROM $table ";
			$database->setQuery($query);
			$rows = $database->loadObjectList();
			if(count($rows))
			{
				foreach($rows as $row)
				{
					$database->setQuery( "SELECT title FROM  #__content WHERE id = '".$row->item_id."'" );
					$database->query();
					$content = $database->loadObject();
					if($content)
					{
						$database->setQuery( "UPDATE ".$table." SET title = '".$content->title."' WHERE id = '".$row->id."'" );
						$database->query();
					}
				}
			}
			*/
		}
	}

}
?>

