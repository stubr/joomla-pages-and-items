<?php
/**
* @version		2.1.1
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2012 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}
/**
*/
require_once(dirname(__FILE__).'/../../../includes/extensions/itemtype.php');
/**
* @package		PagesAndItems
*/

class PagesAndItemsExtensionItemtypeGooglevideo extends PagesAndItemsExtensionItemtype
{

	/*
	public function __construct(&$subject, $config = array())
	{
		parent::__construct($subject, $config);
	}
	*/
	function onGetTables(&$tables,$item_type,$item_id)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}

		//tables are:
		$table->name = 'content';
		$table->reference_id = 'id';
		$table->reference_id_value = $item_id;
		$tables[] = $table;
		$table = null;

		$table->name = 'pi_subitem_googlevideo';
		$table->reference_id = 'item_id';
		$table->reference_id_value = $item_id;
		$tables[] = $table;
		$table = null;

		return true;
	}

	function onItemtypeDisplay_config_form(&$itemtypeHtmlIsConfig,$item_type)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}

		$html = true; 
		$itemtypeHtmlIsConfig->text = $html;
		return true;
	}

	function XonItemtypeDisplay_config_form(&$itemtypeHtml,$item_type)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}
		/*
		here we get the params from the itemtype
		*/
		$html = $this->renderParams();
		/*
		if we need more for the config we can get it here
		*/

		$itemtypeHtml->text = $html;
		return true;
	}
	function XonItemtypeConfig_save(&$msg,$item_type)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}
		/*
			we use params so no extra save here need
		*/
		$msg = $this->saveParams();


		return true;
	}

	function onGetPluginName(&$itemtypeHtml,$item_type)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}
		$html = '';
		$html = JText::_('PI_EXTENSION_ITEMTYPE_GOOGLEVIDEO_NAME');
		$itemtypeHtml = $html;
		return true;
	}

	function onItemtypeDisplay_item_frontend(&$itemtypeHtml,$item_type,$item_id,$row)
	{
		//in old PI this was the file item_frontend.php
		if($item_type != 'googlevideo')
		{
			return false;
		}

		//get data from sub_item
		$this->db->setQuery("SELECT url, size FROM #__pi_subitem_googlevideo WHERE item_id='$item_id' LIMIT 1");
		$rows = $this->db->loadObjectList();
		$sub_item_row = $rows[0];
		$sub_item_url = trim($sub_item_row->url);
		$sub_item_size = trim($sub_item_row->size);

		//get videoid out of url
		$pos_docid = strpos($sub_item_url, 'docid=');
		$string_lenght = strlen($sub_item_url);
		$temp = substr($sub_item_url, $pos_docid+6, $string_lenght);
		$pos_and = strpos($temp, '&');
		if(!$pos_and)
		{
			$pos_and = strlen($temp);
		}
		$video_id = substr($sub_item_url, $pos_docid+6, $pos_and);

		//get sizes
		$sizes = explode('x',$sub_item_size);
		$width = $sizes[0];
		$height = $sizes[1];

		//only output when there is a video
		$googlevideo_code = false;
		if($video_id)
		{
			$googlevideo_code = '<object class="embed" width="'.$width.'" height="'.$height.'" type="application/x-shockwave-flash" data="http://video.google.com/googleplayer.swf?docId='.$video_id.'"><param name="movie" value="http://video.google.com/googleplayer.swf?docId='.$video_id.'" /><param name="wmode" value="transparent"><em>You need to have flashplayer enabled to watch this Google video</em></object>';
		}

		$itemtypeHtml->text = $itemtypeHtml->text.$googlevideo_code;


		return true;
	}

	function onItemtypeGetSubitemId(&$subitemId,$item_type,$item_id)
	{
		//in old PI this was the file admin/item.php
		if($item_type != 'googlevideo')
		{
			return true;
		}
		//get data from sub_item
		$this->db->setQuery("SELECT * FROM #__pi_subitem_googlevideo WHERE item_id='$item_id' LIMIT 1");
		$row = $this->db->loadObject();
		$subitemId = $row->id;
		return true;
	}

	function onItemtypeDisplay_item_edit(&$itemtypeHtml,$item_type,$item_id,$text,$itemIntroText,$itemFullText)
	{
		//in old PI this was the file admin/item.php
		if($item_type != 'googlevideo')
		{
			return false;
		}
		$html = '';
		//get data from sub_item
		$this->db->setQuery("SELECT * FROM #__pi_subitem_googlevideo WHERE item_id='$item_id' LIMIT 1");
		$rows = $this->db->loadObjectList();
		if($rows)
		{
			$row = $rows[0];
		}
		//check if item is restored
		$restored_item = false;
		$sub_task= JRequest::getVar('sub_task');
		if($sub_task=='edit' && $row->id=='')
		{
			//previously deleted item which has been restored. subitem-data has been deleted so start like new item
			$restored_item = true;
		}

		//switch for new and edit
		if($sub_task=='new' || $restored_item)
		{
			//begin getting data for new item
			$sub_item_id = '';
			$url = '';
			$size = '';
			//end getting data for new item
		}
		else
		{
			//begin getting data to edit item
			$sub_item_id = $row->id;
			$url = stripslashes($row->url);
			$size = $row->size;
			//end getting data to edit item
		}

		$html .= '<input type="hidden" name="sub_item_id" value="'.$sub_item_id.'" />';
		$html .= '<div class="pi_form_wrapper">';
			$html .= '<div class="left">';
				$html .= '<span class="editlinktip hasTip" title="'.JText::_('PI_EXTENSION_ITEMTYPE_GOOGLEVIDEO_URL').'::'.JText::_('PI_EXTENSION_ITEMTYPE_GOOGLEVIDEO_URL_TIP').'">';
				$html .= JText::_('PI_EXTENSION_ITEMTYPE_GOOGLEVIDEO_URL').': ';
				$html .= '<img src="components/com_pagesanditems/images/star.gif" alt="required field" />';
				$html .= '</span>';
			$html .= '</div>';
			$html .= '<div class="right">';
				$html .= '<input type="text" value="'.$url.'" name="url" style="width: 400px;" id="google_video_url" />';
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="pi_form_wrapper">';
			$html .= '<div class="left">';
				$html .= JText::_('PI_EXTENSION_ITEMTYPE_GOOGLEVIDEO_SIZE').': ';
			$html .= '</div>';
			$html .= '<div class="right">';
				$html .= '<select name="size">';

				$sizes = $this->params->get('sizes');
				$sizes = is_string($sizes) ? json_decode(str_replace('\'','"',$sizes)) : (is_array($sizes) ? json_decode(json_encode($sizes)) : $sizes);
				foreach($sizes as $row)
				{
					$size_label = JText::_($row->name);
					
					$size_value = $row->value;
					
					$html .= '<option value="';
					$html .= $size_value;
					$html .= '"';
					if(($sub_task=='new' && (isset($row->default_value) && $size_value == $row->default_value)) || ($sub_task=='edit' && $size_value == $size))
					{
						$html .= 'selected="selected"';
					}
					$html .= '>';
					$html .= $size_label.' '.$size_value;
					$html .= '</option>';
				}
				$html .= '</select>';
			$html .= '</div>';
		$html .= '</div>';

		$itemtypeHtml->text = $itemtypeHtml->text.$html;
		return true;
	}

	//function item_save($item_id)
	function onItemtypeItemSave($item_type, $delete_item, $item_id,$new_or_edit)
	{
		if($item_type != 'googlevideo')
		{
			return false;
		}

		if($delete_item)
		{
			//make an redirect to the controller?
			$this->item_delete($item_id);
			return true;
		}

		//get vars
		$sub_item_id = intval($this->get_var('sub_item_id', ''));
		$url = $this->get_var('url', '', 'post');
		$size = $this->get_var('size', '', 'post');

		if($sub_item_id=='')
		{
			//if new item
			$this->db->setQuery( "INSERT INTO #__pi_subitem_googlevideo SET item_id='$item_id', url='$url', size='$size'");
			if (!$this->db->query())
			{
				echo "<script> alert('".$this->db->getErrorMsg()."'); window.history.go(-1); </script>";
				exit();
			}

			//end if new item
		}else
		{
			//start update item
			$this->db->setQuery( "UPDATE #__pi_subitem_googlevideo SET url='$url', size='$size' WHERE id='$sub_item_id'" );
			if (!$this->db->query())
			{
				echo "<script> alert('".$this->db->getErrorMsg()."'); window.history.go(-1); </script>";
				exit();
			}
			//end update item
		}
		return true;
	}

	function item_delete($item_id)
	{
		//delete sub-item
		$this->db->setQuery("DELETE FROM #__pi_subitem_googlevideo WHERE item_id='$item_id'");
		if (!$this->db->query())
		{
			echo "<script> alert('".$this->db -> getErrorMsg()."'); window.history.go(-1); </script>";
		}
	}
}

?>
