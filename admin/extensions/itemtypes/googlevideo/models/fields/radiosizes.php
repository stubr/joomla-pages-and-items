<?php
/**
* @version		2.1.1
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2012 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
 */

//defined('JPATH_BASE') or die;
defined('_JEXEC') or die;


/**
 * Supports an HTML select list of plugins
 *
 * @package		Joomla.Administrator
 * @subpackage	com_newsfeeds
 * @since		1.6
 */
jimport('joomla.html.html');
jimport('joomla.form.formfield');
//jimport('joomla.form.fields.radio');

class JFormFieldRadiosizes extends JFormField //JFormFieldRadio //JFormField //JElementSpacerjtext extends JElement
{
	protected $type = 'Radiosizes';


	protected function getLabel()
	{
		// Initialise variables.
		$label = '';

		if ($this->hidden) 
		{
			return $label;
		}

		// Get the label text from the XML element, defaulting to the element name.
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;

		// Build the class for the label.
		$class = !empty($this->description) ? 'hasTip' : '';
		$class = $this->required == true ? $class.' required' : $class;
		$class = $this->element['class'] ? $class.' '.(string) $this->element['class'] : $class;
		$style = $this->element['style'] ? 'style="'.(string) $this->element['style'].'"' : '';

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="'.$this->id.'-lbl" for="'.$this->id.'" class="'.$class.'" '.$style;

		// If a description is specified, use it to build a tooltip.
		if (!empty($this->description)) {
			$label .= ' title="'.htmlspecialchars(trim($text, ':').'::' .
						($this->translateDescription ? JText::_($this->description) : $this->description), ENT_COMPAT, 'UTF-8').'"';
		}

		// Add the label text and closing tag.
		if ($this->required) {
			$label .= '>'.$text.'<span class="star">&#160;*</span></label>';
		} else {
			$label .= '>'.$text.'</label>';
		}

		return $label;
	
	
		return;
	}

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since
	 */
	protected function getInput()
	{
		// Initialize some field attributes.
		$attr = $this->element['class'] ? ' class="'.(string) $this->element['class'].'"' : '';
		$attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
		$attr .= $this->element['size'] ? ' size="'.(int) $this->element['size'].'"' : '';


		$sizes = array ();
		
		$old = false;
		if(!is_array($this->value) && is_array(json_decode(str_replace('\'','"',$this->value))))
		{
			//in the array we have objects
			$old = true;
		}
		
		$sizes = is_string($this->value) ? json_decode(str_replace('\'','"',$this->value),true) : (is_object($this->value) ? json_decode(json_encode($this->value),true) : (is_array($this->value) ? json_decode(json_encode($this->value),true) : $this->value));
		// Get the field options.
		$options = $this->getOptions();
		if(!$sizes)
		{
			// Build the radio field output.
			foreach ($options as $key => $option)
			{
				$_value = $option->value;
				$_name = $option->name;
				$_default_value = $option->default_value;
				$obj = array(); //$obj = new stdClass;
				$obj['value'] = $_value;
				$obj['default_value'] = $_default_value;
				$obj['name'] = trim( $_name ) ? $_name : $_value;
				//$sizes[$_name] = $obj;
				$sizes[] = $obj;
			}
		}
		elseif($old)
		{
			//need for backwards
			foreach ($sizes as $key => $option)
			{
				//$option is object
				$_value = $option['value']; //$option->value; //
				$_name = $option['name']; //$option->name; //
				$_default_value = isset($option['default_value']) ? $option['default_value'] : 0; //isset($option->default_value) ? 1 : 0; //
				$obj = array(); //new stdClass;
				$obj['default_value'] = $_default_value;
				$obj['value'] = $_value;
				$obj['name'] = trim( $_name ) ? $_name : $_value;
				//unset($sizes[$key]);
				//$sizes[$_name] = $obj;
				$sizes[$key] = $obj;
			}
		}
		
		//$value = str_replace('"','\'',json_encode($sizes));
		//here return to array->object
		/*foreach ($options as $option)
		{
			$_value = $option->value;
			$_name = $option->name;
			$_default_value = $option->default_value;
			$_displayName = JText::_($option->data);
			$obj = new stdClass;
			$obj->displayName = $_displayName;
			$obj->default_value = isset($sizes[$_name]['default_value']) ? $sizes[$_name]['default_value'] : 0;
			$obj->value = isset($sizes[$_name]['value']) ? $sizes[$_name]['value'] : $_value;
			$obj->name = trim( $_name ) ? $_name : $_value;
			//if(isset($sizes[$_name]))
			$sizes[$_name] = $obj;
		}
		*/
		foreach ($sizes as $key => $option)
		{
			$_ordering = isset($option['ordering']) ? $option['ordering'] : $key;
			$_value = $option['value'];
			$_name = $option['name'];
			$_default_value = isset($option['default_value']) ? $option['default_value'] : 0; //$option['default_value'];
			$_displayName = JText::_($_name);
			$obj = new stdClass;
			$obj->ordering = $_ordering;
			$obj->displayName = $_displayName;
			$obj->default_value = $_default_value; //isset($default_value) ? $default_value : 0;
			$obj->value = $_value;//isset($sizes[$_name]['value']) ? $sizes[$_name]['value'] : $_value;
			$obj->name = trim( $_name ) ? $_name : $_value;
			
			//if(isset($sizes[$_name]))
			$sizes[$key] = $obj;
		}
		
		
		return $this->makeTable($sizes,$this->name, $this->id); //, $value);
		//return $this->sizesHtml($sizes,$this->name, $this->id); //, $value);
	}

	function makeTable($rows,$name, $control_name)
	{
		$ds = DIRECTORY_SEPARATOR;
		$html = '';
		$html .= '<div class="paddingList">';
		$imagePath = PagesAndItemsHelper::getDirIcons();

		$counter = 0;
		$outputRows = '';
		$countRows = count($rows);
		if(count($rows))
		{
			//headers
			require_once(realpath(dirname(__FILE__).$ds.'..'.$ds.'..'.$ds.'..'.$ds.'..'.$ds.'..').$ds.'includes'.$ds.'html'.$ds.'tableitems.php');
			$countColumns = 3;
			$config = array('countRows'=>$countRows,'countColumns'=>$countColumns,'itemName'=>$control_name,'output'=>true);
			$table = new htmlTableItems($config);
			$table->table();
			$columns = array();
			$columns[] = array('type'=>'th','content'=>JText::_('Default'),'config'=> array('attributes'=>array('style'=>'width:45px;')));
			$columns[] = array('type'=>'th','content'=>JText::_('Value'),'config'=> array('attributes'=>array('style'=>'width:145px;')));
			$columns[] = array('type'=>'th','content'=>JText::_('Name'));
			//$columns[] = array('type' => 'orderingIcon','config'=> array('loadJs'=>0)); //only thre icon
			$columns[] = array('type' => 'ordering');
			$table->header($columns);
			$table->tbody();

			//loop through items and echo data to hidden fields
			foreach($rows as $row)
			{
				
				$table->trColored();
				$counter++;
				$checked = '';
				if($row->default_value)
				{
					$checked = 'checked="checked"';
				}
				//column 1
				$column1 = '';
				$column1 .= '<input id="reorder_'.$control_name.'_id_'.$counter.'" type="hidden" value="'.$counter.'" />';
				$column1 .= '<input type="radio" ';
				$column1 .= 'name="'.$name.'['.$row->ordering.'][default_value]" ';
				$column1 .= 'id="'.$control_name.'default_value'.$counter.'"  '.$checked.' value="'.$row->default_value.'" ';
				$column1 .= 'onclick="newDefault(\''.$control_name.'default_value\','.$counter.');"';
				$column1 .= '>';
					
				$table->td($column1,array('attributes'=>array('class'=>'items_row_checkbox')));
					
				///column 2
				$column2 = '';
				$column2 .= '<input  type="text" ';
				//$column2 .= 'name="'.$name.'[]['.$row->name.'][value]" ';
				$column2 .= 'name="'.$name.'['.$row->ordering.'][value]" ';
				$column2 .= 'id="'.$control_name.'value'.$counter.'" value="'.$row->value.'" ';
				$column2 .= '/>';
				$table->td($column2);

				$column3 = '';
				$column3 .= '<input type="text" ';
				//$column3 .= 'name="'.$name.'[]['.$row->name.'][name]" ';
				$column3 .= 'name="'.$name.'['.$row->ordering.'][name]" ';
				$column3 .= 'id="'.$control_name.'name'.$counter.'" value="'.$row->name.'" ';
				//TODO is column  3 changed we must change in all columns ['.$row->name.'] 
				//$column3 .= 'onchange="newName(\''.$name.'\',\''.$control_name.'\','.$counter.');"';
				$column3 .= '/>';
				$column3 .= '<input type="text" class="readonly"';
				$column3 .= 'value="'.$row->displayName.'" ';
				$column3 .= '/>';
				$table->td($column3);
				
				$configTd4 = array('countRows'=>$countRows, 'currentRow'=>$counter);
				$table->tdOrdering('',$configTd4);
			}
			$html .= $table->getOutput();
			$html .= '<script src="'.JURI::root(true).'/administrator/components/com_pagesanditems/extensions/itemtypes/googlevideo/media/js/sizes.js" language="JavaScript" type="text/javascript"></script>';
			//$html = '<script src="'.JURI::root(true).'/administrator/components/com_pagesanditems/extensions/itemtypes/youtube/media/js/reorder_rows.js" language="JavaScript" type="text/javascript"></script>';
			$html .= '<script language="JavaScript"  type="text/javascript">';
			$html .= "<!--\n";
			$html .= "var counter = ".$counter.";\n";
			$html .= "-->\n";
			$html .= "</script>\n";
		}
		else
		{
			$html .= JText::_('NO SIZES');

		}
		$html .= '</div>';
		return $html;
	}


//		return $this->sizesHtml($sizes,$this->name, $this->id); //, $value);
//	}

	function sizesHtml($sizes,$name, $control_name) //, $value)
	{
		$html = '';
		//hide the data on the page
		//$html .= '<input type="hidden" id="'.$control_name.'" name="'.$name.'"  value="'.$value.'">';
		//$html .= '<fieldset id="'.$control_name.'_fieldset">';
		$html .= '<div class="pi_width100 xpaddingList" id="target_pages">';
			$html .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">'; // style="display: none;">';
				$html .= '<tbody>';
				$html .= '<tr>';
					$html .= '<td id="pagesheader_column_1">';
						$html .= '<strong>'.JText::_('Default').'</strong>';
					$html .= '</td>';
					$html .= '<td id="pagesheader_column_2">';
						$html .= '<strong>'.JText::_('Value').'</strong>';
					$html .= '</td>';
					$html .= '<td id="pagesheader_column_3">';
						$html .= '<strong>'.JText::_('Name').'</strong>';
					$html .= '</td>';
				$html .= '</tr>';
				//loop through items and echo data to hidden fields
				$count = count($sizes);
				$counter = 0;
				foreach($sizes as $row)
				{
					$counter++;
					$html .= '<tr>';
						//column 1
						$html .= '<td id="'.$control_name.'_column_1_'.$counter.'">';
							$checked = '';
							if($row->default_value)
							{
								$checked = 'checked="checked"';
							}
							$html .= '<input type="radio"';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][default_value]" ';
							//$html .= 'id="'.$control_name.$name.$counter.'default_value" value="'.$row->default_value.'" >';
							$html .= 'name="'.str_replace('sizes','sizes',$name).'['.$row->name.'][default_value]" ';
							$html .= ' id="'.$control_name.'default_value'.$counter.'"  '.$checked.' value="'.$row->default_value.'" ';
							$html .= 'onclick="newDefault(\''.$control_name.'default_value\','.$counter.');"';
							$html .= '>';
						$html .= '</td>';
						//column 2
						$html .= '<td id="'.$control_name.'_column_2_'.$counter.'">';
							$html .= '<input  type="text" ';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][value]" ';
							$html .= 'name="'.str_replace('sizes','sizes',$name).'['.$row->name.'][value]" ';
							$html .= 'id="'.$control_name.$counter.'value" value="'.$row->value.'" ';
							//$html .= 'onchange="newValue(\''.$control_name.$counter.'value\','.$counter.');"';
							$html .= '/>';
						$html .= '</td>';
						$html .= '<td id="'.$control_name.'_column_3_'.$counter.'">';
							$html .= '<input type="text" ';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][name]" ';
							$html .= 'name="'.str_replace('sizes','sizes',$name).'['.$row->name.'][name]" ';
							$html .= 'id="'.$control_name.$counter.'name" value="'.$row->name.'" ';
							//$html .= 'onchange="newName(\''.$control_name.$counter.'name\','.$counter.');"';
							$html .= '/>';
						$html .= '</td>';
					$html .= '</tr>';
					//$counter = $counter + 1;
				}
				$html .= '</tbody>';
			$html .= '</table>';

			$html .= '<script src="'.JURI::root(true).'/administrator/components/com_pagesanditems/extensions/itemtypes/googlevideo/media/js/sizes.js" language="JavaScript" type="text/javascript"></script>';
			$html .= '<script language="JavaScript"  type="text/javascript">';
			$html .= "<!--\n";
			/*
			$html .= "var namePrefix = false;\n";
			$html .= "var controlNamePrefix = '".$control_name."';\n";
			*/
			$html .= "var counter = ".$counter.";\n";
			$html .= "-->\n";
			$html .= "</script>\n";


		$html .= '</div>';
		//$html .= '</fieldset>';
		return $html;
	}
	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	protected function getOptions()
	{
		// Initialize variables.
		$options = array();

		foreach ($this->element->children() as $option) {

			// Only add <option /> elements.
			if ($option->getName() != 'option') {
				continue;
			}

			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_('select.option', (string) $option['value'], trim((string) $option), 'value', 'text', ((string) $option['disabled']=='true'));

			// Set some option attributes.
			$tmp->class = (string) $option['class'];

			// Set some JavaScript option attributes.
			$tmp->onclick = (string) $option['onclick'];

			$tmp->name = (string) $option['name'];
			$tmp->default_value = (string) $option['default_value'];
			
			$tmp->data = (string) $option->data();
			
			// Add the option object to the result set.
			$options[] = $tmp;
		}

		reset($options);

		return $options;
	}
}