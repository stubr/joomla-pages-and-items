<?php
/**
* @version		2.1.1
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2012 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
 */

defined('_JEXEC') or die;
//defined('JPATH_BASE') or die;


/**
 * Supports an HTML select list of plugins
 *
 * @package		Joomla.Administrator
 * @subpackage	com_newsfeeds
 * @since		1.6
 */
class JElementRadiosizes extends JElement
{
	var	$_name = 'Radiosizes';
	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since
	 */
	function fetchElement($name, $value, &$node, $control_name)
	{
		// Initialize some field attributes.
		$size = ( $node->attributes('size') ? 'size="'.$node->attributes('size').'"' : '' );
		$class = ( $node->attributes('class') ? 'class="'.$node->attributes('class').'"' : 'class="text_area"' );

		$sizes = array ();

		$sizes = json_decode(str_replace('\'','"',$value));
		if(!$sizes)
		{
			foreach ($node->children() as $option)
			{
				$_value = $option->attributes('value');
				$_name = $option->attributes('name');
				$_default_value = $option->attributes('default_value');
				$obj = new stdClass;
				$obj->value = $_value;
				$obj->default_value = $_default_value;
				$obj->name = trim( $_name ) ? $_name : $_value;
				$sizes[] = $obj;
			}
		}
		$value = str_replace('"','\'',json_encode($sizes));
		return $this->sizesHtml($sizes,$name, $control_name, $value);
	}

	function sizesHtml($sizes,$name, $control_name, $value)
	{
		$html = '';
		//hide the data on the page
		$html .= '<input type="hidden" id="'.$control_name.$name.'" name="'.$control_name.'['.$name.']"  value="'.$value.'">';
		$html .= '<div class="paddingList">';
			$html .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">'; // style="display: none;">';
				$html .= '<tbody>';
				$html .= '<tr>';
					$html .= '<td id="pagesheader_column_1">';

					$html .= '</td>';
					$html .= '<td id="pagesheader_column_2">';
						$html .= '<strong>'.JText::_('Value').'</strong>';
					$html .= '</td>';
					$html .= '<td id="pagesheader_column_3">';
						$html .= '<strong>'.JText::_('Name').'</strong>';
					$html .= '</td>';
				$html .= '</tr>';
				//loop through items and echo data to hidden fields
				$count = count($sizes);
				$counter = 0;
				foreach($sizes as $row)
				{
					$html .= '<tr>';
						//column 1
						$html .= '<td id="'.$name.'_column_1_'.$counter.'">';
							$checked = '';
							if($row->default_value)
							{
								$checked = 'checked="checked"';
							}
							//$html .= '<input type="hidden" ';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][default_value]" ';
							//$html .= 'id="'.$control_name.$name.$counter.'default_value" value="'.$row->default_value.'" >';

							$html .= '<input type="radio" id="'.$control_name.$name.'default_value'.$counter.'"  '.$checked.' value="'.$row->default_value.'" onclick="newDefault(\''.$control_name.$name.'default_value\','.$counter.');">';
						$html .= '</td>';
						//column 2
						$html .= '<td id="'.$name.'_column_2_'.$counter.'">';
							$html .= '<input  type="text" ';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][value]" ';
							$html .= 'id="'.$control_name.$name.$counter.'value" value="'.$row->value.'" onchange="newValue(\''.$control_name.$name.$counter.'value\','.$counter.');"/>';
						$html .= '</td>';
						$html .= '<td id="'.$name.'_column_2_'.$counter.'">';
							$html .= '<input type="text" ';
							//$html .= 'name="'.$control_name.'['.$name.']['.$counter.'][name]" ';
							$html .= 'id="'.$control_name.$name.$counter.'name" value="'.$row->name.'" onchange="newName(\''.$control_name.$name.$counter.'name\','.$counter.');"/>';
						$html .= '</td>';
					$html .= '</tr>';
					$counter = $counter + 1;
				}
				$html .= '</tbody>';
			$html .= '</table>';


			//$html .= '<div id="target_pages"></div>';
			$html .= '<script src="'.JURI::root(true).'/administrator/components/com_pagesanditems/extensions/itemtypes/googlevideo/media/js/sizes.js" language="JavaScript" type="text/javascript"></script>';
			$html .= '<script language="JavaScript"  type="text/javascript">';
			$html .= "<!--\n";
			//$html .= "var pages_total = ".$counter.";\n";
			$html .= "var namePrefix = '".$name."';\n";
			$html .= "var controlNamePrefix = '".$control_name."';\n";
			$html .= "var counter = ".$counter.";\n";
			//$html .= "var number_of_columns_pages = '2';\n";
			//$html .= "var ordering = '".JText::_('ordering')."';\n";
			//$html .= "var no_pages = '".'_pi_lang_thispagenounderlyingpages'."';\n";
			//$html .= "document.onload = print_ordering();\n";
			$html .= "-->\n";
			$html .= "</script>\n";

		//$html .= '</div>';
		return $html;
	}
}