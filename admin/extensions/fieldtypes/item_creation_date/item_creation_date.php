<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}
require_once(dirname(__FILE__).'/../../../includes/extensions/fieldtype.php');
//ITEM_CREATION_DATE
class PagesAndItemsExtensionFieldtypeItem_creation_date extends PagesAndItemsExtensionFieldtype
{
	function params_base()
	{
		$param[] = 'only_once=1';
		$param[] = 'no_pi_fish_table=1';
		return $param;
	}

	function display_config_form($plugin, $type_id, $name, $field_params, $field_id){
		if(!$field_id)
		{
			//new field, set defaults here			
			if(PagesAndItemsHelper::getJoomlaVersion() >= '3.0'){	
				$field_params['date_format'] = 'd-m-Y';
			}else{
				$field_params['date_format'] = '%d-%m-%Y';
			}
		}
		if(PagesAndItemsHelper::getJoomlaVersion() >= '3.0'){
			$example = 'd-m-Y H:i:s';	
		}else{
			$example = '%d-%m-%Y %H:%i:%s';
		}
		//display
		$field_name = JText::_('COM_PAGESANDITEMS_DATE_FORMAT');
		$field_content = '<input type="text" class="width200" value="';
		$field_content .= $field_params['date_format'];
		$field_content .= '" name="field_params[date_format]" />';
		$field_content .= '<br />';
		$field_content .= JText::_('COM_PAGESANDITEMS_EXAMPLE');
		$field_content .= '<br />'.$example.'<br /><a href="http://php.net/manual/en/function.date.php" target="_blank">';
		$field_content .= 'http://php.net/manual/en/function.date.php</a><br />';
		$field_content .= JText::_('COM_PAGESANDITEMS_NOT_SHOW_ON_EDIT');
		$html = $this->display_field($field_name, $field_content);

		return $html;
	}

	function display_item_edit($field, $field_params, $field_values, $field_value, $new_field, $field_id){
		return '';
	}

	function render_field_output($field, $intro_or_full, $readmore_type=0, $editor_id=0,$language = null){

		$item_id = $field->item_id;
		$app = JFactory::getApplication();
		$ds = DIRECTORY_SEPARATOR;
		
		$this->db->setQuery("SELECT created FROM #__content WHERE id='$item_id' LIMIT 1");
		//$items = $this->db->loadColumn(); //loadResultArray
		//$item = $items[0];
		$item = $this->db->loadResult();
		
		$format = $this->get_field_param($field->params, 'date_format');
		
		$path = realpath(dirname(__FILE__).$ds.'..'.$ds.'..'.$ds.'..');
		require_once($path.$ds.'includes'.$ds.'date.php');
		$date = new PagesAndItemsDate($item);
		
		if(PagesAndItemsHelper::getJoomlaVersion() >= '3.0'){	
			$config = JFactory::getConfig();
			$user	= JFactory::getUser();			
			require_once(JPATH_ROOT.$ds.'libraries'.$ds.'src'.$ds.'Date'.$ds.'Date.php');
			$jDate = new jDate($date);
			$jDate->setTimezone(new DateTimeZone($user->getParam('timezone', $config->get('offset'))));			
			$date = $jDate->toSql(true);
			$date = $jDate->format($format,true);
		}else{
			//this appeared to have worked for j2.5			
			$offset = $app->getCfg('offset');
			$summertime = date( 'I', $date->toUnix() );
			if($summertime){
				$offset = $offset +1;
			}
			$date->setOffset($offset);	
			$date = $date->format($format,true);		
		}

		return $date;
	}
}

?>