<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'view.html.php');

/**
 * HTML View class for the  component

 */
class PagesAndItemsViewMenuItemTypeSelect extends PagesAndItemsViewDefault
{
	function display($tpl = null)
	{
		$doc = JFactory::getDocument();
		
		/*
		if ($model =$this->getModel('Page')) //,'PagesAndItemsModel'))
		{
			//echo 'model';
			$this->assignRef( 'model',$model);
			//$joomlaVersion = $model->getJoomlaVersion();
			//$this->assignRef( 'joomlaVersion',$joomlaVersion);
		}
		*/

		$path = PagesAndItemsHelper::getDirCSS();
		JHTML::stylesheet($path.'/popup.css');
		//JHTML::stylesheet('pagesanditems3.css', $path.'/');
		
		$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).'/../../media/css/').DIRECTORY_SEPARATOR));
		JHTML::stylesheet($path.'/menuitemtypeselect.css');
/*
		}
		else
		{
			JHTML::stylesheet($path.'menuitemtypeselect.css');
		}
*/


		JHTML::_('behavior.tooltip');


		$pageId = JFactory::getApplication()->input->get( '$pageId', 0); 
		$menutype = JFactory::getApplication()->input->get( 'menutype', null); 
		$menutypes = null;
		$modelMenu = new PagesAndItemsModelMenutypes();
		$menutypes = $modelMenu->getTypeListItems(null,$pageId,$menutype);
		$this->assignRef( 'menutypes',$menutypes);
		$lang = JFactory::getLanguage();

		parent::display($tpl);

	}



	function getMenuItemTypes16()
	{
		/*
		//require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'fields'.DIRECTORY_SEPARATOR.'menutype.php');
		//TODO make new model componentmenutype and menutype extends JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'fields'.DIRECTORY_SEPARATOR.'menutype.php

		from this model we can use here we can use
		_getTypeOptions()
		here we can get all we need

		// Initialise variables.
		$html		= array();
		$types		= $this->_getTypeOptions();
		$recordId	= (int) $this->form->getValue('id');

		$html[] = '<h2 class="modal-title">'.JText::_('COM_MENUS_TYPE_CHOOSE').'</h2>';
		$html[] = '<ul class="menu_types">';

		foreach ($types as $name => $list)
		{
			$html[] = '<li>';
			$html[] = '<dl class="menu_type">';
			$html[] = '	<dt>'.JText::_($name).'</dt>';
			$html[] = '	<dd>';
			$html[] = '		<ul>';
			foreach ($list as $item)
			{
				$html[] = '			<li>';
				$html[] = '				<a class="choose_type" href="#" onclick="javascript:Joomla.submitbutton(\'item.setType\', \''.
											base64_encode(json_encode(array('id' => $recordId, 'title' => $item->title, 'request' => $item->request))).'\')"' .
											' title="'.JText::_($item->description).'">'.
											JText::_($item->title).'</a>';
				$html[] = '			</li>';
			}

			$html[] = '		</ul>';
			$html[] = '	</dd>';
			$html[] = '</dl>';
			$html[] = '</li>';
		}

		$html[] = '<li>';
		$html[] = '<dl class="menu_type">';
		$html[] = '	<dt>'.JText::_('COM_MENUS_TYPE_SYSTEM').'</dt>';
		$html[] = '	<dd>';
		$html[] = '		<ul>';
		$html[] = '			<li>';
		$html[] = '				<a class="choose_type" href="#" onclick="javascript:Joomla.submitbutton(\'item.setType\', \''.
									base64_encode(json_encode(array('id' => $recordId, 'title'=>'url'))).'\')"' .
									' title="'.JText::_('COM_MENUS_TYPE_EXTERNAL_URL_DESC').'">'.
									JText::_('COM_MENUS_TYPE_EXTERNAL_URL').'</a>';
		$html[] = '			</li>';
		$html[] = '			<li>';
		$html[] = '				<a class="choose_type" href="#" onclick="javascript:Joomla.submitbutton(\'item.setType\', \''.
									base64_encode(json_encode(array('id' => $recordId, 'title'=>'alias'))).'\')"' .
									' title="'.JText::_('COM_MENUS_TYPE_ALIAS_DESC').'">'.
									JText::_('COM_MENUS_TYPE_ALIAS').'</a>';
		$html[] = '			</li>';
		$html[] = '			<li>';
		$html[] = '				<a class="choose_type" href="#" onclick="javascript:Joomla.submitbutton(\'item.setType\', \''.
									base64_encode(json_encode(array('id' => $recordId, 'title'=>'separator'))).'\')"' .
									' title="'.JText::_('COM_MENUS_TYPE_SEPARATOR_DESC').'">'.
									JText::_('COM_MENUS_TYPE_SEPARATOR').'</a>';
		$html[] = '			</li>';
		$html[] = '		</ul>';
		$html[] = '	</dd>';
		$html[] = '</dl>';
		$html[] = '</li>';
		$html[] = '</ul>';

		return implode("\n", $html);


		*/

	}


	function getMenuItemTypes()
	{
		if(PagesAndItemsHelper::getIsJoomlaVersion('>=','1.6'))
		{
			return $this->getMenuItemTypes16();
		}


		$sub_task = JFactory::getApplication()->input->get( 'sub_task', 'edit'); 
		$extension = 'com_menus';
		$lang = JFactory::getLanguage();
		$lang->load(strtolower($extension), JPATH_ADMINISTRATOR, null, false, false) || $lang->load(strtolower($extension), JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
		$pageType = JFactory::getApplication()->input->get( 'pageType', '' ); 
		$pageId = JFactory::getApplication()->input->get( 'pageId', 0 ); 
		$type = JFactory::getApplication()->input->get( 'type', 'component' ); 
		$pageUrl = JFactory::getApplication()->input->get('pageUrl', array(), 'array'); 
		$pageUrlOption = JFactory::getApplication()->input->get('pageUrlOption', array(), 'array'); 
		$sub_task = JFactory::getApplication()->input->get('sub_task'); 
		require_once( JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php' );
		//require_once( JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'item.php' );

		$requestUrl = JFactory::getApplication()->input->get('url', array(), 'array'); 
		$requestType = JFactory::getApplication()->input->get('type', null); 
		$requestEdit = JFactory::getApplication()->input->get( 'edit', false ); 
		$requestCid = JFactory::getApplication()->input->get('cid', array(), 'array'); 
		$requestExpand = JFactory::getApplication()->input->get('expand', '');

		/*
		$dirIcons = $this->controller->dirIcons;
		JFactory::getApplication()->input->set( 'dirIcons', $dirIcons );
		*/
		if($sub_task=='new')
		{
			JFactory::getApplication()->input->set( 'edit', false );
			
		}
		else
		{
			JFactory::getApplication()->input->set( 'edit', true );
			JFactory::getApplication()->input->set( 'cid',  array($pageId));
		}
		$html = '';
		$components = MenusHelper::getComponentList();
		//$html .= '<ul class="adminformlist" style="float: left;">'; //style="float: left;height: 254px;overflow: auto;width: auto;">';
		//require_once( JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php' );
		//require_once( JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'item.php' );
		/*
		$image = '<img src="'.$dirIcons.'processing.gif" >';
		$text = '';
		//$text = '<div style="padding:50px;">';
		$text .= JText::_('LOAD');
		$text .= $image;
		//$text .= '</div>';
		//$text = htmlspecialchars($text);
		$text = '';
		*/
		$html .= '<ul class="uLmenulist" id="uLmenulist" style="">';
		for ($i=0,$n=count($components);$i<$n;$i++)
		{
			//if($components[$i]->option != 'com_content')
			//{
				//TODO replace href with onclick
				//in models/page_item.php too
				$path = JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$components[$i]->option.DIRECTORY_SEPARATOR.'views';
				$components[$i]->legacy = !is_dir($path);
				$lang->load($components[$i]->option, JPATH_ADMINISTRATOR, null, false, false) || $lang->load($components[$i]->option, JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);

				if($components[$i]->legacy)
				{
					
					
					//$menu_item = new PagesAndItemsModelPage_item();
					$menu_item = new PagesAndItemsModelPage_items();
					$item = $menu_item->getItem();


					$html .='<li>';
						//$html .='<div class="node-open">';
							//$html .='<span></span>';

							$html .='<a ';
								$html .= 'class="child" ';
								$html .= 'onclick="';
									$html .='window.parent.document.getElementById(\'urloption\').value = \''.$components[$i]->option.'\'; ';
									$html .='window.parent.document.getElementById(\'pageType\').value = \''.str_replace('com_', '', $components[$i]->option).'\'; ';
									$html .='window.parent.document.getElementById(\'type\').value = \'component\'; ';
									$html .='window.parent.document.getElementById(\'pageId\').value = \''.$pageId.'\'; ';
									$html .='window.parent.document.getElementById(\'menutype\').value = \''.htmlspecialchars($item->menutype).'\'; ';
									$html .='window.parent.document.getElementById(\'sub_task\').value = \'new\'; ';
									$html .='window.parent.document.getElementById(\'view\').value = \'page\'; ';
									$version = new JVersion();
									$joomlaVersion = $version->getShortVersion();
									if($joomlaVersion >= '1.6')
									{
										//$onclick .='Joomla.';
										$html .='window.parent.Joomla.submitbutton(\'newMenuItem\'); ';
									}
									else
									{
										$html .='window.parent.submitbutton(\'newMenuItem\'); ';
									}
									$html .='window.parent.document.getElementById(\'sbox-window\').close();" ';
								$html .= ' id="'.str_replace('com_', '', $components[$i]->option).'" ';
								$html .= ' >';
								$html .= JText::_($components[$i]->name);
							$html .='</a>';

				}
				elseif(isset($pageUrlOption) && $components[$i]->option == $pageUrlOption)
				{
					$html .= '<li class="expand" style="">';
					JFactory::getApplication()->input->set( 'url', $pageUrl);
					JFactory::getApplication()->input->set( 'type', $type);
					//$menu_item = new PagesAndItemsModelPage_item($pageId,true);
					$menu_item = new PagesAndItemsModelPage_items($pageId,true);
					$item = $menu_item->getItem();
					$typeCid = '&amp;menutype=' . htmlspecialchars($item->menutype).'&amp;cid[]=' . $item->id;

					$html .= '<a ';
						$html .= 'class="parent" ';
						$html .= '>';
						$html .= JText::_($components[$i]->name);
					$html .= '</a>';
				}
				else
				{
					JFactory::getApplication()->input->set( 'type',  'component');
					JFactory::getApplication()->input->set( 'url', array('option'=>$components[$i]->option));
					//$menu_item = new PagesAndItemsModelPage_item();
					$menu_item = new PagesAndItemsModelPage_items();
					$item = $menu_item->getItem();
					JFactory::getApplication()->input->set('expand',str_replace('com_', '', $components[$i]->option));
					$expansion =$menu_item->getExpansion();
					$class = ($i == $n-1)? 'class="last"' : '';
					$html .= '<li '.$class.'>';
					//	$html .= '<div class="node-open">';
					//		$html .= '<span></span>';
							if($expansion['html'] != '')
							{
								$html .= '<a id="'.str_replace('com_', '', $components[$i]->option).'" ';
								$html .= 'class="parent" ';
								$html .= '>';
									$html .= JText::_($components[$i]->name);
									//$html .= $components[$i]->name;
								$html .= '</a>';
							}
							else
							{
								$html .='<a ';

									/*
									$url = array();
									*/
									$html .= 'class="child" ';
									//$this->_output .= 'title="' . JText::_($this->_current->title);
									//$this->_output .= '::' . JText::_($this->_current->msg) . '" ';

									$html .= 'onclick="';
									$html .='window.parent.document.getElementById(\'urloption\').value = \''.$components[$i]->option.'\'; ';

									$html .='window.parent.document.getElementById(\'pageType\').value = \''.str_replace('com_', '', $components[$i]->option).'\'; ';
									$html .='window.parent.document.getElementById(\'type\').value = \'component\'; ';
									//if($pageId)
									//{
										$html .='window.parent.document.getElementById(\'pageId\').value = \''.$pageId.'\'; ';
									//}
									$html .='window.parent.document.getElementById(\'menutype\').value = \''.htmlspecialchars($item->menutype).'\'; ';
									//$html .='window.parent.document.getElementById(\'sub_task\').value = \''.$sub_task.'\'; ';
									$html .='window.parent.document.getElementById(\'sub_task\').value = \'new\'; ';
									$html .='window.parent.document.getElementById(\'view\').value = \'page\'; ';
									//TODO place here an image
									//$html .='window.parent.document.getElementById(\'underlayingPages\').innerHTML = \''.$text.'\'; ';
									$version = new JVersion();
									$joomlaVersion = $version->getShortVersion();
									if($joomlaVersion >= '1.6')
									{
										//$onclick .='Joomla.';
										$html .='window.parent.Joomla.submitbutton(\'newMenuItem\'); ';
									}
									else
									{
										$html .='window.parent.submitbutton(\'newMenuItem\'); ';
									}
									$html .='window.parent.document.getElementById(\'sbox-window\').close();" ';




									/*
									$html .= 'onclick="';
									$html .= 'window.parent.document.location.href=\'';
									$html .= 'index.php?option=com_pagesanditems';
									$html .= '&amp;view=page';
									if(!$pageId)
									{
										$html .= '&amp;layout=root';
									}
									else
									{
										$html .= '&amp;pageId='.$pageId;
									}
									$html .= '&amp;sub_task='.$sub_task;
									$html .= '';
									$html .= '&amp;type=component';
									$html .= '&amp;url[option]=';
									$html .= $components[$i]->option;
									$html .= '&amp;menutype='.htmlspecialchars($item->menutype);
									$html .= '&amp;pageType='.str_replace('com_', '', $components[$i]->option);
									$html .= '&amp;cid[]='.$item->id;
									$html .= '\'';
									$html .= ';window.parent.document.getElementById(\'sbox-window\').close();"';
									*/

									$html .='id="';
									$html .= str_replace('com_', '', $components[$i]->option);
									$html .= '" ';
									$html .= ' >';
									$html .= JText::_($components[$i]->name);
									/*
									$this->_output .= 'title="' . JText::_($this->_current->title);
									$this->_output .= '::' . JText::_($this->_current->msg) . '">';
									$this->_output .= JText::_($this->_current->title);
									*/
								$html .= '</a>';
							}
					//	$html .= '</div>';

					$html .= $expansion['html'];

						/*
						expansion['html'] =
						"index.php?
						option=com_menus
						&amp;task=edit
						&amp;type=component
						&amp;url[option]=com_content
						&amp;url[view]=archive
						&amp;menutype=customcontent"

						 title="Archived Article List Layout::Das Standardlayout f�r archivierte Beitr�ge zeigt Beitr�ge die archiviert wurden. Sie sind nach dem Datum durchsuchbar.">Archived Article List Layout</a></div></li>

						*/

				//}
				$html .= '</li>';
			//$html .= '</ul>';
			//$html .= '</div>';
			//$html .= '<div class="clr_left"></div>';
			}
		}
		$html .= '</ul>';
		/*

		JFactory::getApplication()->input->set( 'type',  'component');
		JFactory::getApplication()->input->set( 'url', array('option'=>$components[$i]->option));
		JFactory::getApplication()->input->set('expand',str_replace('com_', '', $components[$i]->option));
		*/
		JFactory::getApplication()->input->set( 'type', $requestType);
		JFactory::getApplication()->input->set( 'url', $requestUrl);
		JFactory::getApplication()->input->set( 'edit', $requestEdit );
		JFactory::getApplication()->input->set( 'cid', $requestCid );
		JFactory::getApplication()->input->set( 'expand', $requestExpand );



		return $html;
		/*
		onclick="window.parent.document.getElementById('sbox-window').close();">

		$expansion		=$this->get('Expansion');

		//$this->menu_item
		//$expansion = $this->menu_item->getExpansion();


		for ($i=0,$n=count($components);$i<$n;$i++)
		{
			//$menu_item =new MenusModelItem();
			if(components[$i]->option == $pageUrl['option'])
			{

				JFactory::getApplication()->input->set( 'url',  array("option"=>$pageUrl['option'],"view"=>$pageUrl['view'],"layout"=>$pageUrl['layout']));
			}
			else
			{
				JFactory::getApplication()->input->set( 'url',  array());
			}
			$menu_item = new PagesAndItemsModelPage_item(null,true);
			$menu_item_urlparams = $menu_item->getUrlParams();

					$menu_item_urlparams_option = $menu_item_urlparams->get('option',null);
					$menu_item_urlparams_view = $menu_item_urlparams->get('view',null);
					$menu_item_urlparams_layout = $menu_item_urlparams->get('layout',null);
					$menu_item_urlparams_id = $menu_item_urlparams->get('id',null);

			//test for expansion
			//if(!$components[$i]->option == 'com_content'
			//{
				if($components[$i]->legacy)
				{
					<li>
						<div class="node-open">
							<span></span>
							<a href="index.php?option=com_menus&amp;task=edit&amp;type=component&amp;url[option]=
								<?php echo $this->components[$i]->option . $typeCid; ?>"
								id="<?php echo str_replace('com_', '', $this->components[$i]->option); ?>">
								<?php echo $this->components[$i]->name; ?>
							</a>
						</div>
				}
				elseif ($this->expansion['option'] == str_replace('com_', '', $this->components[$i]->option))
				{
					<li <?php echo ($i == $n-1)? 'class="last"' : '' ?>>
						<div class="node-open">
							<span></span>
								<a id="<?php echo str_replace('com_', '', $this->components[$i]->option); ?>"><?php echo JText::_($this->components[$i]->name); ?>
								</a>
							</div>
						<?php echo $this->expansion['html']; ?>
				}
				else
				{
					<li <?php echo ($i == $n-1)? 'class="last"' : '' ?>>
						<div class="node">
							<span></span>
							<a href="index.php?option=com_menus&amp;task=type<?php echo $typeCid; ?>&amp;expand=<?php echo str_replace('com_', '', $this->components[$i]->option); ?>" id="<?php echo str_replace('com_', '', $this->components[$i]->option); ?>"><?php echo JText::_($this->components[$i]->name); ?>
							</a>
						</div>
				}
				</li>
			//}
		}

		/*
		$menu_item =new MenusModelItem();
		$menu_item->pageType = $pageType;
		$this->menu_item = $menu_item; //->getItem();
		if($sub_task=='new')
		{
			$this->pageMenuItem = $menu_item->getItem();
			//$this->pageMenuItem->pageType = $pageType;
		}
		*/
		/*
		if($sub_task=='new')
		{
			$this->menu_item = $menu_item->getItem();
		}
		*/
	}

}
