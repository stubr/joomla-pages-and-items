<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
//require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'view.html.php');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'page'.DIRECTORY_SEPARATOR.'view.html.php');

/**
 * HTML View class for the  component

 */


class PagesAndItemsViewArchive extends PagesAndItemsViewDefault
{
	function display($tpl = null)
	{
		$archiveType = JFactory::getApplication()->input->get('archiveType','all'); 
		$this->assignRef('archiveType', $archiveType);
		if ($model =$this->getModel('Page'))
		{

			$pageTree = $model->getPages();
			$this->assignRef( 'pageTree',$pageTree);

			$menuItemsTypes = $model->menuItemsTypes;
			$this->assignRef( 'menuItemsTypes',$menuItemsTypes);

			$this->assignRef( 'model',$model);
		}
		/*
		not here JHTML::stylesheet('pagesanditems3.css', 'administrator/components/com_pagesanditems/css/');
		JHTML::stylesheet('dtree.css', 'administrator/components/com_pagesanditems/css/');
		*/
		$pathComponent = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).'/../../../../../../../').DIRECTORY_SEPARATOR));
		JHTML::script($path.'/dtree.js',false); //, 'administrator/components/com_pagesanditems/javascript/',false);
		//JHTML::script('overlib_mini.js', 'includes/js/',false);
		JHTML::_('behavior.tooltip');

		parent::display($tpl);

	}

}
