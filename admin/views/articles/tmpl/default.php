<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
$user		= JFactory::getUser();
$app		= JFactory::getApplication();
$userId		= $user->get('id');

$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');

$selected = 'selected="selected"'; 	

JHtml::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_content/helpers/html');
JHtml::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHtml::_('behavior.multiselect');

if(version_compare(JVERSION, '3', '>')){
	JHtml::_('bootstrap.tooltip');
	JHtml::_('formbehavior.chosen', 'select');
}
		
?>
<script language="javascript" type="text/javascript">

Joomla.submitbutton = function(task){		
	if(task=='articles.articles_delete'){
		//all other actions which require a selection are caught with the javascript in the toolbar button itself	
		if (document.adminForm.boxchecked.value == '0') {						
			alert('<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NO_ITEMS_SELECTED')); ?>');
			return false;
		}else{
			if(confirm("<?php echo addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_ITEMS_DELETE')); ?>?")){
				submitform(task);
			}			
		}
	}
	if(task=='article_new'){
		//this would be a nice way to open the modal dialog if it had not the bug where it does not open a second time
		//see bottom of page					
		//SqueezeBox.initialize('{size: {x: 300, y: 150}}');//setting size does not work
		//SqueezeBox.setContent('string', modalcontent);
		SqueezeBox.fromElement('modal_link',{url: 'index.php?option=com_pagesanditems&view=itemtypeselect&tmpl=component', handler: 'iframe',size: {x: 500, y: 150}});
	}
	if(task=='articles.articles_publish' || task=='articles.articles_unpublish' || task=='articles.articles_featured' || task=='articles.articles_archive' || task=='articles.articles_trash'){		
		submitform(task);
	}
}

function publish(id){
	document.adminForm.article_id.value = id;
	submitform('articles.article_publish');
}

function unpublish(id){
	document.adminForm.article_id.value = id;	
	submitform('articles.article_unpublish');
}

function feature(id){
	document.adminForm.article_id.value = id;
	submitform('articles.article_feature');
}

function unfeature(id){
	document.adminForm.article_id.value = id;	
	submitform('articles.article_unfeature');
}

function listItemTask(id, task){
	id = id.replace(/cb/, "");	
	document.adminForm.article_id.value = id;	
	submitform(task);
}

</script>
<div id="form_content">
<form name="adminForm" id="adminForm" method="post" action="<?php echo JRoute::_('index.php?option=com_pagesanditems&view=articles'); ?>">
	<table style="width: 100%;">
		<tr>
			<td>				
				<table style="width: 100%;">
					<tr>
						<td>					
						<input type="text" name="items_search" id="items_search" value="<?php echo $this->state->get('filter.search'); ?>" class="text_area"  />			
						&nbsp;
						<button onclick="this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
						&nbsp;
						<button onclick="document.getElementById('items_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
						</td>
						<td >
							<a style="text-align: right;" href="index.php?option=com_pagesanditems&view=config&tab=itemtypes">CCK <?php echo JText::_('COM_PAGESANDITEMS_CONFIG'); ?></a>
							&nbsp;
						<!--</td>
						<td>-->
							<select name="filter_type" class="inputbox" onchange="this.form.submit()">
								<option value=""> - <?php echo JText::_('COM_PAGESANDITEMS_EXTENSIONS_SELECT_TYPE');?> - </option>
								<?php echo JHtml::_('select.options', $this->get_type_options(), 'value', 'text', $this->state->get('filter.type'), true);?>
							</select>
							&nbsp;
							<select name="filter_published" class="inputbox" onchange="this.form.submit()">
								<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
								<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true);?>
							</select>
							&nbsp;
							<select name="items_category_filter" onchange="this.form.submit()" class="inputbox">
								<option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option>
								<?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text', $this->state->get('filter.category'));?>
							</select>			
							&nbsp;
							<select name="filter_access" class="inputbox" onchange="this.form.submit()">
								<option value=""><?php echo JText::_('JOPTION_SELECT_ACCESS');?></option>
								<?php echo JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'));?>
							</select>
							&nbsp;
							<select name="filter_author" class="inputbox" onchange="this.form.submit()">
								<option value=""><?php echo JText::_('JOPTION_SELECT_AUTHOR');?></option>
								<?php echo JHtml::_('select.options', $this->authors, 'value', 'text', $this->state->get('filter.author'));?>
							</select>
							&nbsp;
							<select name="filter_language" class="inputbox" onchange="this.form.submit()">
								<option value=""><?php echo JText::_('JOPTION_SELECT_LANGUAGE');?></option>
								<?php echo JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'));?>
							</select>
						</td>
					</tr>
				</table>			
			</td>
			
		</tr>
	</table>	
	<table class="adminlist">
		<tr>
			<th width="5" class="center">
				<?php if(version_compare(JVERSION, '3', '>')){ ?>
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />		
				<?php }else{ ?>
					<input type="checkbox" name="toggle" value="" <?php if(isset($this->items)){ ?>onclick="checkAll(<?php echo count($this->items); ?>);"<?php } ?> />			
				<?php } ?>
			</th>			
			<th align="left">
				<?php 			
				$label = ucfirst(JText::_('JFIELD_TITLE_DESC')).' '; 			
				echo JHTML::_('grid.sort', $label, 'c.title', $listDirn, $listOrder); 			
				?>						
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JSTATUS', 'c.state', $listDirn, $listOrder); ?>
			</th>			
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JFEATURED', 'c.featured', $listDirn, $listOrder); ?>
			</th>			
			<th class="center nowrap">
				<?php 			
				echo JHTML::_('grid.sort', 'JCATEGORY', 'category_title', $listDirn, $listOrder); 			
				?>
			</th>
			<th class="center nowrap">
				<?php 
				$label = 'CCK '.JText::_('COM_PAGESANDITEMS_TYPE');			
				echo JHTML::_('grid.sort', $label, 'itemtype', $listDirn, $listOrder); 			
				?>
			</th>
			<th class="center">				
				<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ORDERING', 'c.ordering', $listDirn, $listOrder); ?>				
				<a href="javascript:submitform('articles.articles_order_save');" class="saveorder" title="Save Order"></a>			
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ACCESS', 'access_level', $listDirn, $listOrder); ?>
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_CREATED_BY', 'c.created_by', $listDirn, $listOrder); ?>
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JDATE', 'c.created', $listDirn, $listOrder); ?>
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JGLOBAL_HITS', 'c.hits', $listDirn, $listOrder); ?>
			</th>
			<th class="center">
				<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
			</th>
			<th width="1%" class="nowrap center">
				<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'c.id', $listDirn, $listOrder); ?>
			</th>
		</tr>
			
		<?php					
			$k = 1;	
			$i = 0;			
			foreach($this->items as $item){						
				
			$item->max_ordering = 0; //??
			$ordering	= ($listOrder == 'a.ordering');
			$canCreate	= $user->authorise('core.create',		'com_content.category.'.$item->catid);
			$canEdit	= $user->authorise('core.edit',			'com_content.article.'.$item->id);
			$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
			$canEditOwn	= $user->authorise('core.edit.own',		'com_content.article.'.$item->id) && $item->created_by == $userId;
			$canChange	= $user->authorise('core.edit.state',	'com_content.article.'.$item->id) && $canCheckin;
				
				
				echo '<tr class="row'.$k.'">';
				echo '<td class="center" style="padding: 3px;">';
				if(version_compare(JVERSION, '3', '>=')){
					echo JHtml::_('grid.id', $i, $item->id);
				}else{
					echo '<input type="checkbox" id="cb'.$i.'" name="cid[]" value="'.$item->id.'" onclick="isChecked(this.checked);" />';
				}	
				echo '</td>';		
				echo '<td>';
				echo '<a href="index.php?option=com_pagesanditems&view=item&sub_task=edit&itemId='.$item->id.'">';
				echo $this->escape($item->title);
				echo '</a>';
				echo '</td>';			
				echo '<td class="center reorder_rows_arrows">';				
				echo '<a href="javascript:void(0);" onclick="';
				if($item->state!=0){
					echo 'un';
				}
				echo 'publish('.$item->id.');" class="jgrid" title="';
				if($item->state!=0){
					echo 'un';
				}
				echo 'publish">';
				echo '<span class="state ';				
				if($item->state!=1){
					echo 'un';
				}
				echo 'publish';				
				echo '"></span>';
				echo '</a>';				
				echo '</td>';
				echo '<td class="center">';
				echo '<a href="javascript:void(0);" onclick="';
				if($item->featured==1){
					echo 'un';
				}
				echo 'feature('.$item->id.');" class="jgrid" title="';
				if($item->featured==1){
					echo 'un';
				}
				echo 'feature">';
				echo '<img src="components/com_pagesanditems/images/';
				if($item->featured==0){
					echo 'un';
				}
				echo 'featured.png" alt="toggle_featured" />';
				echo '</a>';
				echo '</td>';				
				echo '<td class="center">'.$this->escape($item->categorytitle).'</td>';
				echo '<td class="center">';							
				echo $this->escape($this->helper->translate_item_type($item->itemtype));
				echo '</td>';
				?>
				<td class="alignright order">
					<?php					
					if($listOrder=='c.ordering'){						
						?>				
						<?php if ($listDirn == 'asc') : ?>
								<span><?php echo $this->pagination->orderUpIcon($item->id, ($item->catid == @$this->items[$i-1]->catid), 'articles.reorder_article_up', 'JLIB_HTML_MOVE_UP', 1); ?></span>
								<span><?php echo $this->pagination->orderDownIcon($item->id, $this->pagination->total, ($item->catid == @$this->items[$i+1]->catid), 'articles.reorder_article_down', 'JLIB_HTML_MOVE_DOWN', 1); ?></span>
							<?php elseif ($listDirn == 'desc') : ?>
								<span><?php echo $this->pagination->orderUpIcon($item->id, ($item->catid == @$this->items[$i-1]->catid), 'articles.reorder_article_down', 'JLIB_HTML_MOVE_UP', 1); ?></span>
								<span><?php echo $this->pagination->orderDownIcon($item->id, $this->pagination->total, ($item->catid == @$this->items[$i+1]->catid), 'articles.reorder_article_up', 'JLIB_HTML_MOVE_DOWN', 1); ?></span>
							<?php endif; ?>
					<?php
					}
					?>					
					<input type="text" name="orders[]" size="5" value="<?php echo $item->ordering;?>" class="text-area-order" />
					<input type="hidden" name="order_ids[]" value="<?php echo $item->id; ?>" />
				</td>
				<?php				
				echo '<td class="center">'.$this->escape($item->access_level).'</td>';
				echo '<td class="center">'.$this->escape($item->author_name).'</td>';
				echo '<td class="center">'.JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4')).'</td>';
				echo '<td class="center">'.$item->hits.'</td>';				
				echo '<td class="center">';
				if ($item->language=='*'){
					echo JText::alt('JALL', 'language');
				}else{
					if($item->language_title){
						echo $this->escape($item->language_title);
					}else{
						JText::_('JUNDEFINED');
					}
				}
				echo '</td>';
				echo '<td class="center">'.$item->id.'</td>';			
				echo '</tr>';
				if($k==1){
					$k = 0;
				}else{
					$k = 1;
				}	
				$i++;								
			}
		?>		
	</table>
	<table class="adminlist">
		<tfoot>
			<tr>
				<td>
				<?php 
					echo $this->pagination->getListFooter();
				?>
				</td>
			</tr>
		</tfoot>
	</table>
	<input type="hidden" name="reorder_id" value="" />
	<input type="hidden" name="reorder_direction" value="" />		
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="article_id" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />	
	<?php echo JHTML::_( 'form.token' ); ?>		

</form>
</div>
<!--
this would be a nice way to open the modal dialog if it had not the bug where it does not open a second time
<div style="display: none;">
	<div id="modalcontent">	
		<div style="text-align: center;">	
			<h2 class="componentheading" id="header_item_type_select">
				<?php echo JText::_('COM_PAGESANDITEMS_SELECT_ITEMTYPE_FOR_NEW_ITEM'); ?>
			</h2>
			<div class="item">
				<?php 
				echo $this->helper->itemtype_select(0);
				?>			
			</div>	
		</div>			
	</div>	
</div>
so I use the link underneath to open the modal. this method also ignores the sizes. sight.
-->
<div>
<a href="index.php?option=com_pagesanditems&view=itemtypeselect&tmpl=component" id="modal_link" class="modal" rel="{handler: 'iframe',size: {x: 300, y: 150}}"></a>
</div>



<?php
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
?>