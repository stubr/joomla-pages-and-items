<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'view.html.php');

class PagesAndItemsViewArticles extends PagesAndItemsViewDefault //JView
{

	protected $items;
	protected $pagination;
	protected $state;
	protected $helper;

	function display($tpl = null){	
	
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->authors = $this->get_authors();	
		
		$controller = new PagesAndItemsController();	
		$this->assignRef('controller', $controller);
		
		$helper = new PagesAndItemsHelper();
		$this->helper = $helper;
		$this->assignRef('helper', $helper);	
		
		
		//JHtmlSidebar::setAction('index.php?option=com_content&view=articles');
		//toolbar	
		JToolBarHelper::custom('article_new','new.png','new_f2.png',JText::_('JTOOLBAR_NEW'),false,false);	
		JToolBarHelper::divider();	
		JToolBarHelper::publish('articles.articles_publish', 'JTOOLBAR_PUBLISH', true);
		JToolBarHelper::unpublish('articles.articles_unpublish', 'JTOOLBAR_UNPUBLISH', true);
		JToolBarHelper::custom('articles.articles_featured', 'featured.png', 'featured_f2.png', 'JFEATURED', true);
		JToolBarHelper::archiveList('articles.articles_archive');
		JToolBarHelper::trash('articles.articles_trash');			
		JToolBarHelper::custom('articles.articles_delete','delete.png','delete_f2.png',JText::_('JTOOLBAR_DELETE'),false,false);	
		//submenu		
		$helper->addSubmenu();
		
		//get language
		$lang = JFactory::getLanguage();
		$lang->load('mod_menu', JPATH_ADMINISTRATOR, null, false);
		
		$extension = 'com_content';
		$lang->load(strtolower($extension), JPATH_ADMINISTRATOR, null, false, false) || $lang->load(strtolower($extension), JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
		
		//set header		
		JToolBarHelper::title('Pages and Items :: <small>'.JText::_('MOD_MENU_COM_CONTENT_ARTICLE_MANAGER').'</small>', 'pi.png');
		
		$document = JFactory::getDocument();	
		$document->addStyleSheet('components/com_pagesanditems/css/pagesanditems3.css');
		if(version_compare(JVERSION, '3', '>=')) {JHTML::stylesheet('components/com_pagesanditems/css/pagesanditemsj3.css');}
		parent::display($tpl);
	}
	
	function get_authors(){
		
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);		
		$query->select('u.id AS value, u.name AS text');
		$query->from('#__users AS u');
		$query->join('INNER', '#__content AS c ON c.created_by = u.id');
		$query->group('u.id, u.name');
		$query->order('u.name');
		$rows = $db->setQuery($query);				
		$rows = $db->loadObjectList();
				
		return $rows;
	}
	
	function get_type_options(){
	
		$types = $this->helper->getItemtypes(0);
		
		//rebuild for sorting by label and take empty one out (on fresh install)
		$temp = array();
		for($n = 0; $n < count($types); $n++){
			if($types[$n]!=''){		
				$temp[] = array($types[$n], $this->helper->translate_item_type($types[$n]));
			}
		}
				
		//sorting by label, case insensitive
		$column = array();		
		foreach($temp as $sortarray){
			$column[] = $sortarray[1];	
		}
		$sort_order = SORT_ASC;
		$column_lowercase = array_map('strtolower', $column);
		array_multisort($column_lowercase, $sort_order, $temp);		
		
		//make options
		$options = array();
		for($n = 0; $n < count($temp); $n++){
			$options[] = JHtml::_('select.option', $temp[$n][0],  $temp[$n][1]);
		}
				
		return $options;
	}
	
	

}
?>