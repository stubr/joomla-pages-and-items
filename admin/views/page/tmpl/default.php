<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

$view = JFactory::getApplication()->input->get( 'view', 'page'); 
$sub_task = JFactory::getApplication()->input->get( 'sub_task', ''); 
$menutype = JFactory::getApplication()->input->get('menutype', ''); 
$pageType = JFactory::getApplication()->input->get('pageType', ''); 

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHTML::_('behavior.framework');//mootools');
JHTML::_('behavior.modal');
if(version_compare(JVERSION, '3', '>')){
	JHtml::_('formbehavior.chosen', 'select');
}
?>
<?php
	echo $this->reload;
?>
<?php //echo PagesAndItemsHelper::sidebar(); ?>
<?php //echo PagesAndItemsHelper::sidebarHtmlBegin(); ?>

<div id="page_content">
<!-- begin id="form_content" need for css-->
<div id="form_content">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td valign="top" class="treeList"> <!--width="20%">-->
		<?php
			//display the page-tree
			//incl. htmls/page_tree
			echo $this->pageTree;
		?>

		</td>
		<td valign="top">
			<form class="form-validate form-horizontal" name="adminForm" id="adminForm" method="post" action="">
			<?php
			//if($sub_task == 'edit')
			if($sub_task != 'new')
			{
			?>
				<script language="javascript" type="text/javascript">
				<?php
				if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
				{
					$joomlaSubmit = '';
					//echo 'function submitbutton(pressbutton)'."\n";
				?>
					function submitbutton(pressbutton)
				<?php
				}
				else
				{
					$joomlaSubmit = 'Joomla.';
					//echo 'Joomla.submitbutton = function(pressbutton)'."\n";
				?>
					Joomla.submitbutton = function(pressbutton,type)
					//Joomla.submitbutton = function(pressbutton)
				<?php
				}
				?>
					{
						//alert(pressbutton);
						var check = false;
						if(pressbutton == 'newMenuItem')
						{
						<?php
						//TODO also for extensions/htmls/page_childs/menuitemtypeselect
						if($this->useCheckedOut && $sub_task == 'edit')
						{
						?>
							alert('Not in Edit-Mode');
							return;
						<?php
						}
						?>
							
							//document.getElementById('page_reload').style.display  = 'block';
							document.getElementById('sub_task').value = 'new';
							document.getElementById('task').value = 'page.page_new'; //pressbutton;
							document.adminForm.submit();
							<?php
							if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
							{
								echo 'document.adminForm.submit();'."\n";
							}
							else
							{
								echo "Joomla.submitform('page.page_new',document.getElementById('adminForm'));"."\n";
								//echo 'Joomla.submitform();'."\n";
							}
							?>
							return;
						}
						<?php
						if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
						{
							echo 'check=true;'."\n";
						}
						else
						{
						?>
						if (pressbutton == 'page.cancel')
						{
							document.getElementById('sub_task').value = 'edit';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform( pressbutton );
							return;
						}
						if(document.formvalidator.isValid(document.id('adminForm')))
						{
							var check = true;
						}
						<?php
						}
						?>
						//alert(check);
						
						//add this?
						if (pressbutton == 'item.setType' || pressbutton == 'item.setMenuType') 
						{
							if(pressbutton == 'item.setType') 
							{
								document.id('adminForm').elements['jform[type]'].value = type;
								document.id('fieldtype').value = 'type';
							}
							else
							{
								document.id('adminForm').elements['jform[menutype]'].value = type;
							}
							document.id('sub_task').value = 'edit';
							Joomla.submitform('page.setType', document.id('adminForm'));
						}
						//add this? END
						
						if (pressbutton == 'page_move_select')
						{
							document.location.href = 'index.php?option=com_pagesanditems&view=page_move_select&pageId='+<?php echo $this->pageId; ?>+'&menutype='+'<?php echo $this->menutype; ?>';
							return;
						}
						if (pressbutton == 'page.page_delete')
						{
							<?php
							if($this->pageId==PagesAndItemsHelper::get_homepage()){
								echo "alert('".addslashes(JText::_('COM_PAGESANDITEMS_CAN_NOT_DELETE_HOMEPAGE'))."');";
								echo "return;";
							}else{
							?>	
								are_you_sure = '<?php
								$confirm_delete = addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_DELETE2')).'?'.'\n\n';
								$confirm_delete .= addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_DELETE1')).':\n';
								$confirm_delete .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH4')).'\n';
								//if($this->helper->config['page_delete_cat']){
								if(PagesAndItemsHelper::getConfigAsRegistry()->get('page_delete_cat',0)){
									$confirm_delete .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_IF_PAGE')).' '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH5')).'\n';
								}
								//if($this->helper->config['page_delete_items']){
								if(PagesAndItemsHelper::getConfigAsRegistry()->get('page_delete_items',0)){
									$confirm_delete .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_IF_PAGE')).' '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH6')).'\n';
								}
								echo $confirm_delete;
								?>';
								if(confirm(are_you_sure)){
	
									document.getElementById('task').value = pressbutton;
									document.adminForm.submit();
									<?php //echo $joomlaSubmit;?>//submitform('page.page_delete');
								}
							<?php
							}
							?>
						}
						if (pressbutton == 'page.page_trash'){
							<?php
							if($this->pageId==PagesAndItemsHelper::get_homepage()){
								echo "alert('".addslashes(JText::_('COM_PAGESANDITEMS_CAN_NOT_DELETE_HOMEPAGE'))."');";
								echo "return;";
							}else{
							?>							
								are_you_sure = '<?php
								$confirm_trash = addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH2')).'?'.'\n\n';
								$confirm_trash .= addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH3')).':\n';
								$confirm_trash .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH4')).'\n';
								//if($this->helper->config['page_trash_cat']){
								if(PagesAndItemsHelper::getConfigAsRegistry()->get('page_trash_cat',0)){
									$confirm_trash .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_IF_PAGE')).' '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH5')).'\n';
								}
								//if($this->helper->config['page_trash_items']){
								if(PagesAndItemsHelper::getConfigAsRegistry()->get('page_trash_items',0)){
									$confirm_trash .= '- '.addslashes(JText::_('COM_PAGESANDITEMS_IF_PAGE')).' '.addslashes(JText::_('COM_PAGESANDITEMS_CONFIRM_PAGE_TRASH6')).'\n';
								}
								echo $confirm_trash;
								?>';
								if(confirm(are_you_sure)){
									document.getElementById('task').value = pressbutton;
									document.adminForm.submit();
								}
							<?php
							}
							?>
						}						
						
						if (pressbutton == 'page.page_edit')
						{
							document.getElementById('sub_task').value = 'edit';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							return;
						}
						
						if (pressbutton == 'page.page_checkin')
						{
							if ( document.adminForm.name && document.adminForm.name.value == '' )
							{
								alert( '<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NEED_TITLE')); ?>' );
								return;
							}
							else if(!check)
							{
								return;
							}
							else
							{
								document.getElementById('sub_task').value = 'edit';
								document.getElementById('subsub_task').value = 'apply';
								document.getElementById('task').value = pressbutton;
								document.adminForm.submit();
							}
						}
						
						if (pressbutton == 'page.reorder_save')
						{
							document.getElementById('subsub_task').value = 'save';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
						}
						if (pressbutton == 'page.reorder_apply')
						{
							//alert('apply');
							document.getElementById('subsub_task').value = 'apply';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
						}
						
						
						
						
						if (pressbutton == 'page.page_save')
						{
							if ( document.adminForm.name && document.adminForm.name.value == '' )
							{
								alert( '<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NEED_TITLE')); ?>' );
								return;
							}
							else if(!check)
							{
								return;
							}
							else
							{
								//alert('save');
								//alert('this function must do rewrite');
								//return;
								document.getElementById('subsub_task').value = 'save';
								document.getElementById('task').value = pressbutton;
								document.adminForm.submit();
								<?php
								//echo $joomlaSubmit;
								?>
								//submitform(pressbutton);
							}
						}
						if (pressbutton == 'page.page_apply')
						{
							if ( document.adminForm.name && document.adminForm.name.value == '' )
							{
								alert( '<?php echo JText::_('COM_PAGESANDITEMS_NEED_TITLE'); ?>' );
								return;
							}
							else if(!check)
							{
								return;
							}
							else
							{
								//alert('apply');
								document.getElementById('subsub_task').value = 'apply';
								document.getElementById('task').value = pressbutton;
								document.adminForm.submit();
								//alert('apply1');
								<?php
								//echo $joomlaSubmit;
								?>
								//submitform('page.page_apply');
								//alert('apply1');
							}
						}
					}

					function new_item(page_id){

						
						
						document.getElementById('task').value = 'item.item_new';
						document.adminForm.submit();
						
					}
					
				</script>
			<?php
			}
			else
			{
			?>
				<script language="javascript" type="text/javascript">
					<?php
						if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
						{
							$joomlaSubmit = '';
							echo 'function submitbutton(pressbutton)'."\n";
						}
						else
						{
							$joomlaSubmit = 'Joomla.';
							echo "Joomla.submitbutton = function(pressbutton, type)"."\n";
							//echo 'Joomla.submitbutton = function(pressbutton)'."\n";
						}
					?>
					{
						if(pressbutton == 'newMenuItem')
						{
							//alert('newMenuItemNew');
							document.getElementById('sub_task').value = 'new';
							document.getElementById('task').value = 'page.page_new'; //pressbutton;
							document.adminForm.submit();
							
							<?php
							if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
							{
								echo 'document.adminForm.submit();'."\n";
							}
							else
							{
								echo 'Joomla.submitform(\'page.page_new\');'."\n";
								//echo 'Joomla.submitform();'."\n";
							}
							?>
							return;
						}
						var check = false;
						<?php
						if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
						{
							echo 'check=true;'."\n";
						}
						else
						{
						?>

						<?php
						}
						?>
						
						if (pressbutton == 'item.setType' || pressbutton == 'item.setMenuType') 
						{
							if(pressbutton == 'item.setType') 
							{
								document.id('adminForm').elements['jform[type]'].value = type;
								document.id('fieldtype').value = 'type';
							}
							else
							{
								document.id('adminForm').elements['jform[menutype]'].value = type;
							}
							document.id('sub_task').value = 'new';
							Joomla.submitform('page.setType', document.id('adminForm'));
							
							
							
							
							return;
						}						
						
						if (pressbutton == 'page.cancel')
						{
							document.getElementById('sub_task').value = 'edit';
							//document.getElementById('task').value = pressbutton;
							//document.adminForm.submit();
							<?php echo $joomlaSubmit;?>submitform( pressbutton, document.getElementById('adminForm' ));
							return;
						}
						if(document.formvalidator.isValid(document.id('adminForm')))
						{
							var check = true;
						}
						if (pressbutton == 'page.page_save')
						{
							if ( document.adminForm.name && document.adminForm.name.value == '' )
							{
								alert( '<?php echo JText::_('COM_PAGESANDITEMS_NEED_TITLE'); ?>' );
								return;
							}
							else if(!check)
							{
								return;
							}
							else
							{

								//alert('this function must do rewrite');
								//return;
								document.getElementById('subsub_task').value = 'save';
								document.getElementById('task').value = pressbutton;
								document.adminForm.submit();
								<?php
								//echo $joomlaSubmit;
								?>
								//submitform(pressbutton);
							}
						}
						if (pressbutton == 'page.page_apply')
						{
							if ( document.adminForm.name && document.adminForm.name.value == '' )
							{
								alert( '<?php echo JText::_('COM_PAGESANDITEMS_NEED_TITLE'); ?>' );
								return;
							}
							else if(!check)
							{
								return;
							}
							else
							{
								document.getElementById('subsub_task').value = 'apply';
								document.getElementById('task').value = pressbutton;
								document.adminForm.submit();
							}
						}
					}


				</script>
			<?php
			}			
			if($sub_task != 'new')
			{
			
			?>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td valign="top" class="pagesList"> <!--width="49%" class="pagesList">-->
						<table class="piadminform xadminform" width="98%">
						<?php
							//display the underlayingpages/childs
							echo $this->pageChilds;							
						?>
						</table>
					</td>
					
					<td valign="top" class="itemsList" id="itemsPage"> <!-- width="49%" class="itemsList" id="itemsPage">-->

					<?php
						
						echo $this->pageItems;

					?>
					</td>
				</tr>
			</table>
			<?php
			}

			//mootools script to hide fields and panels as set in PI config
			$fields_to_hide = array();
			
			$all_fields = PagesAndItemsHelper::get_all_page_fields();//$this->helper->get_all_page_fields();
			
			foreach($all_fields as $field){
				$field_right = $field[0];
				$field_id = $field[1];
				$field_type = $field[4];
				//if(!$this->helper->check_display($field_right) && $field_type!='menutype'){
				if(!PagesAndItemsHelper::check_display($field_right) && $field_type!='menutype'){
					$fields_to_hide[] = $field_id;
				}
			}
			if(count($fields_to_hide)){
				echo '<script>'."\n";
				echo 'var fields_array = new Array(';
				$first = 1;
				foreach($fields_to_hide as $field_to_hide){
					if(!$first){
						echo ',';
					}else{
						$first = 0;
					}
					echo '"';
					echo $field_to_hide;
					echo '"';
				}
				echo ');'."\n";
				echo 'window.addEvent(\'domready\', function() {'."\n";
					echo 'for (i = 0; i < fields_array.length; i++){'."\n";
						echo 'var myElement = document.id(fields_array[i]);'."\n";
						echo 'if(myElement){';
						echo 'var parent = myElement.getParent();'."\n";
						if(PagesAndItemsHelper::getJoomlaVersion() >= '3.0'){	
							echo 'var parent = parent.getParent();'."\n";
						}						
						echo 'parent.addClass(\'display_none\');'."\n";
						echo'}';

					echo '}'."\n";
				echo '});'."\n";

				echo '</script>'."\n";
			}			
			
			if($this->isPagePropertys)
			//if($this->menuItem)
			{
				echo $this->loadTemplate('properties');
			}


	
			?>

			<input type="hidden" id="option" name="option" value="com_pagesanditems" />
			<input type="hidden" id="view" name="view" value="<?php echo $view; ?>" />
			<!--<input type="hidden" id="item_type" name="item_type" value="" />-->
			<input type="hidden" id="sub_task" name="sub_task" value="<?php echo $sub_task; ?>" />
			<input type="hidden" id="task" name="task" value="<?php echo $this->task; ?>" />
			<input type="hidden" id="subsub_task" name="subsub_task" value="" />
			<input type="hidden" id="menutype" name="menutype" value="<?php echo $menutype; ?>" />

			<input type="hidden" id="pageTypeType" name="pageTypeType" value="<?php echo $this->pageTypeType; ?>" />
			<input type="hidden" id="pageType" name="pageType" value="<?php echo $this->pageType; ?>" />
			<input type="hidden" id="type" name="type" value="<?php echo $this->type; ?>" />


			<input type="hidden" id="pageId" name="pageId" value="<?php echo $this->pageId; ?>" />
			<input type="hidden" id="categoryId" name="categoryId" value="<?php echo JFactory::getApplication()->input->get('categoryId',0); ?>" />
			
			<input type="hidden" id="fieldtype" name="fieldtype" value="" />
			<?php echo JHtml::_('form.token'); ?>			
			</form>
		</td>
	</tr>
</table>
<!-- end id="form_content" need for css-->
</div>
<!-- end id="page_content"-->
</div>
<?php
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
?>