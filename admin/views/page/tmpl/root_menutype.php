<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('behavior.tooltip');

$sub_task = JRequest::getVar( 'sub_task', 'edit');
$menutype = JRequest::getVar('menutype', '');
$view = JRequest::getVar( 'view', 'page');
$layout = JRequest::getVar( 'layout', null);

$user		= JFactory::getUser();
$canCreateModule	= $user->authorise('core.create',		'com_modules'); //'com_menus');
$canEditModule	= $user->authorise('core.edit',			'com_modules'); //'com_menus');
$canChangeModule	= $user->authorise('core.edit.state',	'com_modules'); //'com_menus');

$canAdminMenu	= $user->authorise('core.admin',		'com_menus');
$canCreateMenu	= $user->authorise('core.create',		'com_menus');
$canEditMenu	= $user->authorise('core.edit',			'com_menus');
$canChangeMenu	= $user->authorise('core.edit.state',	'com_menus');

$lang		= JFactory::getLanguage();
$lang->load('com_modules', JPATH_ADMINISTRATOR, null, false, false)
		||	$lang->load('com_modules', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);




//no checked_out for menutype
		if(!$this->canDoMenutype->get('core.edit') )
		{
			//$this->form->setFieldAttribute('title','type','text');
			$this->form->setFieldAttribute('title','class','readonly');
			$this->form->setFieldAttribute('title','readonly','true');

			//$this->form->setFieldAttribute('menutype','type','text');
			$this->form->setFieldAttribute('menutype','class','readonly');
			$this->form->setFieldAttribute('menutype','readonly','true');


			//$this->form->setFieldAttribute('description','type','text');
			$this->form->setFieldAttribute('description','class','readonly');
			$this->form->setFieldAttribute('description','readonly','true');
		}






// && PagesAndItemsHelper::getIsSuperAdmin()
//Todo edit only for superadmin?
?>


	<table class="piadminform xadminform" width="98%">
		<thead class="piheader">
		<tr>
			<th> <!-- class="piheader">--><!--style="background: none repeat scroll 0 0 #F0F0F0;border-bottom: 1px solid #999999;">-->
				<!--<img style="vertical-align: middle;" alt="" src="/administrator/components/com_pagesanditems/media/images/icons/icon-16-menu_edit.png">-->
				<img style="vertical-align: middle;" alt="" src="<?php echo PagesAndItemsHelper::getDirIcons();?>/icon-16-menu_edit.png">
				Menutype Properties
			</th>
		</tr>
		</thead>
		<tbody>	
		<tr>
			<td>			
				<fieldset class="adminform">
					<legend><?php echo JText::_('COM_MENUS_MENU_DETAILS');?></legend>
						
						<?php 
						if($this->helper->getJoomlaVersion() >= '3.0'){
						?>
							<div>
								<div class="control-group">
									<div class="control-label">
										<?php echo $this->form->getLabel('title'); ?>
									</div>
									<div class="controls">
										<?php echo $this->form->getInput('title'); ?>
									</div>
								</div>
								<div class="control-group">
									<div class="control-label">
										<?php echo $this->form->getLabel('menutype'); ?>
									</div>
									<div class="controls">
										<?php echo $this->form->getInput('menutype'); ?>
									</div>
								</div>
								<div class="control-group">
									<div class="control-label">
										<?php echo $this->form->getLabel('description'); ?>
									</div>
									<div class="controls">
										<?php echo $this->form->getInput('description'); ?>
									</div>
								</div>	
								<?php							
								if(!$this->form->getValue('menutype') && PagesAndItemsHelper::getIsSuperAdmin()){
										$element = new JXMLElement('<field ></field>');
										$element->addAttribute('name', 'enabled');
										$element->addAttribute('type', 'radio');
										$element->addAttribute('label', 'Enabled in PI');
										$element->addAttribute('default', '0');
										$option = $element->addChild('option','JNO');
										$option->addAttribute('value', '0');
										$option = $element->addChild('option','JYES');
										$option->addAttribute('value', '1');
										$this->form->setField($element);
									?>
									<div class="control-group">
										<div class="control-label">
											<?php echo $this->form->getLabel('enabled'); ?>
										</div>
										<div class="controls">	
											<?php echo $this->form->getInput('enabled'); ?>
										</div>
									</div>
								<?php
								}
								?>
							</div>
						<?php
						}else{
						?>
							<ul class="adminformlist">
								<li>
									<?php echo $this->form->getLabel('title'); 
									echo $this->form->getInput('title'); ?>
								</li>
								<li>
									<?php echo $this->form->getLabel('menutype'); 
									echo $this->form->getInput('menutype'); ?>
								</li>
								<li>
									<?php echo $this->form->getLabel('description'); 
									echo $this->form->getInput('description'); ?>
								</li>
	
								<?php							
								if(!$this->form->getValue('menutype') && PagesAndItemsHelper::getIsSuperAdmin()){
										$element = new JXMLElement('<field ></field>');
										$element->addAttribute('name', 'enabled');
										$element->addAttribute('type', 'radio');
										$element->addAttribute('label', 'Enabled in PI');
										$element->addAttribute('default', '0');
										$option = $element->addChild('option','JNO');
										$option->addAttribute('value', '0');
										$option = $element->addChild('option','JYES');
										$option->addAttribute('value', '1');
										$this->form->setField($element);
									?>
									<li>
										<?php echo $this->form->getLabel('enabled');
										echo $this->form->getInput('enabled'); ?>
									</li>
								<?php
								}
								?>
							</ul>
							
							
							
						<?php
						}
						?>
						
				</fieldset>

				<?php
				$uri = JFactory::getUri();
				$return = base64_encode($uri);
				$addModule = '';				
				
				$modMenuId = 0;
				if(version_compare(JVERSION, '2.5', 'ge'))
				{
					require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'menus.php');
					$modelMenutypes = new MenusModelMenus();
					$modMenuId = (int) $modelMenutypes->getModMenuId();
				}
				
				if (isset($this->modules[$this->menutypeItem->menutype]) || $modMenuId) :
				echo '<fieldset class="adminform">';
						echo '<legend>';
							echo JText::_('COM_MENUS_HEADING_LINKED_MODULES');
						echo '</legend>';
				endif;
				
				if (isset($this->modules[$this->menutypeItem->menutype])) :

					$link = 'index.php?option=com_pagesanditems'; //.$option;
					$link .= '&amp;task=extension.doExecute';
					$link .= '&amp;extension=menutype';
					$link .= '&amp;extensionType=html';
					$link .= '&amp;extensionFolder=page_tree';
					$link .= '&amp;view=module';
					$link .= '&amp;tmpl=component';
				
						echo '<ul>';
							if($addModule)
							{
								echo '<li>';
									echo $addModule;
								echo '</li>';
							}

							foreach ($this->modules[$this->menutypeItem->menutype] as &$module) :
							?>
								<li>
								<?php if ($canEditModule) : ?>
								<a class="modal" href="<?php echo JRoute::_('index.php?option=com_modules&task=module.edit&id='.$module->id.'&return='.$return.'&tmpl=component&layout=modal');?>" rel="{handler: 'iframe', size: {x: 1024, y: 450}, onClose: function() {window.location.reload()}}"  title="<?php echo JText::_('COM_MENUS_EDIT_MODULE_SETTINGS');?>">
								<?php echo JText::sprintf('COM_MENUS_MODULE_ACCESS_POSITION', $this->escape($module->title), $this->escape($module->access_title), $this->escape($module->position)); ?></a>
								<?php else :?>
								<?php echo JText::sprintf('COM_MENUS_MODULE_ACCESS_POSITION', $this->escape($module->title), $this->escape($module->access_title), $this->escape($module->position)); ?>
								<?php endif; ?>
								</li>
							<?php
						
							endforeach;
						echo '</ul>';
					
				elseif ($modMenuId) : 
					
					
					?>
					<a class="modal" href="<?php echo JRoute::_('index.php?option=com_modules&task=module.add&eid=' . $modMenuId . '&params[menutype]='.$this->menutypeItem->menutype.'&return='.$return.'&tmpl=component&layout=modal');?>" rel="{handler: 'iframe', size: {x: 1024, y: 450}, onClose: function() {window.location.reload()}}">
						<?php echo JText::_('COM_MENUS_ADD_MENU_MODULE'); ?></a>
					<?php 
				endif;
				
				if (isset($this->modules[$this->menutypeItem->menutype]) || $modMenuId) :
					echo '</fieldset>';
				endif;
				
?>
			</td>
		</tr>
		</tbody>
	</table>