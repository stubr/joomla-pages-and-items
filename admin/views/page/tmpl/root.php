<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

$db = JFactory::getDBO();

/*
$db = JFactory::getDBO();
$query = $db->getQuery(true);
$query->select('id, title');
$query->from('#__content');
$query->where('introtext LIKE "youtube"');
$rows = $db->setQuery($query);				
$rows = $db->loadObjectList();
	
foreach($rows as $row){	
	echo '<br />';
	echo '<a href="index.php?option=com_content&task=article.edit&id='.$row->id.'">';	
	echo $row->title;
	echo '</a>';	
}
*/





/*
$query = $db->getQuery(true);		
$query->update('#__phocaguestbook_items');
$query->set('catid='.$db->q('256'));
$db->setQuery((string)$query);
$db->query();
*/

JHTML::_('behavior.tooltip');

$sub_task = JFactory::getApplication()->input->get( 'sub_task', ''); 
$menutype = JFactory::getApplication()->input->get('menutype', ''); 
$pageType = JFactory::getApplication()->input->get('pageType', ''); 
$view = JFactory::getApplication()->input->get( 'view', 'page'); 
$layout = JFactory::getApplication()->input->get( 'layout', null); 
$subsub_task = JFactory::getApplication()->input->get('subsub_task', ''); 
JHtml::_('behavior.formvalidation');
?>
<?php
	echo $this->reload;
?>
<div id="page_content">
<!-- begin id="form_content" need for css-->
<div id="form_content">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>

		<td valign="top"  class="treeList"> <!--width="20%">-->
		<?php
			//display the page-tree
			//incl. htmls/page_tree
			//TODO
			echo $this->pageTree;
			//echo $this->getPages();
		?>
		</td>
		<td valign="top">
			<form class="form-validate form-horizontal" name="adminForm" id="adminForm" method="post" action="">
				<script language="javascript" type="text/javascript">
					<?php
						if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
						{
							$joomlaSubmit = '';
							echo 'function submitbutton(pressbutton)'."\n";
						}
						else
						{
							$joomlaSubmit = 'Joomla.';
							echo 'Joomla.submitbutton = function(pressbutton)'."\n";
						}
					?>
					{

						//root
						//alert(pressbutton);
						if(pressbutton == 'newMenuItem')
						{
							//document.getElementById('page_reload').style.display  = 'block';
							document.getElementById('sub_task').value = 'new';
							document.getElementById('task').value = 'page.page_new'; //pressbutton;
							
							
							//alert('new');
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
							//document.adminForm.submit();
						}
						/*
						if (pressbutton == 'page.root_underlayingpages')
						{
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
						}
						*/
						if (pressbutton == 'menutype.root_save')
						{
							//alert('must write');
							document.getElementById('subsub_task').value = 'save';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
						}
						
						
						if (pressbutton == 'menutype.root_menutype_new')
						{
							//document.getElementById('subsub_task').value = 'menutype';
							document.getElementById('sub_task').value = 'newMenutype';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
						}
						if (pressbutton == 'menutype.root_cancel')
						{
							//document.getElementById('subsub_task').value = 'cancel';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							<?php
							//	echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
						}
						if(document.formvalidator.isValid(document.id('adminForm')))
						{
							var check = true;
						}
						
						if (pressbutton == 'page.root_apply' && check)
						{
							document.getElementById('sub_task').value = 'apply';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
						}
						else if(pressbutton == 'page.root_apply' && !check)
						{
							return false
						}
						
						if (pressbutton == 'menutype.root_menutype_save' && check)
						{
							//document.getElementById('subsub_task').value = 'menutype';
							document.getElementById('sub_task').value = 'save';
							document.getElementById('task').value = pressbutton;
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
						}
						else if(pressbutton == 'menutype.root_menutype_save' && !check)
						{
							return false
						}
						if (pressbutton == 'menutype.root_menutype_apply' && check)
						{
							//document.getElementById('subsub_task').value = 'menutype';
							document.getElementById('sub_task').value = 'apply';
							document.getElementById('task').value = 'menutype.root_menutype_save';
							document.adminForm.submit();
							<?php
							//echo $joomlaSubmit;
							?>
							//submitform(pressbutton);
						}
						else if(pressbutton == 'menutype.root_menutype_apply' && !check)
						{
							return false
						}						
					}
					
				</script>
			<?php			
			if($sub_task != 'new' && $sub_task != 'newMenutype')
			{
			?>
			
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td valign="top"class="pagesList"> <!--width="49%" class="pagesList">-->
						<table class="piadminform xadminform" width="98%">

							<?php
							if($sub_task == 'edit' || $sub_task == '')
							{
								echo $this->pageChilds;								
							}
							?>
						</table>
					</td>					
					<td class="itemsList" id="itemsPage">
					</td>
				</tr>
			</table>

			<?php
			}			
			
			if($this->menuItem)
			{
				echo $this->loadTemplate('properties');
			}			
		
			if($this->menutypeItem )
			{
				echo $this->loadTemplate('menutype');
			}

			?>
			<input type="hidden" id="option" 		name="option" value="com_pagesanditems" />
			<input type="hidden" id="view" 		name="view" value="<?php echo $view; ?>" />
			<input type="hidden" id="sub_task" 		name="sub_task" value="<?php echo $sub_task; ?>" />
			<input type="hidden" id="task" 		name="task" value="" />
			<input type="hidden" id="subsub_task" 	name="subsub_task" value="" />
			<input type="hidden" id="menutype" 		name="menutype" value="<?php echo $menutype; ?>">
			<?php
				if($layout)
				{
					echo '<input type="hidden" id="layout" name="layout" value="'.$layout.'">';
				}
			?>			

			<input type="hidden" id="pageTypeType" name="pageTypeType" value="<?php echo $this->pageTypeType; ?>" />
			<input type="hidden" id="pageType" name="pageType" value="<?php echo $this->pageType; ?>" />
			<input type="hidden" id="type" name="type" value="<?php echo $this->type; ?>" />
			<input type="hidden" id="pageId" name="pageId" value="<?php echo $this->pageId; ?>">
			<input type="hidden" id="menutypeId" name="menutypeId" value="<?php echo isset($this->menutypeItem->id) ? $this->menutypeItem->id : 0; ?>">
			<?php echo JHtml::_('form.token'); ?>
			</form>
		</td>
	</tr>
</table>
</div>
</div>
<?php
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
?>