<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );
if(PagesAndItemsHelper::getIsAdmin()){ //$this->helper->app->isAdmin()){
	//only show footer in backend
	/*
	$document = JFactory::getDocument();
	$path = PagesAndItemsHelper::getDirCSS(true);
	//$document->addStylesheet('components/com_pagesanditems/css/pagesanditems3.css');
	$document->addStylesheet($path.'/pagesanditems3.css');
	*/
	$path = PagesAndItemsHelper::getDirCSS();
	JHtml::stylesheet($path.'/pagesanditems3.css');
	if(version_compare(JVERSION, '3', '>=')) {JHTML::stylesheet($path.'/pagesanditemsj3.css');}
			//TODO add JHTML::stylesheet($path.'/pagesanditemsj3.css');
		//TODO add JHTML::stylesheet($path.'/pagesanditemsj3_icons.css');
	//$document->addStylesheet('components/com_pagesanditems/css/dtree.css');

	echo '<div class="smallgrey" id="pi_footer">';
	echo '<table>';
	echo '<tr>';
	echo '<td class="text_right">';
	echo '<a href="http://www.pages-and-items.com" target="_blank">Pages-and-Items</a>';
	echo '</td>';
	echo '<td class="five_pix">';
	echo '&copy;';
	echo '</td>';
	echo '<td>';
	echo '2008 - 2018 Carsten Engel / 2018 Sturm und Bräm GmbH';
	echo '</td>';
	echo '</tr>';
	echo '<tr>';
	echo '<td class="text_right">';
	echo PagesAndItemsHelper::pi_strtolower(JText::_('JVERSION'));//$this->helper->pi_strtolower(JText::_('JVERSION'));
	echo '</td>';
	echo '<td class="five_pix">';
	echo '=';
	echo '</td>';
	echo '<td>';
	//echo $version.' ('.$this->fua_version_type.' '.$this->fua_strtolower(JText::_('JVERSION')).')';
	echo PagesAndItemsHelper::getPagesAndItemsVersion(); //$this->helper->version;
	//if($this->fua_version_type!='trial'){
		//echo ' <a href="http://www.gnu.org/licenses/gpl-2.0.html" target="blank">GNU/GPL License</a>';
	//}
	echo '</td>';
	echo '</tr>';
	echo '</table>';
	echo '</div>';
}
?>
