<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//jimport( 'joomla.application.component.view');


jimport('joomla.application.component.view');

if (!class_exists('PagesAndItemsViewBase')) {
    if (interface_exists('JView')) {

        abstract class PagesAndItemsViewBase extends JViewLegacy {
            
        }

    } else {

        abstract class PagesAndItemsViewBase extends JView {
            
        }

    }
}


/**
 * HTML View class for the Plugins component

 */
class PagesAndItemsViewDefault extends PagesAndItemsViewBase
{
	function display( $tpl = null )
	{
		require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagesanditems.php';

		$helper = new pagesanditemsHelper();
		$this->assignRef('helper', $helper);

		$pi_config = PagesAndItemsHelper::getConfig();
		$this->assignRef('pi_config', $pi_config);

		//$model =$this->getModel('base');
		//if($model->isAdmin)
		$isAdmin = PagesAndItemsHelper::getIsAdmin();
		if($isAdmin)
		{

			if(!defined('COM_PAGESANDITEMS_TITLE_IS_SET'))
			{
				PagesAndItemsHelper::addTitle();
			}
		}
		//$controller->display_footer();
		//$this->controller->set_title();

		//here we set the title
		//can override from child
		//$this->setTitle();

		parent::display($tpl);
	}

	function setTitle()
	{
		if(PagesAndItemsHelper::getIsAdmin()) //$this->controller->is_admin)
		{
			//require_once JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagesanditems.php';
			PagesAndItemsHelper::addTitle(null);
		}
	}

}