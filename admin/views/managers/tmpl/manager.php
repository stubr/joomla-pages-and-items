<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if(!defined('_JEXEC'))
{
	die('Restricted access');
}
/*
this is for extensions/managers

*/

$item_type = JFactory::getApplication()->input->get('item_type', '' ); 
$sub_task = JFactory::getApplication()->input->get('sub_task', '' ); 
$pageId = JFactory::getApplication()->input->get('pageId', '' ); 
$item_id = JFactory::getApplication()->input->get('itemId', '' ); 

$showPageTree = JFactory::getApplication()->input->get('showPageTree', 0 ); 
$showFooter = JFactory::getApplication()->input->get('showFooter', 1 ); 
$tmpl = JFactory::getApplication()->input->get('tmpl', 0 ); 
$popup = JFactory::getApplication()->input->get('popup', 0 ); 

$extensionName = JFactory::getApplication()->input->get('extensionName',JFactory::getApplication()->input->get('extension', '' ));
$extensionType = JFactory::getApplication()->input->get('extensionType', ''); 
$extensionFolder = JFactory::getApplication()->input->get('extensionFolder', ''); 
$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..');

if($extensionType != '')
{
	require_once($path.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.$extensionType.'helper.php');
	$typeName = 'Extension'.ucfirst($extensionType).'Helper';
	$typeName::importExtension($extensionFolder, $extensionName,true,null,true);
}
$dispatcher =JDispatcher::getInstance();


if($popup)
{

	/*
	we will set something here like
	header with image ....

	an javascript to set the dimensions

	*/
	$headerTitle = JText::_( 'Pages and Items');
	$headerImage = PagesAndItemsHelper::getDirIcons().'icon-32-pi.png';

	$results = $dispatcher->trigger('onDisplay_HeaderImage', array(&$headerImage,$extensionName); //,$this->model));
	$results = $dispatcher->trigger('onDisplay_HeaderTitle', array(&$headerTitle,$extensionName); //,$this->model));


?>



<!-- begin id="form_content" need for css-->
<div id="form_content">
<form name="adminForm" method="post" action="" enctype="multipart/form-data" id="adminForm">
<fieldset id="fieldset_top">
<?php echo PagesAndItemsHelper::getHeaderImageTitle($headerImage,$headerTitle); ?>
		<?php
		/*
<div class="formHeader">

	<h1 class="pi_h1">
		<img src="<?php echo $headerImage; ?>" alt="..." class="pi_icon" />
		<?php echo '&nbsp;'.$headerTitle; ?>
	</h1>
</div>
*/
?>
</fieldset>
<fieldset id="fieldset_content">
<div id="formContent" class="formContent">
<?php

	//$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,JUri::root(),realpath(dirname(__FILE__).'/../../../')));
	//JHTML::script('popup_extension.js', $path.'/javascript/',false);

	//here we need an path to the component dir javascript
	$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).'/../../../')));
	JHTML::script($path.'/javascript/popup_extension.js',false);
	//here we get an absolute path like /administrator/components/com_pagesanditems/javascript/popup_extension.js
	//here we need an relative path
	$path = str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).'/../../../')));
// TODO CHECK 
	echo '<link href="'.JUri::root(true).'/'.$path.'/css/pagesanditems3.css" " rel="stylesheet" type="text/css" />'."\n";
	if(version_compare(JVERSION, '3', '>=')) {JHTML::stylesheet(JUri::root(true).'/'.$path.'/css/pagesanditemsj3.css');}

// TODO CHECK 
	echo '<link href="'.JUri::root(true).'/'.$path.'/css/dtree.css" rel="stylesheet" type="text/css" />'."\n";
// TODO CHECK 
	echo '<link href="'.JUri::root(true).'/'.$path.'/css/pages_and_items_extension.css" rel="stylesheet" type="text/css" />'."\n";
	/*
	TODO calc content height -90px

	*/
}
?>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<?php
	if($showPageTree)
	{
		echo '<td valign="top" width="20%">';
			echo $this->pageTree;
		echo '</td>';
	}
	?>
		<td valign="top" style="  font-size: 100% !important;font-family: monospace;">



			<input type="hidden" name="option" id="option" value="com_pagesanditems" />
			<input type="hidden" id="task" name="task" value="extension.display" />
			<input type="hidden" name="pageId" id="pageId" value="<?php echo $pageId; ?>">
			<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
			<input type="hidden" name="itemId" value="<?php echo $item_id; ?>">
			<input type="hidden" name="item_type" value="<?php echo $item_type; ?>">

			<input type="hidden" name="extensionName" id="extensionName" value="<?php echo $extensionName; ?>">
			<input type="hidden" name="extensionType" id="extensionType" value="<?php echo $extensionType; ?>">
			<input type="hidden" name="extensionFolder" id="extensionFolder" value="<?php echo $extensionFolder; ?>">


			<?php

			//$content = new JObject();
			$content = new JObject();
			$content->text = '';
			$results = $dispatcher->trigger('onDisplayContent', array(&$content,$extension,$sub_task); //,$this->model));
			echo $content->text;

			?>
		</td>
	</tr>
</table>
<?php
if($popup)
{
	/*
	we will set something here like
	an cancle button as default
	*/
?>
</div>
</fieldset>
<fieldset id="fieldset_bottom">
<div class="formFooter">
<?php
	/*
	$htmlButton = '<button style="float:right;" class="button_action" name="close-button" id="button_close" type="button" onclick="window.parent.document.getElementById(\'sbox-window\').close();">';
	$htmlButton .= JText::_('Cancel');
	$htmlButton .= '</button>';
	*/
	$button = PagesAndItemsHelper::getButtonMaker('close');
	$button->onclick = 'window.parent.document.getElementById(\'sbox-window\').close();';
	$button->style = 'float:right;';
	$htmlButton = $button->makeButton();

	$results = $dispatcher->trigger('onHtmlDisplay_Button', array(&$htmlButton,$extensionName); //,$this->model));
	echo $htmlButton;
?>

</div>
</fieldset>
</form>
<!-- end id="form_content" need for css-->
</div>
<?php
}
?>

<?php
if($showFooter && !$tmpl)
{
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');
}
?>