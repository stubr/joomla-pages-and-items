<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

defined('_JEXEC') or die;
?>

<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<?php
$fields = $this->form->getFieldset('item_associations');

foreach ($fields as $field) :
?>
<div class="control-group">
	<div class="control-label">
		<?php echo $field->label ?>
	</div>
	<div class="controls">
		<?php echo $field->input; ?>
	</div>
</div>
<?php endforeach; ?>
<?php else:?>
<?php
$fields = $this->form->getFieldset('item_associations');
if(count($fields))
{
echo JHtml::_('sliders.panel',JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS'), 'associations');
foreach ($fields as $field) :
?>
<fieldset class="panelform">
<div class="control-group">
	<div class="control-label">
		<?php echo $field->label ?>
	</div>
	<div class="controls">
		<?php echo $field->input; ?>
	</div>
</div>
</fieldset>
<?php endforeach;
} ?>
<?php endif;?>
