<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined( '_JEXEC' ) or die();

jimport( 'joomla.application.component.view');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'page'.DIRECTORY_SEPARATOR.'view.html.php');

class PagesAndItemsViewItemtypeselect extends PagesAndItemsViewDefault
{

	function display( $tpl = null ){
	
		$document = JFactory::getDocument();	
		$document->addStyleSheet('components/com_pagesanditems/css/pagesanditems3.css');
		if(version_compare(JVERSION, '3', '>=')) {JHTML::stylesheet('components/com_pagesanditems/css/pagesanditemsj3.css');}
				//TODO add JHTML::stylesheet($path.'/pagesanditemsj3.css');
		//TODO add JHTML::stylesheet($path.'/pagesanditemsj3_icons.css');
		parent::display($tpl);
	}
}

?>