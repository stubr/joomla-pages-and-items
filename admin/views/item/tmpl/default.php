<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//no direct access
if( !defined('_JEXEC')){
	die('Restricted access');
}

require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_content'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'contentadministrator.php');

//declare the var to hide fields etc.
//$display_none = 'class="display_none"';
$app = JFactory::getApplication();
$db = JFactory::getDBO();
/*
$input = $app->input;
$assoc = isset($app->item_associations) ? $app->item_associations : 0;
// Create shortcut to parameters.
$params = $this->state->get('params');
$params = $params->toArray();
*/
// Load the tooltip behavior.


JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHTML::_('behavior.framework');
JHtml::_('behavior.calendar');

if(version_compare(JVERSION, '3', '>')){
	JHtml::_('formbehavior.chosen', 'select');
}


/*
if(!PagesAndItemsHelper::getIsAdmin())
{
	$frontend = 1;
}
else
{
	$frontend = 0;
}
*/
?>


<!-- begin id="form_content" need for css-->
<div id="form_content">
<?php

$pageType = JFactory::getApplication()->input->get('pageType',''); 

//menutype from menu-item
$menutype = $this->menutype;

$pageId = JFactory::getApplication()->input->get('pageId',0); 
$categoryId = JFactory::getApplication()->input->get('categoryId', '' ); 
//get item_id
if($this->frontend)
{
	$this->item_id = JFactory::getApplication()->input->get('item_id', '' ); 
}
else
{
	$this->item_id = JFactory::getApplication()->input->get('itemId', '' ); 
}
$this->item_id = JFactory::getApplication()->input->get('itemId', JFactory::getApplication()->input->get('item_id', '' )); 


$sub_task = JFactory::getApplication()->input->get('sub_task',''); 
if($sub_task==''){
	if($this->item_id==''){
		$sub_task = 'new';
	}else{
		if(!$this->useCheckedOut)
		{
			$sub_task = 'edit';
		}
	}
}

//PI ACL
//this stuff is not in the view because it is also used for the frontend
if($sub_task=='new' && !$this->item_id){
	//new item
	PagesAndItemsHelper::to_previous_page_when_no_permission('3'); //
}else{
	//edit item
	PagesAndItemsHelper::to_previous_page_when_no_permission('4'); //$this->helper->
}

if($this->frontend){
	//get extra css to style frontend
	$doc =JFactory::getDocument();
	$doc->addStylesheet('components/com_pagesanditems/css/frontend_edit.css'); //same as addScript but for css
}


//get page_id
//$page_id = JFactory::getApplication()->input->get('pageId', '' ); 
//get date and time

$datenow = PagesAndItemsHelper::get_date_now(true);
$config = PagesAndItemsHelper::getConfig();
$this->show_title_item = true;

$show_tree = true;
//switch for new and edit
if($sub_task=='new')
{
	//begin getting data for new item
	$popup = JFactory::getApplication()->input->get('tmpl', null ); 
	if($popup && $popup == 'component')
	{
		$show_tree = false;
	}

	//get category id of menuitem
	$cat_id = '';
	$cat_ids = 0;
	$section_id = '';
	if(!$this->frontend)
	{
		$this->db->setQuery("SELECT * FROM #__menu WHERE id='$pageId' LIMIT 1");
		$row = $this->db->loadObject();
		if($row)
		{
			if(strpos($row->link, 'index.php?option=com_content&view=category&layout=blog') !== FALSE)
			{			
				$cat_id = str_replace('index.php?option=com_content&view=category&layout=blog&id=','',$row->link);
			}
			elseif(strpos($row->link, 'index.php?option=com_content&view=category') !== FALSE)
			{
				$cat_id = str_replace('index.php?option=com_content&view=category&id=','',$row->link);
			}
			elseif(strpos($row->link, 'index.php?option=com_content&view=featured') !== FALSE)
			{
				/*
				get the $row->params 
				*/
				$row_params = json_decode($row->params);
				
				if(isset($row_params->featured_categories))
				{

					$cat_ids = $row_params->featured_categories;
					if(is_array($cat_ids) && count($cat_ids) && $cat_ids[0] != '' )
					{
						$this->form->setFieldAttribute('catid', 'catids', json_encode($cat_ids));
						$this->form->setFieldAttribute('catid', 'type', 'FeaturedCategory');
					}
				}
				
				$cat_id = 0;
				$this->form->setFieldAttribute('featured','default','1');
			}
			else
			{
				$cat_id = 0;
			}
			
			

		}
		else
		{
			$cat_id = 0;
		}

		
		$section_id = 0;
		
	}
		

	//get category and section id from url
	if($this->frontend){
		$section_id = intval(JFactory::getApplication()->input->get('section', '' )); 
		$cat_id = intval(JFactory::getApplication()->input->get('category', '' )); 
		$hide_page_select = JFactory::getApplication()->input->get('hide_select', '' ); 
		
		
	}	
	
	
	
	//$categoryId = $cat_id;
		
	//fix for when $cat_id = 0
	//that does not set the category select to anything
	//there is no form validation for if no category is selected, so set the default to the first category-item
	if($cat_id==0){
		//get categories		
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__categories');
		$query->where('extension='.$db->q('com_content'));		
		$query->order('lft');
		$temp_cats = $db->setQuery($query, 0, 1);				
		$temp_cats = $db->loadObjectList();	
				
		foreach($temp_cats as $temp_cat){		
			$cat_id = $temp_cat->id;	
		}	
	}
	//set vars for new item
	$this->item_id = '';
	$itemTitle = '';
	$itemTitleAlias = '';
	$itemIntroText = '';
	$itemFullText = '';
	$text = '';
	$itemCreatedByAlias = '';
	$itemMetadesc = '';
	$itemMetakey = '';
	$user_id = '';
	$created = $datenow;
	$created_by = PagesAndItemsHelper::getUserId();
	$item_publish_up = $datenow;
	$item_publish_down = JText::_('COM_PAGESANDITEMS_NEVER');
	$version = '0';

	$item_type = JFactory::getApplication()->input->get('item_type', 'text'); 
	$this->item_type = $item_type;

	$metadata_thing['robots'] = '';
	$metadata_thing['author'] = '';
	$values = array();
	$values['introtext'] = 1;
	$values['item_title'] = 1;

	$itemAttribs = 'show_title=
show_pdf_icon=
show_print_icon=
show_email_icon=
link_titles=
show_intro=
show_section=
link_section=
show_category=
link_category=
show_vote=
show_author=
show_create_date=
show_modify_date=
keyref=
language=en-GB
readmore=
';

	//explode array attributes

	$itemAttribs = explode( "\n", $itemAttribs);
	for($n = 0; $n < count($itemAttribs); $n++){
		$temp = explode('=',$itemAttribs[$n]);
		$var = $temp[0];
		$value = '';
		if(count($temp)==2){
			$value = $temp[1];
		}
		$values[$var] = trim($value);
	}


	//end getting data for new item
	?>
<script language="javascript" type="text/javascript">
<!-- 

<?php
//needed to rename the function for frontend because the javascript function got overwritten somewhere by the core
if($this->frontend)
{

?>
	function submitbutton2(pressbutton)
	{
<?php
}
else
{
?>
	Joomla.submitbutton = function(pressbutton)
	{
<?php
}
?>
		if (pressbutton == 'item.cancel')
		{
			document.getElementById('task').value = pressbutton;
			document.adminForm.submit();
			return;
		}

		if (pressbutton == 'item.item_apply')
		{
			document.getElementById('item_apply').value = 1;
		}
		if (pressbutton == 'item.item_apply' || pressbutton == 'item.item_save' || pressbutton == 'item.item_checkin' || pressbutton == 'item.save2new' || pressbutton == 'item.save2copy')
		{
			//alert('save apply');
			item_title = document.getElementById('jform_title').value;
			<?php
			if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
			{
			?>
			item_title = trim(item_title);
			<?php
			}
			else
			{
			?>
				item_title = item_title.trim();
			<?php
			}
			?>
			if (item_title == '')
			{
				alert('<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NO_TITLE')); ?>');
				return;
			}
<?php
			//if itemtype is 'other_item' do validation
			if($item_type=='other_item'){
			?>
			if(document.adminForm.other_item_id.value == '0'){
				alert('<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NO_OTHERITEM_SELECTED')); ?>');
				return;
			}
			<?php
			}

if($this->frontend)
{
?>
			else if(document.adminForm.jform_catid.value == '' || document.adminForm.jform_catid.value=='0')
			{
				alert('<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NO_PAGE_SELECTED')); ?>');
				return;
			}
<?php
}//END if($this->frontend)
?>
			else
			{
<?php
//if custom itemtype
if(strpos($item_type, 'ustom_'))
{
?>
				//validate_custom_itemtype_fields
				if(validate_custom_itemtype_fields())
				{
<?php
//if itemtype plugin
}
elseif($item_type!='content' && $item_type!='text' && $item_type!='html' && $item_type!='other_item')
{
?>
				//validate_itemtype
				if(validate_itemtype())
				{
<?php
}
//if custom itemtype or itemtype plugin
if($item_type!='content' && $item_type!='text' && $item_type!='html' && $item_type!='other_item')
{
?>
					document.getElementById('task').value = pressbutton;
					document.adminForm.submit();
				}
<?php
}
else
{
?>
				document.getElementById('task').value = pressbutton;
				document.adminForm.submit();
<?php
}
?>
			}
		}
	}
-->
</script>
<?php
}
else
{


	//begin getting data to edit item
	//data from item
	$this->db->setQuery("SELECT * FROM #__content WHERE id='$this->item_id' LIMIT 1");
	$rows = $this->db->loadObjectList();
	$row = $rows[0];

	$this->item_id = $row->id;
	$itemTitle = htmlspecialchars($row->title);


	$itemTitleAlias = htmlspecialchars($row->alias);


	$itemIntroText = htmlspecialchars($row->introtext);
	$itemFullText = htmlspecialchars($row->fulltext);
	if($itemFullText!=''){
		$text = $itemIntroText.'<hr id="system-readmore" />'.$itemFullText;
	}else{
		$text = $itemIntroText;
	}

	$itemState = $row->state;
	$cat_id = $row->catid;
	$section_id = '';
	$itemCreatedByAlias = $row->created_by_alias;
	$item_publish_up = $row->publish_up;
	$item_publish_up = PagesAndItemsHelper::get_date_to_format($item_publish_up);
	$item_publish_down = $row->publish_down;
	if($item_publish_down!='0000-00-00 00:00:00'){
		$item_publish_down = PagesAndItemsHelper::get_date_to_format($item_publish_down);
	}
	$itemMetakey = $row->metakey;
	$itemMetadesc = $row->metadesc;
	$itemAttribs = $row->attribs;
	$version = $row->version;
	$itemAccess = $row->access;
	$item_hits = $row->hits;
	$created = $row->created;
	$created = PagesAndItemsHelper::get_date_to_format($created);
	$created_by = $row->created_by;
	$item_modified = $row->modified;
	$item_modified = PagesAndItemsHelper::get_date_to_format($item_modified);

	if($itemAttribs==''){
		$itemAttribs = 'item_title=1
introtext=1
show_title=
show_pdf_icon=
show_print_icon=
show_email_icon=
link_titles=
show_intro=
show_section=
link_section=
show_category=
link_category=
show_vote=
show_author=
show_create_date=
show_modify_date=
keyref=
language=en-GB
readmore=
';

	}
	

	if($item_publish_down=='0000-00-00 00:00:00'){
		$item_publish_down = JText::_('COM_PAGESANDITEMS_NEVER');
	}

	$user_id = PagesAndItemsHelper::getUserId();

	//if author only new items and own items can be editted
	//must rewrite??
	//if($this->model->user_type=='Author'){
		//check if the item to edit is the authors own article
		/*	
		$canDoContent = PagesAndItemsHelper::canDoContent($cat_id, $this->item_id);
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$canEdit	= $canDoContent->get('core.edit');
		$canEditOwn	= $canDoContent->get('core.edit.own') && $created_by == $userId;
		*/
		//if((!$canEdit && !$canEditOwn))
		if(!$this->canEdit)
		{
			echo "<script> alert('".JText::_('COM_PAGESANDITEMS_NOITEMACCESS')."'); window.history.go(-1); </script>";
			exit();
		}

	//explode array attributes
	$itemAttribs = explode( "\n", $itemAttribs);
	for($n = 0; $n < count($itemAttribs); $n++){
		//list($var,$value) = split('=',$itemAttribs[$n]);
		//$values[$var] = trim($value);

		$values_temp = explode('=',$itemAttribs[$n]);
		$var = $values_temp[0];
		$value = str_replace($var.'=','',$itemAttribs[$n]);
		$value = trim($value);
		$values[$var] = $value;
	}

	$metadata = $row->metadata;
	$metadata = explode( "\n", $metadata);
	for($n = 0; $n < count($metadata); $n++)
	{
		//list($var,$value) = split('=',$metadata[$n]);
		//$metadata_thing[$var] = trim($value);

		$values_temp = explode('=',$metadata[$n]);
		$var = $values_temp[0];
		$value = str_replace($var.'=','',$metadata[$n]);
		$value = trim($value);
		$metadata_thing[$var] = $value;
	}


	//get data from item index
	$this->db->setQuery("SELECT * FROM #__pi_item_index WHERE item_id='$this->item_id' LIMIT 1");
	$rows = $this->db->loadObjectList();

	if($rows)
	{
		$row = $rows[0];
		$this->show_title_item = $row->show_title;
		$item_type = $row->itemtype;
	}
	else
	{
		$this->show_title_item = true;
		$item_type = 'text';
	}

	if($item_type=='' || $item_type=='content')
	{
		$item_type = 'text';
	}
	$this->item_type = $item_type;
	//check if item is currently on frontpage
	$this->db->setQuery("SELECT content_id FROM #__content_frontpage WHERE content_id='$this->item_id' LIMIT 1");
	$rows = $this->db->loadObjectList();
	if(count($rows)>0)
	{
		$itemFrontpage = true;
	}else{
		$itemFrontpage = false;
	}

	?>
	<script language="javascript" type="text/javascript">
		<!-- 
		<?php
		//needed to rename the function for frontend because the javascript function got overwritten somewhere by the core
		if($this->frontend){
		?>
		function submitbutton2(pressbutton) {
		<?php
		}else{
		?>

		Joomla.submitbutton = function(pressbutton)
		{
		<?php
		}
		?>
			do_item_save =false;
			if (pressbutton == 'item.cancel') {
				submitform( pressbutton );
				document.getElementById('task').value = 'item.cancel';
				document.adminForm.submit();
				return;
			}

			if (pressbutton == 'item_move_select') {
				document.location.href = 'index.php?option=com_pagesanditems&view=item_move_select&pageId=<?php if($this->frontend){echo 'nothing';}else{echo $pageId;} ?>&item_id=<?php echo $this->item_id; ?>&categoryId=<?php echo $categoryId; ?>';
			}

			if (pressbutton == 'item.item_archive'){
				if(confirm("<?php echo JText::_('COM_PAGESANDITEMS_SURE_ARCHIVE'); ?>")){
					document.getElementById('subsub_task').value = 'archive';
					document.getElementById('task').value = 'item.state';
					document.adminForm.submit();
				}
				return false;
			}

			if (pressbutton == 'item.item_publish') {
				if(confirm("<?php echo JText::_('COM_PAGESANDITEMS_SURE_PUBLISH'); ?>")){
					document.getElementById('subsub_task').value = 'publish';
					document.getElementById('task').value = 'item.state';
					document.adminForm.submit();
				}
				return false;
			}
			if (pressbutton == 'item.item_unpublish') {
				if(confirm("<?php echo JText::_('COM_PAGESANDITEMS_SURE_UNPUBLISH'); ?>")){
					document.getElementById('subsub_task').value = 'unpublish';
					document.getElementById('task').value = 'item.state';
					document.adminForm.submit();
				}
				return false;
			}
			if (pressbutton == 'item.item_trash') {
				if(confirm("<?php echo JText::_('COM_PAGESANDITEMS_SURE_TRASH'); ?>")){
					document.getElementById('subsub_task').value = 'trash';
					document.getElementById('task').value = 'item.state';
					document.adminForm.submit();
				}
				return false;
			}
			
			if (pressbutton == 'item.item_delete') {
				if(confirm("<?php echo JText::_('COM_PAGESANDITEMS_SURE_DELETE'); ?>")){
					document.getElementById('subsub_task').value = 'delete';
					document.getElementById('task').value = 'item.state';
					//document.getElementById('task').value = 'item.item_delete';
					document.adminForm.submit();
				}
				return false;
			}
			if (pressbutton == 'item.item_edit') {
					document.getElementById('task').value = pressbutton;
					document.adminForm.submit();
				return false;
			}
			if (pressbutton == 'item.item_apply') {
				if (document.getElementById('jform_title').value == '') 
				{
					alert('<?php echo JText::_('COM_PAGESANDITEMS_NO_TITLE'); ?>');
					return false;
				} else {
					document.getElementById('item_apply').value = 1;
					do_item_save =true;
				}
			}
			if (pressbutton == 'item.item_checkin') {
				if (document.getElementById('jform_title').value == '') {
					alert('<?php echo JText::_('COM_PAGESANDITEMS_NO_TITLE'); ?>');
					return false;
				} else {
					//document.getElementById('item_apply').value = 1;
					do_item_save =true;
				}
			}
			if (pressbutton == 'item.item_save' || pressbutton == 'item.save2new' || pressbutton == 'item.save2copy') {
				if (document.getElementById('jform_title').value == '') {
					alert('<?php echo JText::_('COM_PAGESANDITEMS_NO_TITLE'); ?>');
					return;
				<?php
				if($this->frontend)
				{
				/*
				with the new pagetypes we can have articles without section / categorie
				so we can remove this
				*/
				?>
					}else if(document.adminForm.cat_id.value == '' || document.adminForm.cat_id.value=='0'){
						alert('<?php echo JText::_('COM_PAGESANDITEMS_NO_PAGE_SELECTED'); ?>');
						return;
				<?php
				}
				?>
				} else {
					do_item_save =true;
				}
			}
			if(do_item_save){
				<?php
					//if custom itemtype
					if(strpos($item_type, 'ustom_')){
				?>
				if(validate_custom_itemtype_fields())
				{
				<?php
					}elseif($item_type!='content' && $item_type!='text' && $item_type!='html' && $item_type!='other_item'){//if itemtype plugin
					?>
					if(validate_itemtype()){
					<?php
					}

					if($item_type=='other_item'){
					?>
					if(document.adminForm.other_item_id.value == '0'){
						alert('<?php echo addslashes(JText::_('COM_PAGESANDITEMS_NO_OTHERITEM_SELECTED')); ?>');
						return;
					}
					<?php
					}
					?>
					//submitform('item.item_save');
					//document.getElementById('task').value = 'item.item_save';
					document.getElementById('task').value = pressbutton;
					document.adminForm.submit();
				<?php
					//if custom itemtype or itemtype plugin
					if($item_type!='content' && $item_type!='text' && $item_type!='html' && $item_type!='other_item'){
				?>
				}
				<?php
					//if custom itemtype
					}
				?>

			}
		}
		-->
	</script>
	<?php
}//end getting data to edit item

$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..');
require_once($path.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'itemtypehelper.php');
if(strpos($item_type, 'ustom_'))
{
	//here we will load all custom_?
	$itemtype = ExtensionItemtypeHelper::importExtension(null, 'custom',true,null,true);
}
else
{
	//here we will load all the other
	//content, text, html and other_item are integrated
	$itemtype = ExtensionItemtypeHelper::importExtension(null, $item_type,true,null,true);
}

//$itemtype = ExtensionHelper::importExtension('itemtype',null, null,true,null,true);
$dispatcher =JDispatcher::getInstance();

//javascript for frontend to set section and category from select
if($this->frontend){
?>
<script language="javascript" type="text/javascript">
<!--
function set_section_and_category(section_category){
	pos = section_category.indexOf("_");
	category = section_category.substr((pos+1),section_category.length);
	alert('set cat'+category);
	document.getElementById('cat_id').value = category;
	section = section_category.substr(0,pos);
	document.getElementById('section_id').value = section;
}
function set_section(section){
	document.getElementById('section_id').value = section;
}
function set_category(section_category){
	document.getElementById('cat_id').value = category;
}
-->
</script>

<?php
}//end if frontend

//auto select category, only at backend
if($this->helper->getJoomlaVersion() < '3.0'){
	//does not work for joomla 3
	echo '<script>'."\n";
	echo 'function select_category(){'."\n";	
	echo 'document.getElementById(\'jform_catid\').value = \''.$cat_id.'\';'."\n";	
	echo '}'."\n";

	echo 'if(window.addEventListener)window.addEventListener("load",select_category,false);'."\n";
	echo 'else if(window.attachEvent)window.attachEvent("onload",select_category);'."\n";

	echo '</script>'."\n";
}

?>

<?php
//set empty item_type to text/content
if($item_type=='')
{
	$item_type = 'text';
}

//if itemtype is not installed
if(!PagesAndItemsHelper::checkItemTypeInstall($item_type))
{
	echo '<script> alert(\''.addslashes(JText::_('COM_PAGESANDITEMS_ITEMTYPENOTINSTALLED')).$item_type.'\'); window.history.go(-1); </script>';
	exit();
}

//if itemtype is not published, throw error
if (!in_array($item_type, PagesAndItemsHelper::getItemtypes()))
{
	echo '<script> alert(\''.addslashes(JText::_('COM_PAGESANDITEMS_ITEMTYPENOTPUBLISHED')).$item_type.'\'); window.history.go(-1); </script>';
	exit();
}

//breadcrumbs only at backend
if(!$this->frontend && $show_tree && $app->getUserState( "com_pagesanditems.articles_modus", '')=='')
{
	//only if useCheckedOut
	//if(JFactory::getApplication()->input->get('hidemainmenu',false); 
	//$useCheckedOut = PagesAndItemsHelper::getUseCheckedOut();
	//$sub_task = JFactory::getApplication()->input->get('sub_task', ''); 
	if($this->useCheckedOut && $sub_task == 'edit')
	{
		$link = '';
	}
	else
	{
		$link = PagesanditemsHelper::toogleViewPageCategories('index.php?option=com_pagesanditems&amp;view=page'.($sub_task != 'new' ? '&amp;sub_task='.$sub_task : '').($pageId ? '&amp;pageId='.$pageId : '').($categoryId ? '&amp;categoryId='.$categoryId : ''));
	}
	$url = '<a '.($link ? 'href="'.$link.'"' : 'class="no_underline"' ).'>';
	
	$url .= PagesanditemsHelper::toogleTextPageCategories('COM_PAGESANDITEMS_PAGE');
	
	$url .= '</a>';
	$url .= ' &gt; '.JText::_('COM_PAGESANDITEMS_ITEM'); 
	$url .= ' ['.PagesAndItemsHelper::translate_item_type($item_type).']';
	if($sub_task=='new')
	{
		$url .= '&nbsp; '.JText::_('COM_PAGESANDITEMS_NEW');
	}
	
	echo PagesAndItemsHelper::breadcrumb($url);
	
	echo '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
	echo '<tr>';
		
		if($app->getUserState( "com_pagesanditems.articles_modus", '')==''){
			echo '<td valign="top"  class="treeList">'; //<td  valign="top" width="20%">';
				echo $this->pageTree;
			echo '</td>';
		}
		echo '<td valign="top">';
}

if($this->frontend)
{
//	echo '<form name="adminForm" method="post" action="index.php?option=com_pagesanditems&view=item_save" enctype="multipart/form-data">';
	echo '<form class="form-validate form-horizontal" id="adminForm" name="adminForm" method="post" action="index.php?option=com_pagesanditems&view=item&task=item.item_save" enctype="multipart/form-data">';
}else{
	echo '<form class="form-validate form-horizontal" id="adminForm" name="adminForm" method="post" action="" enctype="multipart/form-data">';
}

?>

		<input type="hidden" id="option" name="option" value="com_pagesanditems" />
		<input type="hidden" name="view" value="item" />
		<input type="hidden" id="item_apply" name="item_apply" value="" />
		<input type="hidden" name="id" value="<?php echo $this->item_id; ?>" />
		<input type="hidden" name="item_id" value="<?php echo $this->item_id; ?>" />
		<input type="hidden" name="itemId" value="<?php echo $this->item_id; ?>" />
		<input type="hidden"  id="cat_id" name="cat_id" value="<?php echo $cat_id; ?>" />
		<input type="hidden" id="section_id" name="section_id" value="<?php echo $section_id; ?>" />
		<input type="hidden" id="page_id" name="page_id" value="<?php echo $pageId; ?>" />
		<input type="hidden" id="pageId" name="pageId" value="<?php echo $pageId; ?>" />
		<input type="hidden" name="modified_by" value="<?php echo $user_id; ?>" />
		<input type="hidden" name="created_by" value="<?php echo $created_by; ?>" />
		<input type="hidden" name="version" value="<?php echo ($version+1); ?>" />
		<input type="hidden" name="item_type" value="<?php echo $item_type; ?>" />
		<input type="hidden" name="edit_from_frontend" value="<?php if($this->frontend){echo '1';} ?>" />

		<input type="hidden" id="sub_task" name="sub_task" value="<?php echo $sub_task; ?>" />
		<input type="hidden" id="task" name="task" value="item.item_save" />
		<input type="hidden" id="subsub_task" name="subsub_task" value="" />
		<input type="hidden" id="menutype" name="menutype" value="<?php echo $menutype; ?>">

		<input type="hidden" id="extensionName" name="extensionName" value="<?php echo $item_type; ?>">
		<input type="hidden" id="extensionType" name="extensionType" value="itemtype">

		<input type="hidden" id="pageType" name="pageType" value="<?php echo JFactory::getApplication()->input->get('pageType',''); ?>" /> 
		<input type="hidden" id="manager" name="manager" value="<?php echo JFactory::getApplication()->input->get('manager',0);  ?>" />
		<input type="hidden" id="categoryId" name="categoryId" value="<?php echo $categoryId; ?>" />
		<input type="hidden" id="type" name="type" value="" />
		<?php echo JHtml::_('form.token'); ?>
		<input type="hidden" name="return" value="<?php echo JFactory::getApplication()->input->get('return');?>" />

		<?php
			$global_hide_show = array(array('',JText::_('COM_PAGESANDITEMS_GLOBAL')),array('0',JText::_('COM_PAGESANDITEMS_HIDE')),array('1',JText::_('COM_PAGESANDITEMS_SHOW')));
			$global_no_yes = array(array('',JText::_('COM_PAGESANDITEMS_GLOBAL')),array('0',JText::_('COM_PAGESANDITEMS_NO')),array('1',JText::_('COM_PAGESANDITEMS_YES')));


			if($this->frontend){
				$path_to_root = '';
			}else{
				$path_to_root = '../';
			}



			//layout and script for frontend
			if($this->frontend)
			{
				echo '<link type="text/css" rel="stylesheet" href="administrator/components/com_pagesanditems/css/pagesanditems3.css" />';
				if(version_compare(JVERSION, '3', '>=')) {JHTML::stylesheet('administrator/components/com_pagesanditems/css/pagesanditemsj3.css');}
			}
		?>

	<style type="text/css">

	.text_area{
		width: 100px;
	}

	<?php if(!$this->frontend){ ?>
	.calendar{
		cursor: pointer;
		float: left;
	}
	<?php } ?>
	</style>

	<?php

	//fix left-side of tabs issue
	//TODO move to css?
		echo '
		<style type="text/css">

			div#tabs_item div.tab-row h2.tab a{
				background: url('.$path_to_root.'administrator/components/com_pagesanditems/images/tab_off.png) no-repeat left top;
			}

			div#tabs_item div.tab-row h2.selected a{
				background: url('.$path_to_root.'administrator/components/com_pagesanditems/images/tab_on.gif) no-repeat left top;
				padding: 2px 10px 3px 10px;
			}
		</style>';

	//submit buttons only at frontend
	if($this->frontend)
	{
		echo '<div class="paddingList" style="margin-top: 40px;">';
			echo '<div>';
				echo '<div class="right_align">';
					$image= PagesAndItemsHelper::getDirIcons().'icon-32-pi.png';
					echo '<img src="'.$image.'" alt="" style="float:left;" />&nbsp;';

					$button = PagesAndItemsHelper::getButtonMaker();
					$button->imagePath = PagesAndItemsHelper::getDirIcons();
					$button->buttonType = 'input';
					$button->text = JText::_('COM_PAGESANDITEMS_SAVE');					
					$button->onclick = 'submitbutton2(\'item.item_save\')';
					$button->imageName = 'base/icon-16-disk.png';
					echo $button->makeButton();

					$button = PagesAndItemsHelper::getButtonMaker();
					$button->imagePath = PagesAndItemsHelper::getDirIcons();
					$button->buttonType = 'input';
					$button->text = JText::_('COM_PAGESANDITEMS_CANCEL');
					$button->onclick = 'history.back();';
					$button->imageName = 'base/icon-16-cancel.png';
					echo $button->makeButton();
					
				echo '</div>';
			echo '</div>';
		echo '</div>';
	}


	$has_content = '';	
	$itemtypeHtmlContent = new JObject();
	$itemtypeHtmlContent->text = '';
	$results = $dispatcher->trigger('onItemtypeDisplay_item_content', array(&$itemtypeHtmlContent,$item_type)); 
	if($itemtypeHtmlContent->text != '')
	{
		$has_content = 'properties';
	}
	
	
	
		$checkedOutText = '';
		$disableItem = false;
		if($this->useCheckedOut && ( !$this->canCheckin || !$this->canEdit || $sub_task =='') && !$this->frontend)// ($sub_task !='new')))// && $sub_task !=='edit')))
		{
			//$user		= JFactory::getUser();

			
			// Join over the users.
			$db		= JFactory::getDBO();
			$query	= $db->getQuery(true);
			$query->select('a.*');
			$query->from('`#__content` AS a');
			$query->select('u.name AS editor');
			$query->join('LEFT', '`#__users` AS u ON u.id = a.checked_out'); //'.$userId); //a.checked_out');
			$query->where("a.id = '".$this->item->id."'");
			$db->setQuery($query);
			$result = $db->loadObject();
			if ($this->item->checked_out)
			{
				/*
				$checkedOutText .= '<input type="hidden" value="0" id="boxchecked" >';
				$checkedOutText .= '<input type="hidden" title="" onclick="isChecked(this.checked);" value="'.$this->menuItem->id.'" name="cid[]" id="cb0" >';
				$checkedOutText .= JHtml::_('jgrid.checkedout', 0, $result->editor, $this->menuItem->checked_out_time, 'page.', $this->canCheckin);
				
				*/
				$checkedOutText .= '<input type="hidden" value="0" id="boxchecked" >';
				$checkedOutText .= '<input type="hidden" title="" onclick="isChecked(this.checked);" value="'.$this->item->id.'" name="cid[]" id="cb0" >';
				$checkedOutText .= JHtml::_('jgrid.checkedout', 0, $result->editor, $this->item->checked_out_time, 'item.', $this->canCheckin);
			//$checkedOutText
			};
			echo '<script>'."\n";
			echo 'window.addEvent(\'domready\', function() {'."\n";
					echo 'document.id(\'item_options\').addClass(\'display_none\');'."\n";
					echo 'document.id(\'item_permissions\').addClass(\'display_none\');'."\n";
			echo '});'."\n";
			echo '</script>'."\n";
			$disableItem = true;
		}

		if($disableItem)
		{

			//$this->form->setFieldAttribute('type','type','text');
			//$this->form->setFieldAttribute('type','class','readonly');
			//$this->form->setFieldAttribute('type','readonly','true');

			$this->form->setFieldAttribute('title','class','readonly');
			$this->form->setFieldAttribute('title','readonly','true');

			$this->form->setFieldAttribute('alias','class','readonly');
			$this->form->setFieldAttribute('alias','readonly','true');

			$this->form->setFieldAttribute('catid','disabled','true');

			$this->form->setFieldAttribute('state','disabled','true');
			
			$this->form->setFieldAttribute('access','disabled','true');

			$this->form->setFieldAttribute('featured','disabled','true');
		
			$this->form->setFieldAttribute('language','readonly','true');
			
			//$this->form->setFieldAttribute('description','type','textarea');
			//$this->form->setFieldAttribute('description','disabled','true');
			//$this->form->setFieldAttribute('description','readonly','true');
		}	
	?>
	<table class="piadminform xadminform" >
	<thead class="piheader">
	<tr>
		<th> <!-- class="piheader">-->
			<?php
				$menuItemsType = $this->menuItemsTypes['content_article'];
				$image = null;
				if($sub_task == 'new')
				{
					if(isset($menuItemsType->icons->item_new->imageUrl))
					{
						$image = $menuItemsType->icons->item_new->imageUrl;
					}
				}
				elseif($sub_task == 'edit')
				{
					if(isset($menuItemsType->icons->item_edit->imageUrl))
					{
						$image = $menuItemsType->icons->item_edit->imageUrl;
					}
				}
				else
				{
					if(isset($menuItemsType->icons->default->imageUrl))
					{
						$image = $menuItemsType->icons->default->imageUrl;
					}
				}
				if(!$image)
				{
					if($sub_task == 'new')
					{
						$image = PagesAndItemsHelper::getDirIcons().'article/icon-16-article_new.png';
					}
					elseif($sub_task == 'edit')
					{
						$image = PagesAndItemsHelper::getDirIcons().'article/icon-16-article_edit.png';
					}
					else
					{
						$image = PagesAndItemsHelper::getDirIcons().'article/icon-16-article.png';
					}
				}
				$titleTh =  empty($this->item->id) ? JText::_('COM_CONTENT_NEW_ARTICLE') : $sub_task == 'edit' ? JText::sprintf('COM_CONTENT_EDIT_ARTICLE', $this->item->id) : JText::_('COM_CONTENT_PAGE_VIEW_ARTICLE');
				echo PagesAndItemsHelper::getThImageTitle($image,$checkedOutText.$titleTh,null,'thIcon16','thText');
			?>
		</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				  <td valign="top">
					<?php

						$itemtypeHtml = new JObject();
						$itemtypeHtml->text = '';

						//ADD ms: 02.05.2011
						//$managerItemtypeItemEdit = new JObject();
						$managerItemtypeItemEdit = new JObject();
						$managerItemtypeItemEdit->text = '';

						//ADD END ms: 02.05.2011
						$paramsSliders = null;

						$dispatcher->trigger('onGetParams',array(&$paramsSliders, $item_type));
						

						//if($this->item_id)
						//{
						$new_or_edit = ($sub_task == 'new') ? 0 : 1;
							$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..');
							require_once($path.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'managerhelper.php');
							$extensions = ExtensionManagerHelper::importExtension(null,null, true,null,true);
							$dispatcher->trigger('onManagerItemtypeItemEdit', array (&$managerItemtypeItemEdit,$item_type,$this->item_id,$paramsSliders,$new_or_edit ));

						//}


						echo "<script language=\"javascript\"  type=\"text/javascript\">\n";
						echo "<!--\n";
						echo "function validate_itemtype(){\n\n";
						echo "is_valid = true;\n";
						echo "alert_message = \"";

						$translated = JText::_('PI_EXTENSION_ITEMTYPE_'.strtoupper($item_type).'_ALERT_MESSAGE');
						if($translated <> 'PI_EXTENSION_ITEMTYPE_'.strtoupper($item_type).'_ALERT_MESSAGE')
						{
							//we have an string
							echo $translated;
						}
						echo "\";\n";
						if(file_exists(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'itemtypes'.DIRECTORY_SEPARATOR.$item_type.DIRECTORY_SEPARATOR.'item_validation.js'))
						{
							echo JFile::read(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'itemtypes'.DIRECTORY_SEPARATOR.$item_type.DIRECTORY_SEPARATOR.'item_validation.js');
						}
						echo "return is_valid;\n";
						echo "};\n";
						//close javascript
						echo "//-->\n";
						echo "</script>\n";
						?>
						
						<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<div class="row-fluid">


	<!-- Begin Content -->
		<div class="span10 form-horizontal">
			<br />			
			<div class="row-fluid">			
				<div class="span8">
					<div class="control-group">
						<div class="control-label">
							<?php echo $this->form->getLabel('title'); ?> 
						</div>
						<div class="control">
							<?php
							if($this->frontend){
								echo '<style>.input-xxlarge, #jform_tags_chzn {width: 300px;}</style>';								
							}
							echo $this->form->getInput('title');
							?> 
						</div>
					</div>
				</div>
				<?php 
				if($this->frontend){
				?>
					<div class="clr"></div>					
				<?php 
				}
				
				if($this->helper->getJoomlaVersion() >= '3.0'){
				?>
					<div class="span4">
						<div class="control-group">
							<div class="control-label">
								<?php echo $this->form->getLabel('alias'); ?> 
							</div>
							<div class="control">
								<?php echo $this->form->getInput('alias'); ?> 
							</div>
						</div>
					</div>
				<?php
				}
				?>			
			</div>			
			<div class="clr"></div>			
			<ul class="nav nav-tabs">
				<li class="active"><a href="#general" data-toggle="tab"><?php echo JText::_('COM_CONTENT_ARTICLE_CONTENT');?></a></li>
				<?php				
				if($this->show_publishing_options){
				?>
					<li><a href="#publishing" data-toggle="tab"><?php echo JText::_('COM_CONTENT_FIELDSET_PUBLISHING');?></a></li>
				<?php
				}				
				?>
				<?php				
				if($this->show_urls_images_backend){
				?>
					<li><a href="#urlimages" data-toggle="tab"><?php echo JText::_('COM_CONTENT_FIELDSET_URLS_AND_IMAGES');?></a></li>
				<?php
				}	
				if(JLanguageAssociations::isEnabled()){			
				?>
				<li><a href="#associations" data-toggle="tab"><?php echo JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS');?></a></li>
				<?php 
				}
				/*
				not sure why this is looped here.
				$fieldSets = $this->form->getFieldsets('attribs'); 
				foreach ($fieldSets as $name => $fieldSet){
					if ($this->params['show_article_options'] || (( $this->params['show_article_options'] == '' && !empty($this->editoroptions) ))){ 
				 		if ($name != 'editorConfig' && $name != 'basic-limited'){
							<li><a href="#attrib-<?php echo $name;?>" data-toggle="tab"><?php echo JText::_($fieldSet->label);?></a></li>							
						} 
					}
				} 
				*/
				?>
				<?php				
				if($this->show_article_options){
				?>
					<li><a href="#attrib-basic" data-toggle="tab"><?php echo JText::_('COM_CONTENT_ATTRIBS_FIELDSET_LABEL');?></a></li>
				<?php
				}
				//if ($this->canDo->get('core.admin')): //can't access this as superadmin, so that not working either way 
				//so falling back to show this only for superadmins
				if($this->helper->getIsSuperAdmin()){
				?>
					<li><a href="#editor" data-toggle="tab"><?php echo JText::_('COM_CONTENT_SLIDER_EDITOR_CONFIG');?></a></li>
				<?php
				}				
				//if ($this->canDo->get('core.admin')): //can't access this as superadmin, so that not working either way 
				//so falling back to show this only for superadmins
				if($this->helper->getIsSuperAdmin()){
				?>
					<li><a href="#permissions" data-toggle="tab"><?php echo JText::_('COM_CONTENT_FIELDSET_RULES');?></a></li>
				<?php
				}
				if(PagesAndItemsHelper::check_display('item_props_pioptions')){
				?>			
					<li><a href="#pioptions" data-toggle="tab"><?php echo JText::_('COM_PAGESANDITEMS_ITEM_OPTIONS');?></a></li>
				<?php
				}
				?>
				
			</ul>

			<div class="tab-content">
				<!-- Begin Tabs -->
				<div class="tab-pane active" id="general">
					
				



						<?php else : ?>
						
						<?php //2.5 ?>
						
						<div class="clr"></div>
						
						
						<!-- details section begin -->
						<div class="width-70 fltlft" >
							<fieldset class="adminform" <?php echo !PagesAndItemsHelper::check_display('item_props_details') ? $this->display_none : ''; ?>>
								<legend id="content_details"><?php echo JText::_('JDETAILS'); ?></legend>
								<ul class="adminformlist">
									<li <?php echo !PagesAndItemsHelper::check_display('item_props_title') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('title'); ?>
									<?php echo $this->form->getInput('title'); ?></li>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_alias') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('alias'); ?>
									<?php echo $this->form->getInput('alias'); ?></li>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_category') ? $this->display_none : ''; ?>>
									
									<?php //need for J2.5 ?>
									<?php $this->form->setFieldAttribute('catid','extension','com_content'); ?>
									<?php echo $this->form->getLabel('catid'); ?>
									<?php echo $this->form->getInput('catid'); ?></li>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_status') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('state'); ?>

									<?php
									$class = '';
									$state_label = '';
									if (!$this->canDo->get('core.edit.state')){
										$class = 'class="display_none"';
										$state = $this->item->state;
										//switch ($state_label) {
										switch ($state) {
										case -2:
											$state_label = JText::_('COM_PAGESANDITEMS_TRASHED');
											break;
										case 0:
											$state_label = JText::_('COM_PAGESANDITEMS_UNPUBLISHED');
											break;
										case 1:
											$state_label = JText::_('COM_PAGESANDITEMS_PUBLISHED');
											break;
										case 2:
											$state_label = JText::_('COM_PAGESANDITEMS_ARCHIVED');
											break;
										}
									}
									?>
									<span <?php echo $class; ?>>
										<?php
										echo $this->form->getInput('state');
										?>
									</span>
									<span>
										<?php echo $state_label; ?>
									</span>
									</li>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_access') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('access'); ?>
									<?php echo $this->form->getInput('access'); ?></li>

									<?php
									if(!$this->frontend){
										if ($this->canDo->get('core.admin')): ?>
											<li <?php echo !PagesAndItemsHelper::check_display('item_props_permissions') ? $this->display_none : ''; ?>>
											<span class="faux-label"><?php echo JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL'); ?></span>
												<div class="button2-left"><div class="blank">
													<button type="button" onclick="document.location.href='#access-rules';">
														<?php echo JText::_('JGLOBAL_PERMISSIONS_ANCHOR'); ?>
													</button>
												</div></div>
											</li>
										<?php
										endif;
									}
									 ?>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_featured') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('featured'); ?>
									<?php echo $this->form->getInput('featured'); ?></li>

									<li <?php echo !PagesAndItemsHelper::check_display('item_props_language') ? $this->display_none : ''; ?>>
									<?php echo $this->form->getLabel('language'); ?>
									<?php echo $this->form->getInput('language'); ?></li>


									<li <?php if(!$this->item_id || !PagesAndItemsHelper::check_display('item_props_id')  || $this->frontend){echo $this->display_none;} ?>><?php echo $this->form->getLabel('id'); ?>
									<?php echo $this->form->getInput('id'); ?></li>

									<?php



									?>
								</ul>

							<?php endif; ?>

							<?php

							
							echo '</fieldset>';
								
								if ($managerItemtypeItemEdit->text != '')
								{									
									echo $managerItemtypeItemEdit->text;
								}
							
							if(($this->useCheckedOut && ($this->canEdit && ($sub_task =='edit' || $sub_task =='new'))) || (!$this->useCheckedOut && $this->canEdit))
							{
								//echo '<div'.$hidden_articletext.'>';
								echo $config['plugin_syntax_cheatcheat'];	
								
								//edit form so we display all
								echo $itemtypeHtmlContent->text;

								if(strpos($item_type, 'ustom_') === false  && $this->helper->getJoomlaVersion() < '3.0')
								{
										echo '<fieldset class="adminform" id="pi_content_pane">';
										echo '<legend id="toggle_content" class="hasTip" title="'.JText::_($this->form->getFieldAttribute('articletext','label')).'::'.JText::_($this->form->getFieldAttribute('articletext','description')).'">'.JText::_($this->form->getFieldAttribute('articletext','label'));
										echo '</legend>';
								}								
								$results = $dispatcher->trigger('onItemtypeDisplay_item_edit', array(&$itemtypeHtml,$item_type,$this->item_id,$text,$itemIntroText,$itemFullText,$this->form));
								
								
								

								if($item_type != 'text'){
									//any other itemtype
									
									echo '<div id="target_content">';
									echo $itemtypeHtml->text;
									echo '</div>';
								}
								
								$hidden_articletext = '';
								$articletextChanged = null;
								//here we trigger so the item_type can change the articletext over $this->form
								//at this moment only used for item_type = html
								$dispatcher->trigger('onItemtypeDisplay_item_edit_articletext', array(&$articletextChanged,$this->form,$item_type));
								if($articletextChanged && PagesAndItemsHelper::check_display('item_props_articletext'))
								{
									//$doc
								}
								else if($item_type !='text' || !PagesAndItemsHelper::check_display('item_props_articletext')){
									//if itemtype is not 'text',, then still put the normal fields on the page
									//but hidden, so the value gets parsed to the com_content save function
									$hidden_articletext = ' style="display: none;"';
									//ms: only the textarea will output
									$this->form->setFieldAttribute('articletext','type','textarea');
									$this->form->setFieldAttribute('articletext','class','hide');
								}
								
								//show editor														
								echo $this->form->getInput('articletext');								
							
								//echo '</div>';

								echo '<div class="clr"></div>';

								if(strpos($item_type, 'ustom_') === false && $this->helper->getJoomlaVersion() < '3.0')
								{
									echo '</fieldset>';
								}
								echo '<br />';
							
								if($this->helper->getJoomlaVersion() >= '3.0'){
									echo '<fieldset>';									
										 echo JLayoutHelper::render('joomla.edit.global', $this); 
									echo '</fieldset>';
								}
								
								
							}
							else
							{
								//no edit form so we display pure 
								echo '<fieldset class="adminform" id="pi_content_pane">';
									echo '<legend id="toggle_content" class="hasTip" title="'.JText::_($this->form->getFieldAttribute('articletext','label')).'::'.JText::_($this->form->getFieldAttribute('articletext','description')).'">';
										echo JText::_($this->form->getFieldAttribute('articletext','label'));
									echo '</legend>';
									$this->form->setFieldAttribute('articletext','type','textarea');
									$this->form->setFieldAttribute('articletext','disabled','true');
									$this->form->setFieldAttribute('articletext','class','articletext_disabled');
									$this->form->setFieldAttribute('articletext','rows','20');
									echo $this->form->getInput('articletext');
								echo '</fieldset>';
							}
							?>
						<?php if(version_compare(JVERSION, '3', '>=')) : ?>
						
				
					</div>
				<!-- End tab general -->
					
					<?php //endif; ?>
						
						<?php else: ?>
						
						</div>
						<?php endif; ?>
						<!-- details section end -->

<?php
	//mootools script to hide sliders as set in PI config
	$panels_to_hide = array();
	if(!PagesAndItemsHelper::check_display('item_props_pioptions')){
		$panels_to_hide[] = 'pi-item-options';
	}
	if(!PagesAndItemsHelper::check_display('item_props_metadataoptions')){
		$panels_to_hide[] = 'meta-options';
	}
	if(!PagesAndItemsHelper::check_display('item_props_articleoptions')){
		$panels_to_hide[] = 'basic-options';
	}
	if(!PagesAndItemsHelper::check_display('item_props_publishingoptions')){
		$panels_to_hide[] = 'publishing-details';
	}	
		
	if(count($panels_to_hide)){
		echo '<script>'."\n";
		echo 'var panels_array = new Array(';
		$first = 1;
		foreach($panels_to_hide as $panel_to_hide){
			if(!$first){
				echo ',';
			}else{
				$first = 0;
			}
			echo '"';
			echo $panel_to_hide;
			echo '"';
		}
		echo ');'."\n";
		echo 'window.addEvent(\'domready\', function() {'."\n";
		echo 'for (i = 0; i < panels_array.length; i++){'."\n";
		echo 'var myElement = document.id(panels_array[i]);'."\n";
		echo 'var parent = myElement.getParent();'."\n";
		echo 'parent.style.display = \'none\';'."\n";
		echo '}'."\n";
		echo '});'."\n";
	echo '</script>'."\n";
}
							

?>
						<?php if(version_compare(JVERSION, '3', '<')) : ?>
						<div id="item_options" class="width-30 fltrt">
						<?php echo JHtml::_('sliders.start','content-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
						
						<?php else : ?>
						
						<?php endif; ?>
						
							<?php echo $this->loadTemplate('options'); ?>
							<?php echo $this->loadTemplate('urlimages'); ?>
							<?php echo $this->loadTemplate('associations'); ?>
							<?php echo $this->loadTemplate('metadata'); ?>							
							<?php 
							if(version_compare(JVERSION, '3', '>')){
								echo $this->loadTemplate('permissions'); 
							}
							?>							
							<?php echo $this->loadTemplate('pioptions'); ?>
						<?php if(version_compare(JVERSION, '3', '<')) : ?>
						<?php echo JHtml::_('sliders.end'); ?>
						</div>

						<div class="clr"></div>
						<?php endif; ?>
						
						<?php if(!$this->frontend){
						?>
						
						<?php if ($this->canDo->get('core.admin')): ?>
							<?php if(version_compare(JVERSION, '3', '>=')) : ?>
							<div class="tab-pane" id="permissions">
								<fieldset>
									<?php echo $this->form->getInput('rules'); ?>
								</fieldset>
							</div>
							<?php endif; ?>


<?php if(version_compare(JVERSION, '3', '>=')){ ?>
						</div>
					</div>
<?php } ?>

<?php if($this->frontend){ ?>
<!-- Begin Sidebar -->
		<div class="span2">
			<h4><?php echo JText::_('JDETAILS');?></h4>			
			<hr />
			<fieldset class="form-vertical">
				<div class="control-group">
					<div class="controls">
						<?php echo $this->form->getValue('title'); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getLabel('state'); ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getInput('state'); ?>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getLabel('access'); ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getInput('access'); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getLabel('featured'); ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getInput('featured'); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getLabel('language'); ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getInput('language'); ?>
					</div>
				</div>
			</fieldset>
		</div>

		<!-- End Sidebar -->

<?php } ?>

					
						<?php
						//submit buttons only at frontend
						if($this->frontend)
						{
							echo '<div class="paddingList right_align">';
							$button = PagesAndItemsHelper::getButtonMaker();
							$button->imagePath = PagesAndItemsHelper::getDirIcons();
							$button->buttonType = 'input';
							$button->text = JText::_('COM_PAGESANDITEMS_SAVE');
							//$button->alt = 'alt JText::_('COM_PAGESANDITEMS_CONVERT_TO_PI_ITEM')(s)';
							$button->onclick = 'submitbutton2(\'item.item_save\')';
							$button->imageName = 'base/icon-16-disk.png';
							echo $button->makeButton();

							$button = PagesAndItemsHelper::getButtonMaker();
							$button->imagePath = PagesAndItemsHelper::getDirIcons();
							$button->buttonType = 'input';
							$button->text = JText::_('COM_PAGESANDITEMS_CANCEL');
							//$button->alt = 'alt JText::_('COM_PAGESANDITEMS_CONVERT_TO_PI_ITEM')(s)';
							$button->onclick = 'submitbutton2(\'item.cancel\')';//'history.back();';
							$button->imageName = 'base/icon-16-cancel.png';
							echo $button->makeButton();

							//echo '<input type="button" value="'.JText::_('COM_PAGESANDITEMS_SAVE').'" onclick="submitbutton2(\'item_save\')" />&nbsp;&nbsp;&nbsp;';
							//echo '<input type="button" value="'.JText::_('COM_PAGESANDITEMS_CANCEL').'" onclick="history.back();" />';
							echo '</div>';
						}
						elseif($show_tree && version_compare(JVERSION, '3', '<'))
						{
							if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
							{
							echo '<table class="toolbar"><tr>';
							echo '<td class="button" id="toolbar-save">';
							echo '<a href="#" onclick="javascript: submitbutton(\'item.item_save\')" class="toolbar">';
							echo '<span class="icon-32-save" title="'.JText::_('COM_PAGESANDITEMS_SAVE').'"></span>'.JText::_('COM_PAGESANDITEMS_SAVE').'</a>';
							echo '</td>';
							echo '<td class="button" id="toolbar-apply">';
							echo '<a href="#" onclick="javascript: submitbutton(\'item.item_apply\')" class="toolbar">';
							echo '<span class="icon-32-apply" title="'.JText::_('COM_PAGESANDITEMS_APPLY').'"></span>'.JText::_('COM_PAGESANDITEMS_APPLY').'</a>';
							echo '</td>';
							echo '<td class="button" id="toolbar-cancel">';
							echo '<a href="#" onclick="javascript: submitbutton(\'item.cancel\')" class="toolbar">';
							echo '<span class="icon-32-cancel" title="'.JText::_('COM_PAGESANDITEMS_CANCEL').'"></span>'.JText::_('COM_PAGESANDITEMS_CANCEL').'</a></td></tr></table>';
							echo '</td></tr></table>';
							}
							else
							{
							if( ($this->useCheckedOut && $sub_task =='edit') || !$this->useCheckedOut)// && $sub_task !=='edit')))
							{
							//in J3 other way
							echo '<div class="toolbar-list" style="float:left;">';								
								echo '<ul>';
									echo '<li class="button" id="toolbar-apply">';
										echo '<a href="#" onclick="javascript: Joomla.submitbutton(\'item.item_apply\')" class="toolbar">';
											echo '<span class="icon-32-apply" title="'.JText::_('COM_PAGESANDITEMS_APPLY').'">';
											echo '</span>';
											echo JText::_('JTOOLBAR_APPLY');//COM_PAGESANDITEMS_APPLY');
										echo '</a>';
									echo '</li>';
									echo '<li class="button" id="toolbar-save">';
										echo '<a href="#" onclick="javascript: Joomla.submitbutton(\'item.item_save\')" class="toolbar">';
											echo '<span class="icon-32-save" title="'.JText::_('COM_PAGESANDITEMS_SAVE').'">';
											echo '</span>';
											echo JText::_('JTOOLBAR_SAVE');//COM_PAGESANDITEMS_SAVE');
										echo '</a>';
									echo '</li>';
									echo '<li class="button" id="toolbar-cancel">';
										echo '<a href="#" onclick="javascript: Joomla.submitbutton(\'item.cancel\')" class="toolbar">';
											echo '<span class="icon-32-cancel" title="'.JText::_('COM_PAGESANDITEMS_CANCEL').'">';
											echo '</span>';
											echo JText::_('JTOOLBAR_CANCEL'); //COM_PAGESANDITEMS_CANCEL');
										echo '</a>';
									echo '</li>';
								echo '</ul>';
							echo '</div>';
							}
							}
						}
						?>
						<?php if(version_compare(JVERSION, '3', '<')) : ?>
						<?php //else : ?>
						
						<?php //endif; ?>
						<div class="clr"></div>
							<fieldset class="permission adminform">
							<div id="item_permissions" class="width-100 fltlft" <?php echo !PagesAndItemsHelper::check_display('item_props_permissions') ? $this->display_none : ''; //if(!$this->helper->check_display('item_props_permissions'))){echo $this->display_none;} ?>>
								<?php echo JHtml::_('sliders.start','permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>

									<?php echo JHtml::_('sliders.panel',JText::_('COM_CONTENT_FIELDSET_RULES'), 'access-rules'); ?>

									<fieldset class="panelform">
										<?php echo $this->form->getLabel('rules'); //only in J2.5 ?>
										<?php echo $this->form->getInput('rules'); ?>
									</fieldset>

								<?php echo JHtml::_('sliders.end'); ?>
							</div>
							</fieldset>
							<?php endif; ?>
						<?php endif; ?>
						<?php //endif; ?>
						<?php if(version_compare(JVERSION, '3', '>=')) : ?>
					<?php endif; ?>	
						<?php }//end backend
						?>

			</td>
			</tr>
		</table> 
		</td>
	</tr>
	</tbody>
	</table>	
	</form>

<?php

if(!$this->frontend)
{
	echo '</td></tr></table>';
}

require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'default'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.'default_footer.php');

?>
</div>