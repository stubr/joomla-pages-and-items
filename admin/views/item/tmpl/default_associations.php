<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_categories
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if(version_compare(JVERSION, '3', '>=')) :
$fields = $this->form->getFieldset('item_associations');
?>
<div class="tab-pane" id="associations">
	<fieldset>
<?php foreach ($fields as $field) : ?>

		<div class="control-group">
			<div class="control-label">
				<?php echo $field->label ?>
			</div>
			<div class="controls">
			<?php
//		$this->form->getFieldAttribute('type','readonly','true')
		?>
				<?php echo $field->input; ?>
			</div>
		</div>
<?php endforeach; ?>
	</fieldset>
</div>
<?php else : ?>

								<?php echo JHtml::_('sliders.panel',JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS'), 'associations'); ?>
								<?php $fields = $this->form->getFieldset('item_associations');

								foreach ($fields as $field) : ?>
									<div class="associations" >
									<fieldset class="panelform">
										<div class="control-group">
											<div class="control-label">
												<?php echo $field->label ?>
											</div>
											<div class="controls">
											<?php
//		$this->form->getFieldAttribute('type','readonly','true')
		
		?>
												<?php echo $field->input; ?>
											</div>
										</div>
									</fieldset>
									</div>
								<?php endforeach; ?>
<?php endif; ?>