<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<div class="tab-pane" id="metadata">
	<fieldset>
		
	</fieldset>
</div>
<?php else : ?>

	<?php echo JHtml::_('sliders.panel',JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS'), 'meta-options'); ?>
		<fieldset class="panelform" <?php echo !PagesAndItemsHelper::check_display('item_props_metadataoptions') ? $this->display_none : ''; ?>>
			<div <?php echo !PagesAndItemsHelper::check_display('item_props_desc') ? $this->display_none : ''; ?>>
				<?php echo $this->form->getLabel('metadesc'); ?>
				<?php echo $this->form->getInput('metadesc'); ?>
			</div>

			<div <?php echo !PagesAndItemsHelper::check_display('item_props_keywords') ? $this->display_none : '';  ?>>
				<?php echo $this->form->getLabel('metakey'); ?>
				<?php echo $this->form->getInput('metakey'); ?>
			</div>

			<?php foreach($this->form->getGroup('metadata') as $field): ?>
				<?php if ($field->hidden): ?>
					<?php echo $field->input; ?>
				<?php else: ?>
					<?php $field_name = 'item_props_'.$field->__get('fieldname'); ?>
					<div <?php if(!PagesAndItemsHelper::check_display($field_name)){echo $this->display_none;} ?>>
						<?php echo $field->label; ?>
						<?php echo $field->input; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</fieldset>
<?php endif; ?>