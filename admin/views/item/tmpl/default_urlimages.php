<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<div class="tab-pane" id="urlimages">
	<fieldset>
	<?php
			// The url and images fields only show if the configuration is set to allow them. This is for legacy reasons.
		?>
		<?php if ($this->params['show_urls_images_backend']): ?>
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<div class="control-label">
							<?php echo $this->form->getLabel('images'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('images'); ?>
						</div>
					</div>
					<?php foreach($this->form->getGroup('images') as $field): ?>
						<div class="control-group">
							<?php if (!$field->hidden): ?>
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
							<?php endif; ?>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="span6">
					<?php foreach($this->form->getGroup('urls') as $field): ?>
						<div class="control-group">
							<?php if (!$field->hidden): ?>
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
							<?php endif; ?>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</fieldset>
</div>
<?php endif; ?>