<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_categories
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if($this->item_type != 'other_item')
{
	//get all instances of this item
	$this->db->setQuery( "SELECT c.catid, o.item_id"
	. "\nFROM #__content AS c"
	. "\nLEFT JOIN #__pi_item_other_index AS o"
	. "\nON c.id=o.item_id"
	. "\nWHERE other_item_id='$this->item_id'"
	. "\nAND (c.state='0' OR c.state='1')"
	. "\nORDER BY c.ordering ASC"
	);
	$instances_of_item = $this->db->loadObjectList();

	//get categories
	$this->db->setQuery("SELECT id, title FROM #__categories");
	$all_categories_db = $this->db->loadObjectList();
}//end if not item instance
?>

<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<div class="tab-pane" id="pioptions">
	<fieldset>
		<?php // here add the options echo $this->loadTemplate('options'); ?>

			<?php if($this->item_type!='other_item')
			{
			echo '<div class="control-group';			
			echo PagesAndItemsHelper::check_display_j3('item_props_instance'); 
			echo '">';
				echo '<label class="control-label">';
					echo JText::_('COM_PAGESANDITEMS_INSTANCES_OF_THIS_ITEM'); //.':<br />';
				echo '</label>';
			
				echo '<fieldset>';
				if(count($instances_of_item)){
					echo '<ul>';
					foreach($instances_of_item as $instance_of_item)
					{
						//find the page_id
						$menuitems = PagesAndItemsHelper::getMenuitems();
						foreach($menuitems as $menu_item_page){
							$temp_cat_id = 0;
							//if category blog
							if((strstr($menu_item_page->link, 'index.php?option=com_content&view=category&layout=blog') && $menu_item_page->type!='url' && $menu_item_page->type=='component') || ($menu_item_page->type=='content_category_blog') ){
								//get the category id of each menu item
								$pos_cat_id = strpos($menu_item_page->link,'id=');
								$temp_cat_id = substr($menu_item_page->link, ($pos_cat_id+3), strlen($menu_item_page->link));
								if($instance_of_item->catid==$temp_cat_id){
									$original_page_id = $menu_item_page->id;
									break;
								}
							}
						}
						if($this->frontend){
							echo '<li><a href="index.php?option=com_pagesanditems&view=item&sub_task=edit&pageId='.$original_page_id.'&item_id='.$instance_of_item->item_id.'">';
						}else{
							echo '<li><a href="index.php?option=com_pagesanditems&view=item&sub_task=edit&pageId='.$original_page_id.'&itemId='.$instance_of_item->item_id.'">';
						}
						foreach($all_categories_db as $category_row){
							if($category_row->id==$instance_of_item->catid){
								echo $category_row->title.'';
								break;
							}
						}
						echo '</a></li>';
					}
					echo '</ul>';
				}else{
					echo JText::_('COM_PAGESANDITEMS_NO_INSTANCES');
				}
				echo '<br />';
				/*
					make as button
				*/
				$link = 'index.php?option=com_pagesanditems&view=';
				if($this->frontend){
					$link .='item&type=content_blog_category&sub_task=new&pageId=0&item_type=other_item';
				}else{
					$link .='instance_select';
				}
					$link .='&other_item_id='.$this->item_id;
					echo '<div class="button2-left">';
						echo '<div class="blank">';
							echo '<a href="'.$link.'">';
								echo JText::_('COM_PAGESANDITEMS_CREATE_INSTANCE');
							echo '</a>';
						echo '</div>';
					echo '</div>';
				echo '</fieldset>';
			echo '</div>';
			}//end if not item instance
			?>
			<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_pishowtitle'); ?>">
				<label class="control-label">
					<?php echo JText::_('COM_PAGESANDITEMS_SHOW_TITLE'); ?>
				</label>
				<div class="controls">
					<input class="inputbox" type="checkbox" name="show_title_item" value="1" <?php if($this->show_title_item){ echo "checked=\"checked\""; }?> />
				</div>
			</div>
	</fieldset>
</div>
<?php else : ?>
	<?php echo JHtml::_('sliders.panel',JText::_('COM_PAGESANDITEMS_ITEM_OPTIONS'), 'pi-item-options'); ?>
	<fieldset class="panelform">
		<ul class="adminformlist">
			<?php if($this->item_type!='other_item')
			{
			echo '<li ';
				if(!PagesAndItemsHelper::check_display('item_props_instance')){
					echo $this->display_none;
			}
			echo '>';
				echo '<label>';
					echo JText::_('COM_PAGESANDITEMS_INSTANCES_OF_THIS_ITEM'); //.':<br />';
				echo '</label>';
				echo '<fieldset>';
				if(count($instances_of_item)){
					echo '<ul>';
					foreach($instances_of_item as $instance_of_item)
					{
						//find the page_id
						$menuitems = PagesAndItemsHelper::getMenuitems();
						foreach($menuitems as $menu_item_page){
							$temp_cat_id = 0;
							//if category blog
							if((strstr($menu_item_page->link, 'index.php?option=com_content&view=category&layout=blog') && $menu_item_page->type!='url' && $menu_item_page->type=='component') || ($menu_item_page->type=='content_category_blog') ){
								//get the category id of each menu item
								$pos_cat_id = strpos($menu_item_page->link,'id=');
								$temp_cat_id = substr($menu_item_page->link, ($pos_cat_id+3), strlen($menu_item_page->link));
								if($instance_of_item->catid==$temp_cat_id){
									$original_page_id = $menu_item_page->id;
									break;
								}
							}
						}
						if($this->frontend){
							echo '<li><a href="index.php?option=com_pagesanditems&view=item&sub_task=edit&pageId='.$original_page_id.'&item_id='.$instance_of_item->item_id.'">';
						}else{
							echo '<li><a href="index.php?option=com_pagesanditems&view=item&sub_task=edit&pageId='.$original_page_id.'&itemId='.$instance_of_item->item_id.'">';
						}
						foreach($all_categories_db as $category_row){
							if($category_row->id==$instance_of_item->catid){
								echo $category_row->title.'';
								break;
							}
						}
						echo '</a></li>';
					}
					echo '</ul>';
				}else{
					echo JText::_('COM_PAGESANDITEMS_NO_INSTANCES');
				}
				echo '<br />';
				/*
					make as button
				*/
				$link = 'index.php?option=com_pagesanditems&view=';
				if($this->frontend){
					$link .='item&type=content_blog_category&sub_task=new&pageId=0&item_type=other_item';
				}else{
					$link .='instance_select';
				}
					$link .='&other_item_id='.$this->item_id;
					echo '<div class="button2-left">';
						echo '<div class="blank">';
							echo '<a href="'.$link.'">';
								echo JText::_('COM_PAGESANDITEMS_CREATE_INSTANCE');
							echo '</a>';
						echo '</div>';
					echo '</div>';
				echo '</fieldset>';
			echo '</li>';
			}//end if not item instance
			?>
			<li <?php echo !PagesAndItemsHelper::check_display('item_props_pishowtitle') ? $this->display_none : ''; ?>>
				<label>
					<?php echo JText::_('COM_PAGESANDITEMS_SHOW_TITLE'); ?>
				</label>
				<fieldset class="radio">
					<input type="checkbox" name="show_title_item" value="1" <?php if($this->show_title_item){ echo "checked=\"checked\""; }?> />
				</fieldset>
			</li>
		</ul>
	</fieldset>
<?php endif; ?>