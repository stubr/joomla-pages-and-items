<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_categories
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<?php if(version_compare(JVERSION, '3', '>=')) : ?>
<?php // Do not show the publishing options if the edit form is configured not to. ?>
	<?php  if ($this->params['show_publishing_options'] || ( $this->params['show_publishing_options'] = '' && !empty($this->editoroptions)) ): ?>
		<div class="tab-pane" id="publishing">
			<div class="row-fluid">
				<div class="span6">	
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_start'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('publish_up'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('publish_up'); ?>
						</div>
					</div>
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_finish'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('publish_down'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('publish_down'); ?>
						</div>
					</div>	
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_createddate'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('created'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('created'); ?>
						</div>
					</div>	
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_createdby'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('created_by'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('created_by'); ?>
						</div>
					</div>	
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_createdbyalias'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('created_by_alias'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('created_by_alias'); ?>
						</div>
					</div>	
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_modified'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('modified'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('modified'); ?>
						</div>
					</div>
					<?php if ($this->item->modified_by) : ?>
						<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_modified_by'); ?>">
							<div class="control-label">
								<?php echo $this->form->getLabel('modified_by'); ?>
							</div>
							<div class="controls">
								<?php echo $this->form->getInput('modified_by'); ?>
							</div>
						</div>										
					<?php endif; ?>	
					<?php if ($this->item->version) : ?>
						<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_revision'); ?>">
							<div class="control-label">
								<?php echo $this->form->getLabel('version'); ?>
							</div>
							<div class="controls">
								<?php echo $this->form->getInput('version'); ?>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($this->item->hits) : ?>
						<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_hits'); ?>">
							<div class="control-label">
								<?php echo $this->form->getLabel('hits'); ?>
							</div>
							<div class="controls">
								<?php echo $this->form->getInput('hits'); ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_id'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('id'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('id'); ?>
						</div>
					</div>
					
				</div>
				<div class="span6">
					
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_desc'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('metadesc'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('metadesc'); ?>
						</div>
					</div>
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_keywords'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('metakey'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('metakey'); ?>
						</div>
					</div>
					<div class="control-group<?php echo PagesAndItemsHelper::check_display_j3('item_props_keyref'); ?>">
						<div class="control-label">
							<?php echo $this->form->getLabel('xreference'); ?>
						</div>
						<div class="controls">
							<?php echo $this->form->getInput('xreference'); ?>
						</div>
					</div>
					<?php 					
					foreach($this->form->getGroup('metadata') as $field){
					$display_none = ''; 
					if(strpos($field->name, 'robots') && !PagesAndItemsHelper::check_display('item_props_robots') ||
					strpos($field->name, 'author') && !PagesAndItemsHelper::check_display('item_props_author') ||
					strpos($field->name, 'rights') && !PagesAndItemsHelper::check_display('item_props_rights') ||
					strpos($field->name, 'xreference') && !PagesAndItemsHelper::check_display('item_props_xreference')					
					){
						$display_none = 'display_none'; 
					}
					?>
						<div class="control-group <?php echo $display_none; ?>">
							<?php if (!$field->hidden): ?>
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
							<?php endif; ?>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php 
					}
					?>

					
				</div>
			</div>
		</div>
	<?php  endif; ?>
	<?php  $fieldSets = $this->form->getFieldsets('attribs'); ?>
		<?php foreach ($fieldSets as $name => $fieldSet) : ?>
			<div class="tab-pane" id="attrib-<?php echo $name;?>">
			<?php
				// If the parameter says to show the article options or if the parameters have never been set, we will
				// show the article options.

				if ($this->params['show_article_options'] || (( $this->params['show_article_options'] == '' && !empty($this->editoroptions) ))):
					// Go through all the fieldsets except the configuration and basic-limited, which are
					// handled separately below.

					if ($name != 'editorConfig' && $name != 'basic-limited') : ?>
						<?php if (isset($fieldSet->description) && trim($fieldSet->description)) : ?>
							<p class="tip"><?php echo $this->escape(JText::_($fieldSet->description));?></p>
						<?php endif;
						foreach ($this->form->getFieldset($name) as $field){ 
						
							$display_none = ''; 
							if(strpos($field->name, 'show_title') && !PagesAndItemsHelper::check_display('item_props_show_title') ||
							strpos($field->name, 'link_titles') && !PagesAndItemsHelper::check_display('item_props_link_titles') ||
							strpos($field->name, 'show_tags') && !PagesAndItemsHelper::check_display('item_props_show_tags') ||
							strpos($field->name, 'show_intro') && !PagesAndItemsHelper::check_display('item_props_show_intro') ||
							strpos($field->name, 'info_block_position') && !PagesAndItemsHelper::check_display('item_props_show_position') ||
							strpos($field->name, 'show_category') && !PagesAndItemsHelper::check_display('item_props_show_category') ||
							strpos($field->name, 'link_category') && !PagesAndItemsHelper::check_display('item_props_link_category') ||
							strpos($field->name, 'show_parent_category') && !PagesAndItemsHelper::check_display('item_props_show_parent_category') ||
							strpos($field->name, 'link_parent_category') && !PagesAndItemsHelper::check_display('item_props_link_parent_category') ||
							strpos($field->name, 'show_author') && !PagesAndItemsHelper::check_display('item_props_show_author') ||
							strpos($field->name, 'link_author') && !PagesAndItemsHelper::check_display('item_props_link_author') ||
							strpos($field->name, 'show_create_date') && !PagesAndItemsHelper::check_display('item_props_show_create_date') ||
							strpos($field->name, 'show_modify_date') && !PagesAndItemsHelper::check_display('item_props_show_modify_date') ||
							strpos($field->name, 'show_publish_date') && !PagesAndItemsHelper::check_display('item_props_show_publish_date') ||
							strpos($field->name, 'show_item_navigation') && !PagesAndItemsHelper::check_display('item_props_show_item_navigation') ||
							strpos($field->name, 'show_icons') && !PagesAndItemsHelper::check_display('item_props_show_icons') ||
							strpos($field->name, 'show_print_icon') && !PagesAndItemsHelper::check_display('item_props_show_print_icon') ||
							strpos($field->name, 'show_email_icon') && !PagesAndItemsHelper::check_display('item_props_show_email_icon') ||
							strpos($field->name, 'show_vote') && !PagesAndItemsHelper::check_display('item_props_show_vote') ||
							strpos($field->name, 'show_hits') && !PagesAndItemsHelper::check_display('item_props_show_hits') ||
							strpos($field->name, 'show_noauth') && !PagesAndItemsHelper::check_display('item_props_show_noauth') ||
							strpos($field->name, 'urls_position') && !PagesAndItemsHelper::check_display('item_props_show_poslinks') ||
							strpos($field->name, 'alternative_readmore') && !PagesAndItemsHelper::check_display('item_props_alternative_readmore') ||
							strpos($field->name, 'article_layout') && !PagesAndItemsHelper::check_display('item_props_article_layout')										
							){
								$display_none = 'display_none'; 
							}
						?>
							<div class="control-group <?php echo $display_none; ?>">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<?php echo $field->input; ?>
								</div>
							</div>
						<?php 
						
						}
					endif;
				// If we are not showing the options we need to use the hidden fields so the values are not lost.
				elseif ($name == 'basic-limited'):
					foreach ($this->form->getFieldset('basic-limited') as $field) :
						echo $field->input;
					endforeach;
				endif;
			?>
			</div>
		<?php endforeach;
		// We need to make a separate space for the configuration
		// so that those fields always show to those wih permissions

		//if ($this->canDo->get('core.admin')): //does currently not seem to work, superadmins can not see this j 3.2.1 ?>
		<div class="tab-pane" id="editor">
			<?php foreach ($this->form->getFieldset('editorConfig') as $field) : ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="controls">
						<?php echo $field->input; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php //endif ?>

<?php else : ?>

			<?php // Do not show the publishing options if the edit form is configured not to. ?>
			<?php if ($this->params['show_publishing_options'] || ( $this->params['show_publishing_options'] = '' && !empty($this->editoroptions)) ): ?>
				<?php echo JHtml::_('sliders.panel',JText::_('COM_CONTENT_FIELDSET_PUBLISHING'), 'publishing-details'); ?>
				<fieldset class="panelform">
					<ul class="adminformlist" <?php echo !PagesAndItemsHelper::check_display('item_props_publishingoptions') ? $this->display_none : ''; //if(!$this->helper->check_display('item_props_publishingoptions'))){echo $this->display_none;} ?>>
						<li <?php if(!PagesAndItemsHelper::check_display('item_props_createdby') || $this->frontend){echo $this->display_none;} ?>>
						<?php echo $this->form->getLabel('created_by'); ?>
						<?php echo $this->form->getInput('created_by'); ?></li>

						<li <?php if(!PagesAndItemsHelper::check_display('item_props_createdbyalias') || $this->frontend){echo $this->display_none;} ?>>
						<?php echo $this->form->getLabel('created_by_alias'); ?>
						<?php echo $this->form->getInput('created_by_alias'); ?></li>

						<li <?php echo !PagesAndItemsHelper::check_display('item_props_createddate') ? $this->display_none : ''; //if(!$this->helper->check_display('item_props_createddate'))){echo $this->display_none;} ?>>
						<?php echo $this->form->getLabel('created'); ?>
						<?php
						if(!$this->frontend){
							//$this->form->setFieldAttribute('created','type','calendartime');
						}
						?>
						<?php echo $this->form->getInput('created'); ?></li>

						<li <?php echo !PagesAndItemsHelper::check_display('item_props_start') ? $this->display_none : ''; //if(!$this->helper->check_display('item_props_start'))){echo $this->display_none;} ?>>
						<?php echo $this->form->getLabel('publish_up'); ?>
						<?php
						if(!$this->frontend){
							//$this->form->setFieldAttribute('publish_up','type','calendartime');
						}
						echo $this->form->getInput('publish_up');
						?></li>

						<li <?php echo !PagesAndItemsHelper::check_display('item_props_finish') ? $this->display_none : ''; //if(!$this->helper->check_display('item_props_finish'))){echo $this->display_none;} ?>>
						<?php echo $this->form->getLabel('publish_down'); ?>
						<?php
						if(!$this->frontend){
							//$this->form->setFieldAttribute('publish_down','type','calendartime');
						}
						?>
						<?php echo $this->form->getInput('publish_down'); ?></li>

						<?php if ($this->item->modified_by) : ?>
							<li <?php if(!PagesAndItemsHelper::check_display('item_props_modified_by') || $this->frontend){echo $this->display_none;} ?>>
							<?php echo $this->form->getLabel('modified_by'); ?>
							<?php echo $this->form->getInput('modified_by'); ?></li>

							<li <?php if(!PagesAndItemsHelper::check_display('item_props_modified') || $this->frontend){echo $this->display_none;} ?>>
							<?php echo $this->form->getLabel('modified'); ?>
							<?php echo $this->form->getInput('modified'); ?></li>
						<?php endif; ?>

						<?php if ($this->item->version) : ?>
							<li <?php if(!PagesAndItemsHelper::check_display('item_props_revision') || $this->frontend){echo $this->display_none;} ?>>
							<?php echo $this->form->getLabel('version'); ?>
							<?php echo $this->form->getInput('version'); ?></li>
						<?php endif; ?>

						<?php if ($this->item->hits) : ?>
							<li <?php if(!PagesAndItemsHelper::check_display('item_props_hits') || $this->frontend){echo $this->display_none;} ?>>
							<?php echo $this->form->getLabel('hits'); ?>
							<?php echo $this->form->getInput('hits'); ?></li>
						<?php endif; ?>
					</ul>
				</fieldset>
			<?php endif; ?>


			<?php $fieldSets = $this->form->getFieldsets('attribs');?>
			<?php foreach ($fieldSets as $name => $fieldSet) :?>
				<?php // If the parameter says to show the article options or if the parameters have never been set, we will show the article options. ?>

				<?php if ($this->params['show_article_options'] || (( $this->params['show_article_options'] == '' && !empty($this->editoroptions) ))): ?>
					<?php // Go through all the fieldsets except the configuration and basic-limited, which are handled separately below. ?>
					<?php if ($name != 'editorConfig' && $name != 'basic-limited') : ?>

						<?php echo JHtml::_('sliders.panel',JText::_($fieldSet->label), $name.'-options');?>
						<?php if (isset($fieldSet->description) && trim($fieldSet->description)) :?>
							<p class="tip"><?php echo $this->escape(JText::_($fieldSet->description));?></p>
						<?php endif;?>
						<fieldset class="panelform">
							<ul class="adminformlist">
							<?php
							foreach ($this->form->getFieldset($name) as $field) : ?>
								<?php
							/*
							$temp = $field->name;
							$temp = str_replace('jform[attribs][', '', $temp);
							$temp = str_replace(']', '', $temp);
							$field_name = 'item_props_'.$temp;
							*/
								$field_name = 'item_props_'.$field->__get('fieldname');
								?>
								<li <?php
								if($field_name!='item_props_spacer2'){
									if(!PagesAndItemsHelper::check_display($field_name)){
										echo $this->display_none;
									}
								}else{
									//hide line
									echo $this->display_none;
								}
								?>>
								<?php echo $field->label; ?><?php echo $field->input; ?></li>
							<?php endforeach;
							?>
							</ul>
						</fieldset>
					<?php endif ?>
					<?php // If we are not showing the options we need to use the hidden fields so the values are not lost.  ?>
				<?php elseif ($name == 'basic-limited'): ?>
					<?php foreach ($this->form->getFieldset('basic-limited') as $field) : ?>
						<?php  echo $field->input; ?>
					<?php endforeach; ?>

				<?php endif; ?>
			<?php endforeach; ?>
		
			<?php // only j2.5 
					//We need to make a separate space for the configuration so that those fields always show to those wih permissions ?>
			<?php if ( $this->canDo->get('core.admin') && count($this->form->getFieldset('editorConfig')) >0 ):  ?>
				<?php  echo JHtml::_('sliders.panel',JText::_('COM_CONTENT_SLIDER_EDITOR_CONFIG'), 'configure-sliders'); ?>
					<fieldset  class="panelform" >
						<ul class="adminformlist">
							<?php foreach ($this->form->getFieldset('editorConfig') as $field) : ?>
								<li>
									<?php echo $field->label; ?>
									<?php echo $field->input; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					</fieldset>
			<?php endif ?>

			<?php // The url and images fields only show if the configuration is set to allow them.  ?>
			<?php // This is for legacy reasons. ?>
			<?php if ($this->params['show_urls_images_backend']): ?>
				<?php echo JHtml::_('sliders.panel',JText::_('COM_CONTENT_FIELDSET_URLS_AND_IMAGES'), 'urls_and_images-options'); ?>
					<fieldset class="panelform">
						<ul class="adminformlist">
						
							<?php
							/*
							MS: i think this must be an error in J2.5
							<li>
								<?php echo $this->form->getLabel('images'); ?>
								<?php echo $this->form->getInput('images'); ?>
							</li>
							*/
							?>
							<?php foreach($this->form->getGroup('images') as $field): ?>
							<li>
							<?php if (!$field->hidden): ?>
								<?php echo $field->label; ?>
							<?php endif; ?>
								<?php echo $field->input; ?>
							</li>
							<?php endforeach; ?>
							<?php foreach($this->form->getGroup('urls') as $field): ?>
							<li>
							<?php if (!$field->hidden): ?>
								<?php echo $field->label; ?>
							<?php endif; ?>
								<?php echo $field->input; ?>
							</li>
							<?php endforeach; ?>
						</ul>
					</fieldset>
				<?php endif; ?>
<?php endif; ?>