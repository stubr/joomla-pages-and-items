<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

defined('JPATH_BASE') or die;
/**
*/
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'extension.php');


abstract class PagesAndItemsExtensionManager extends PagesAndItemsExtension
{
	public $version = '';//

	/**
	 * Constructor
	 *
	 * @param object $subject The object to observe
	 * @param array  $config  An optional associative array of configuration settings.
	 */
	public function __construct(&$subject, $config = array())
	{
		$files = null; // Define the variable
		parent::__construct($subject, $config);
		jimport('joomla.filesystem.folder');
		$folder = realpath(dirname(__FILE__).'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);
		if (is_string($folder)){$files = JFolder::files($folder,'.xml',false,true);}
		if(is_array($files) && count($files))
		{
			foreach($files as $file)
			{
				$xml = simplexml_load_file($file);
				if ($xml)
				{
					if ( is_object($xml) && is_object($xml->install))
					{
						//ok we have the install file
						//we will get the version
						$element = (string)$xml->version;
						$this->version = $element ? $element : '';
					}
				}
			}
		}
	}


}
