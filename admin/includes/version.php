<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/
defined( '_JEXEC' ) or die( 'Restricted access' );

class PagesAndItemsVersion{
	var $_version	= '3.2.0';
	

	function getVersionNr() {
		return $this->_version;
		jimport('joomla.filesystem.folder');
		$folder = realpath(dirname(__FILE__).'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);
		$files = JFolder::files($folder,'.xml',false,true);
		if(count($files))
		{
			foreach($files as $file)
			{
				$xml = simplexml_load_file($file);
				if ($xml)
				{
					if ( is_object($xml) && (is_object($xml->install) || is_object($xml->extension)))
					{
						//ok we have the install file
						//we will get the version
						$element = (string)$xml->version;
						return $element ? $element : '';
						//$this->version = $element ? $element : '';
						
					}
				}
			}
		}
	}


	
}
