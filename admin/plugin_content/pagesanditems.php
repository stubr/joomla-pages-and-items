<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$ds = DIRECTORY_SEPARATOR;

if(file_exists(JPATH_ADMINISTRATOR.$ds.'components'.$ds.'com_pagesanditems'.$ds.'plugin_content'.$ds.'plugin_content.php')){			
	require_once(JPATH_ADMINISTRATOR.$ds.'components'.$ds.'com_pagesanditems'.$ds.'plugin_content'.$ds.'plugin_content.php');
}

?>