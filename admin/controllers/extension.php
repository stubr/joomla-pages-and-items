<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controller.php');
//require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controller.php');
/**
 * @package		PagesAndItems
*/
class PagesAndItemsControllerExtension extends PagesAndItemsController
{
	var $extensionController = null;

	/**
	 * Constructor.
	 *
	 * @param	array An optional associative array of configuration settings.
	 */
	function __construct( $config = array())
	{
		parent::__construct($config);
		
		$extensionName = JFactory::getApplication()->input->get('extensionName','' ); 
		
		if(JFactory::getApplication()->input->get('extension') != '') 
		{
			//TODO error warning
			$extensionName = JFactory::getApplication()->input->get('extension','' ); 
		}
		
		$extensionFolder = JFactory::getApplication()->input->get('extensionFolder', ''); 
		$extensionType = JFactory::getApplication()->input->get('extensionType', ''); 
		$extensionController = JFactory::getApplication()->input->get('extensionController', ''); 
		if($extensionFolder && $extensionFolder != '')
		{
			$extension_folder = str_replace('/',DIRECTORY_SEPARATOR,$extensionFolder);
			$folder = $extensionType.'s'.DIRECTORY_SEPARATOR.$extension_folder;//.DIRECTORY_SEPARATOR;
			$prefix = str_replace('/',DIRECTORY_SEPARATOR,$extensionFolder);
		}
		else
		{
			$folder = $extensionType.'s';
		}

		if (strpos($extensionName, '.') != false)
		{
			// We have a defined controller/task pair -- lets split them out
			list($extensionName,$controllerName) = explode('.', $extensionName);
			$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$controllerName.'.php');
			$controllerName = ucfirst($controllerName);
		}
		else
		{
			$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controller.php');
			$controllerName = '';

		}
		if($extensionController != '')
		{
			$controllerName = $extensionController;
			$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$controllerName.'.php');
			$controllerName = ucfirst($controllerName);
		}
		
		//TODO eliminate extensionSubTask extension_task sub_task
		
		$cmd = JFactory::getApplication()->input->get('extensionTask', JFactory::getApplication()->input->get('extensionSubTask',JFactory::getApplication()->input->get('extension_task',JFactory::getApplication()->input->get('sub_task',null))));
		if($cmd)
		{
			if (strpos($cmd, '.') != false)
			{
				list($controllerName,$task) = explode('.', $cmd);
				$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$controllerName.'.php');
				$controllerName = ucfirst($controllerName);
			}
			else
			{
				$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controller.php');
			}
		}
		else
		{
			$path = realpath(dirname(__FILE__).'/../extensions'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$extensionName.DIRECTORY_SEPARATOR.'controller.php');
		}
		jimport('joomla.filesystem.file');
		if(JFile::exists($path))
		{
			PagesAndItemsHelper::loadExtensionLanguage($extensionName,$extensionType,$extensionFolder);
			require_once($path);
			if($extensionFolder && $extensionFolder != '')
			{
				$extension_folders = explode('/',$extensionFolder);
				if(count($extension_folders))
				{
					$folders = array();
					for($n = 0; $n < (count($extension_folders)); $n++)
					{
						$folders[] = ucfirst($extension_folders[$n]);
					}
					$extension_folder = implode($folders);
				}
				else
				{
					$extension_folder = ucfirst($extensionFolder);
				}
				$prefix = ucfirst($extensionType).$extension_folder;
			}
			else
			{
				$prefix = ucfirst($extensionType);
			}
			$extension_name = ucfirst($extensionName);

				$className = 'PagesAndItemsControllerExtension'.$prefix.$extension_name.$controllerName;

			$extensionController = new $className();
			$this->extensionController = $extensionController;
			$extensionTask = null;
			if($cmd)
			{
				if (strpos($cmd, '.') != false)
				{
					// We have a defined controller/task pair -- lets split them out
					list($controllerName, $extensionTask) = explode('.', $cmd);
					JFactory::getApplication()->input->set('extensionTask', $extensionTask);
				}
				else
				{
					$extensionTask = $cmd;
					JFactory::getApplication()->input->set('extensionTask', $extensionTask);
				}
			}
		}
		else
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage('no extension controller. extensionType: '.$extensionType.($extensionFolder ? ', extensionFolder: '.$extensionFolder:'').', extensionName: '.$extensionName, 'warning');
			//$this->setMessage(JText::_('COM_MENUS_SAVE_SUCCESS'));
			//TODO enquee Message
			/*
			controller cant load:
			$extensionName,$extensionType,$extensionFolder
			*/
		}
	}

	function display($cachable = false, $urlparams = false)
	{
		$extensionTask = JFactory::getApplication()->input->get('extensionTask',null); 
		$extensionName = JFactory::getApplication()->input->get('extensionName',''); 
		if(JFactory::getApplication()->input->get('extension') != '') 
		{
			//TODO error warning
			$extensionName = JFactory::getApplication()->input->get('extension','' ); 
		}

		$extensionFolder = JFactory::getApplication()->input->get('extensionFolder', ''); 
		$extensionType = JFactory::getApplication()->input->get('extensionType', ''); 

		if(JFactory::getApplication()->input->get('view', 'extension') != 'managers') 
		{
			$lang		= JFactory::getLanguage();
			// Load extension-local file.
			$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, null, false, false)
			||	$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);

			JFactory::getApplication()->input->set('view', JFactory::getApplication()->input->get('view', 'extension'));
			$file = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'extension'.DIRECTORY_SEPARATOR.'tmpl'.DIRECTORY_SEPARATOR.$extensionType.'.php');
			if(file_exists($file))
			{
				JFactory::getApplication()->input->set( 'layout', $extensionType );
			}
			else
			{
				//?????
				JFactory::getApplication()->input->set( 'layout', 'default' );
			}
		}
		parent::display($tpl);

	}

	function doExecute()
	{
		$task = JFactory::getApplication()->input->get('extensionTask', null); 
		if($this->extensionController)
		{
			$lang		= JFactory::getLanguage();
			// Load extension-local file.
			$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, null, false, false)
			||	$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
			$this->extensionController->execute($task);
			// Redirect if set by the controller
			$this->extensionController->redirect();
		}
	}

}