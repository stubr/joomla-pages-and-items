<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controller.php');

/**
 * @package		PagesAndItems
*/
class PagesAndItemsControllerItemType extends PagesAndItemsController
{
	function __construct( $config = array())
	{
		parent::__construct($config);

	}

	function getDispatcher($item_type)
	{
		$path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..');
		require_once($path.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'itemtypehelper.php');
		$extensions = ExtensionItemtypeHelper::importExtension(null, $item_type,true,null,true);

		$dispatcher =JDispatcher::getInstance();
		return $dispatcher;
	}

	function config_itemtype_save()
	{
		//here we need the model base for future configcustomitemtype
		//$model =$this->getModel('Base','PagesAndItemsModel');
		$item_type = JFactory::getApplication()->input->get('item_type', ''); 
		$dispatcher = $this->getDispatcher($item_type);
		$msg = '';
		$dispatcher->trigger('onItemtypeConfig_save',array(&$msg,$item_type));
		$msg = '';
		//redirect
		if(JFactory::getApplication()->input->get('sub_task', '')=='apply')
		{
			$url = 'index.php?option=com_pagesanditems&view=config_itemtype&item_type='.$item_type;
		}
		else
		{
			$url = 'index.php?option=com_pagesanditems&view=config&tab=itemtypes';
		}
		$this->setRedirect(JRoute::_($url, false), $msg);
		//$model->redirect_to_url( $url, $msg);

	}

}
