<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controlleradmin');

class PagesAndItemsControllerArticles extends JControllerAdmin{

	//protected $option = 'com_pagesanditems';
	public $helper;
	public $controller_item;
	
	function __construct( $default = array()){	
		
		//get helper
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
		$this->helper = new PagesAndItemsHelper();
		
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'item.php');
		$this->controller_item = new PagesAndItemsControllerItem();
		
		parent::__construct($default);		
	}
	
	function article_publish(){
	
		JSession::checkToken() or jexit('Invalid Token');		
		$article_id = JFactory::getApplication()->input->get('article_id', '');		
		$this->helper->item_state($article_id, '1');		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEM_PUBLISHED'));
	}
		
	function article_unpublish(){
	
		JSession::checkToken() or jexit('Invalid Token');		
		$article_id = JFactory::getApplication()->input->get('article_id', '');		
		$this->helper->item_state($article_id, '0');		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEM_UNPUBLISHED'));
	}
	
	function article_feature(){
	
		JSession::checkToken() or jexit('Invalid Token');		
		$article_id = JFactory::getApplication()->input->get('article_id', '');		
		$this->controller_item->put_item_on_frontpage($article_id);		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEM_SAVED'));
	}
		
	function article_unfeature(){
	
		JSession::checkToken() or jexit('Invalid Token');		
		$article_id = JFactory::getApplication()->input->get('article_id', '');		
		$this->controller_item->take_item_off_frontpage($article_id);	
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEM_SAVED'));
	}
	
	function articles_order_save(){
	
		$db = JFactory::getDBO();
		
		JSession::checkToken() or jexit('Invalid Token');
		
		$orders = JFactory::getApplication()->input->get('orders', array(), 'array');		
		$order_ids = JFactory::getApplication()->input->get('order_ids', array(), 'array');		
				
		for($n = 0; $n < count($order_ids); $n++){		
			$order = $orders[$n];			
			$order_id = $order_ids[$n];					
			
			$query = $db->getQuery(true);		
			$query->update('#__content');
			$query->set('ordering='.$db->q($order));				
			$query->where('id='.(int)$order_id);
			$db->setQuery((string)$query);
			$db->query();				
		}		
		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEMS_ORDER_SAVED'));
	}
	
	function reorder_article_up(){
	
		JSession::checkToken() or jexit('Invalid Token');
		$this->reorder_article('up');	
	}
	
	function reorder_article_down(){
	
		JSession::checkToken() or jexit('Invalid Token');
		$this->reorder_article('down');	
	}	
	
	function reorder_article($direction){
	
		$db = JFactory::getDBO();
	
		JSession::checkToken() or jexit('Invalid Token');
		$article_id = JFactory::getApplication()->input->get('article_id', '');			
		
		//get data of article to move
		$old_order = 0;
		$catid = 0;
		$parentid = '';
		$query = $db->getQuery(true);
		$query->select('ordering, catid');
		$query->from('#__content');		
		$query->where('id='.$article_id);		
		$rows = $db->setQuery($query, 0, 1);				
		$rows = $db->loadObjectList();
		foreach($rows as $row){		
			$old_order = $row->ordering;	
			$catid = $row->catid;					
		}
		
		if($direction=='down'){
			$orderdirection = 'ASC';
		}else{
			$orderdirection = 'DESC';
		}
		
		//get articles in order
		$query = $db->getQuery(true);
		$query->select('id, ordering');
		$query->from('#__content');		
		$query->where('catid='.$catid);		
		$query->order('ordering '.$orderdirection);
		$rows = $db->setQuery($query);				
		$rows = $db->loadObjectList();		
			
		//check which id has the new order
		$id_of_article_moving = 0;
		$order_of_article_moving = 0;
		$next = 0;
		foreach($rows as $row){	
			if($next){
				$id_of_article_moving = $row->id;
				$order_of_article_moving = $row->ordering;
				break;	
			}				
			if($row->id==$article_id){
				$next = 1;
			}					
		}		
		
		//update the article
		$query = $db->getQuery(true);		
		$query->update('#__content');
		$query->set('ordering='.$order_of_article_moving);				
		$query->where('id='.$article_id);
		$db->setQuery($query);
		$db->query();

		
		//update the menuitem which has to switch position
		$query = $db->getQuery(true);		
		$query->update('#__content');
		$query->set('ordering='.$old_order);				
		$query->where('id='.$id_of_article_moving);
		$db->setQuery($query);
		$db->query();		
		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEMS_ORDER_SAVED'));
	}	
	
	function articles_change_state($new_state, $message){
	
		JSession::checkToken() or jexit('Invalid Token');
		$cid = JFactory::getApplication()->input->get('cid', null, 'array');
		if(count($cid)){
			for($n = 0; $n < count($cid); $n++){
				$this->helper->item_state($cid[$n], $new_state);
			}
		}else{
			echo JText::_('COM_PAGESANDITEMS_NO_ITEMS_SELECTED');
			exit();
		}
		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_($message));
	}
	
	function articles_publish(){		
		$this->articles_change_state('1', 'COM_PAGESANDITEMS_ITEMS_PUBLISHED');
	}
	
	function articles_unpublish(){		
		$this->articles_change_state('0', 'COM_PAGESANDITEMS_ITEMS_UNPUBLISHED');
	}
	
	function articles_archive(){		
		$this->articles_change_state('2', 'COM_PAGESANDITEMS_ITEMS_ARCHIVED');
	}
	
	function articles_trash(){		
		$this->articles_change_state('-2', 'COM_PAGESANDITEMS_ITEMS_TRASHED');
	}
	
	function articles_delete(){		
		$this->articles_change_state('delete', 'COM_PAGESANDITEMS_ITEMS_DELETED');
	}
	
	function articles_featured(){
		
		JSession::checkToken() or jexit('Invalid Token');
		$cid = JFactory::getApplication()->input->get('cid', null, 'array');
		if(count($cid)){
			for($n = 0; $n < count($cid); $n++){				
				$this->controller_item->put_item_on_frontpage($cid[$n]);	
			}
		}else{
			echo JText::_('COM_PAGESANDITEMS_NO_ITEMS_SELECTED');
			exit();
		}
		
		$this->setRedirect('index.php?option=com_pagesanditems&view=articles', JText::_('COM_PAGESANDITEMS_ITEM_SAVED'));
	}
	
	
}

?>