<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
jimport('joomla.client.helper');


/**
is this file still used anywhere? seems deprecated.
*/
require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controller.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'customitemtype.php');

/**
 * @package		PagesAndItems
 * Ajax
*/
class PagesAndItemsControllerAjax extends PagesAndItemsController
{
	/**
	 * Custom Constructor
	 */
	function __construct( $default = array())
	{
		parent::__construct($default);
	}

	

	
}