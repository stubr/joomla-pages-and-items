<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

// No direct access.
defined('_JEXEC') or die;

require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'extension.php');
/**
 * @package		PagesAndItems
*/
class PagesAndItemsControllerManager extends PagesAndItemsControllerExtension
{
	/**
	 * Constructor.
	 *
	 * @param	array An optional associative array of configuration settings.
	 */
	function __construct( $config = array())
	{
		parent::__construct($config);
	}

	function display($cachable = false, $urlparams = false)
	{
		$extensionTask = JFactory::getApplication()->input->get('extensionTask',null); 
		$extensionName = JFactory::getApplication()->input->get('extensionName','' ); 
		if(JFactory::getApplication()->input->get('extension') != '')
		{
			//TODO error warning
			$extensionName = JFactory::getApplication()->input->get('extension','' ); 
		}
		$extensionFolder = JFactory::getApplication()->input->get('extensionFolder', ''); 
		$extensionType = JFactory::getApplication()->input->get('extensionType', ''); 
		
		JFactory::getApplication()->input->set('view', JFactory::getApplication()->input->get('view', 'managers'));
		
		JFactory::getApplication()->input->set( 'layout', 'manager' );
		parent::display($tpl);
	}
}