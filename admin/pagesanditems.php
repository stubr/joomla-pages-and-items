<?php
/**
* @version		3.1.2
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

defined('_JEXEC') or die;

jimport('joomla.filesystem.path');

//ACL
if (!JFactory::getUser()->authorise('core.manage', 'com_pagesanditems'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}


//get helper


require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');

// Include dependancies
jimport('joomla.application.component.controller');
	$version = new JVersion();
	/*
	$view = JFactory::getApplication()->input->get('view',null); 
	if($view)
	{
		require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'managerhelper.php');
		ExtensionManagerHelper::importExtension(null, null,true,null,true);
		$dispatcher =JDispatcher::getInstance();
		$dispatcher->trigger('onStartView',array());
		$dispatcher->trigger('detach',array('manager'));
	}
*/
	$config = array(); //'view_path'=>JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views');
	if($version->getShortVersion() < '1.6')
	{
		/*
		 * Joomla 1.5
		*/
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'controllerJ15.php');
		$controller = controllerJ15::getInstance('PagesAndItems',$config);
	}
	elseif($version->getShortVersion() < '3')
	{
		/*
		 * Joomla 1.6
		*/
		$controller= JController::getInstance('PagesAndItems',$config);
	}
	else
	{
		/*
		 * Joomla 3
		*/
		$controller= JControllerLegacy::getInstance('PagesAndItems',$config);
	}
	//$controller->addViewPath( JPATH_COMPONENT_SITE.DIRECTORY_SEPARATOR.'views' );
	// Execute the task.
	$controller->execute(JFactory::getApplication()->input->get('task')); 
	$controller->redirect();




?>