<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/
//-- No direct access
defined('_JEXEC') or die();

jimport('joomla.plugin.plugin');

/**
 * @package		PagesAndItems
 * @subpackage	system-plugin for Pages-and-Items (com_pagesanditems)
 */
class plgSystemPagesanditems extends JPlugin
{
	/**
	 * Constructor
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 */

	function plgSystemPages_and_items( &$subject, $config )
	{
		parent::__construct( $subject, $config );
		// Do some extra initialisation in this constructor if required
	}


	function onAfterInitialise() 
	{

		$app = JFactory::getApplication();
		if (!$app->isAdmin()) 
		{
			return;
		}
		
		$option = JFactory::getApplication()->input->get('option');
		$task = JFactory::getApplication()->input->get('task');
		$view = JFactory::getApplication()->input->get('view');
		
		//here we check if an menutype is before change
		if($option == 'com_menus' && ($task == 'menu.save' || $task == 'menu.apply' || $task == 'menu.save2new') ) // && $view == 'menu')
		{
			$id	= JFactory::getApplication()->input->get('id',0,'int');
			if($id)
			{
				//check menutype if change
				$data		= JFactory::getApplication()->input->get('jform', array(), 'array');
				$menutypeNew = isset($data['menutype']) ? $data['menutype'] : 0;
				$menutypeOld = '';
				$db = JFactory::getDBO();
				$db->setQuery("SELECT title, menutype FROM #__menu_types WHERE id = '$id' ");
				$row = $db->loadObject();
				if($row)
				{
					$menutypeOld = $row->menutype;
					$app->setUserState("com_pagesanditems.com_menus.menutype.old",($menutypeOld == $menutypeNew) ? 0 : $menutypeOld);
					$app->setUserState("com_pagesanditems.com_menus.menutype.new",($menutypeOld == $menutypeNew) ? 0 : $menutypeNew);
					$app->setUserState("com_pagesanditems.com_menus.menutype.id",($menutypeOld == $menutypeNew) ? 0 : $id);
				}
			}
		}
		//here we check is an menutype before delete
		if($option == 'com_menus' && $task == 'menus.delete' )
		{
			// Get items
			$cid	= JFactory::getApplication()->input->get('cid', array(), 'array');
			if (!is_array($cid) || count($cid) < 1) {
				JError::raiseWarning(500, JText::_('COM_MENUS_NO_MENUS_SELECTED'));
			} else {

				// Make sure the item ids are integers
				jimport('joomla.utilities.arrayhelper');
				JArrayHelper::toInteger($cid);
				//we need the menutype as array
				$db = JFactory::getDBO();
				$menutypes = array();
				foreach($cid as $id)
				{
					$db->setQuery("SELECT menutype FROM #__menu_types WHERE id='$id' ");
					$row = $db->loadObject();
					if($row)
					{
						$menutypes[] = $row->menutype;
					}
				}
				$app->setUserState("com_pagesanditems.com_menus.menus.ids",$menutypes);
			}
		}
		return true;
	}

	function onAfterRoute(){

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		
		$option = JFactory::getApplication()->input->get('option');
		$task = JFactory::getApplication()->input->get('task');
		$view = JFactory::getApplication()->input->get('view');
		if (!$app->isAdmin()){
			//frontend
			
			$task = JFactory::getApplication()->input->get('task', '', 'get'); 
			$layout = JFactory::getApplication()->input->get('layout', '', 'get'); 
			$Itemid = JFactory::getApplication()->input->get('Itemid', '', 'get'); 			

			if($option=='com_content' && ($task=='edit' || $layout =='form' || $view == 'form' || $layout == 'edit'))
			{
			
			
				if(file_exists(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php'))
				{
					require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
				}
				else
				{
					return;
				}
				$config = PagesAndItemsHelper::getConfig();

				//get config to check if we need to do a redirect
				$use_pi_frontend_editting = 0;

				if($config['use_pi_frontend_editting']){
					$use_pi_frontend_editting = $config['use_pi_frontend_editting'];
				}

				//only redirect if in pi-config use_pi_frontend_editting is activated
				if($use_pi_frontend_editting)
				{
					//$id = JFactory::getApplication()->input->get('id', JFactory::getApplication()->input->get('a_id', '', 'get'), 'get'); 
					//ce: this line parses the wrong id when SEF urls are used
					$id = JFactory::getApplication()->input->get('a_id', ''); 
					
					if(strpos($id, ':'))
					{
						$pos = strpos($id, ':');
						$item_id = intval(substr($id, 0, $pos));
					}
					else
					{
						$item_id = intval($id);
					}
					
				

					if($use_pi_frontend_editting == 2)
					{
						$database = JFactory::getDBO();
						$database->setQuery("SELECT * FROM #__pi_item_index WHERE item_id='$item_id'");
						$item_row = $database->loadObject();
						if(!$item_row)
						{
							return;
						}
					}
					$menu_id = JFactory::getApplication()->input->get('Itemid', '', 'get'); 

					//redirect item new
					if($option=='com_content' && (($view=='article' && $layout=='form') || ($view=='form' && $layout=='edit' && !JFactory::getApplication()->input->get('a_id', null)) ) )
					{
						//echo 'new item';
						$sub_task = 'new';
					}

					//redirect item edit
					if($option=='com_content' && (($view=='article' && $task=='edit') || ($view=='form' && $layout=='edit' && JFactory::getApplication()->input->get('a_id', null)) ))
					{
						$sub_task = 'edit';
					}

					$referer = '';
					if($config['item_save_redirect']=='current'){
						$referer = '&return='.JFactory::getApplication()->input->get('return',  '', 'string');
					}
					$layout_url = '';
					if($layout=='form'){
						$layout_url = '&layout=form';
					}
					
					
					$default_cat = '';
					if(!$item_id){
						//new article
						//check if there is any default category set in the menu-item
						//get the refering menu item
						$ref_menu_item = JRequest::getVar('Itemid', '');						
						
						//get the default category from the menu item
						$query = $db->getQuery(true);
						$query->select('params');
						$query->from('#__menu');						
						$query->where('id='.$db->q($ref_menu_item));						
						$rows = $db->setQuery($query);				
						$rows = $db->loadObjectList();
						$params = '';	
						foreach($rows as $row){		
							$params = $row->params;	
						}						
						
						if($params){
							$registry = new JRegistry;
							$registry->loadString($params);
							$result = $registry->toArray();
							$default_cat = '&category='.$result['catid'];
						}
						
					}					
					
					//checkin=1 caused the frontend to crash when creating a new article.
					//$url = 'index.php?option=com_pagesanditems&view=item'.$layout_url.'&sub_task='.$sub_task.'&checkin=1&item_id='.$item_id.'&itemId='.$item_id.'&Itemid='.$Itemid.'&pageId='.$menu_id.$referer;
					$url = 'index.php?option=com_pagesanditems&view=item'.$layout_url.'&sub_task='.$sub_task.'&item_id='.$item_id.'&itemId='.$item_id.'&Itemid='.$Itemid.'&pageId='.$menu_id.$referer.$default_cat;
					$app->redirect($url);
				}
				
				
			}
			
			
		}else{
			//backend
						
			if($option == 'com_menus') // && ($task == 'menu.save' || $task == 'menu.apply' || $task == 'menu.save2new') ) // && $view == 'menu')
			{
				$data	= JFactory::getApplication()->input->get('jform', array(), 'array');
				if(!$data)
				{
					//here we check if we have an changed menutype
					$id = $app->getUserState("com_pagesanditems.com_menus.menutype.id");
					if($id)
					{
						//first step is if has the #__menu_types is saved ore error on save?
						$menutypeNew = $app->getUserState("com_pagesanditems.com_menus.menutype.new");
						$db = JFactory::getDBO();
						$db->setQuery("SELECT title, menutype FROM #__menu_types WHERE id = '$id' ");
						$row = $db->loadObject();
						if($row)
						{
							if($row->menutype == $menutypeNew)
							{
								//ok the item in #__menu_types is saved with new menutype
								//we must get the old
								$menutypeOld = $app->getUserState("com_pagesanditems.com_menus.menutype.old");
								//get PagesAndItemsHelper
								require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
								$config = PagesAndItemsHelper::getConfigAsRegistry();
								//we must take an look in pagesanditems config->menu to set the changed menutype
								if($config->get('menus.'.$menutypeOld,0))
								{
									//$menu = $config->get('menus.'.$menutypeOld);
									$title = $row->title; //$menu[1];
									$config->set('menus.'.$menutypeOld,array($menutypeNew,$title));
									//we have an array like  menu['mainmenu']array('mainmenu','Main'),
									//and if we change  menu['mainmenu']array('newmenu','Main'),
									//in the saved config we use only 'newmenu','Main'
									
									PagesAndItemsHelper::saveConfig($config->toArray());
								}
							}
						}
					}
					
					
					//here we check if an menutype is deleted
					$cid = $app->getUserState("com_pagesanditems.com_menus.menus.ids",array());
					$changed = false;
					if(is_array($cid) && count($cid) && count($cid) >= 1)
					{
						require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
						$config = PagesAndItemsHelper::getConfigAsRegistry();
						foreach($cid as $menutype)
						{
							if($config->get('menus.'.$menutype,0))
							{
								//check if delete $menutype in #__menu_types
								$db = JFactory::getDBO();
								$db->setQuery("SELECT menutype FROM #__menu_types WHERE menutype = '$menutype' ");
								$row = $db->loadObject();
								if(!$row)
								{
									//here we set the array menus[$menutype] to array()
									//can we remove from the JRegistry?
									$config->set('menus.'.$menutype,array());
									$changed = true;
								}
							}
						}
						if($changed)
						{
							PagesAndItemsHelper::saveConfig($config->toArray());
						}
					}
					$app->setUserState("com_pagesanditems.com_menus.menutype.old",0);
					$app->setUserState("com_pagesanditems.com_menus.menutype.new",0);
					$app->setUserState("com_pagesanditems.com_menus.menutype.id",0);
					$app->setUserState("com_pagesanditems.com_menus.menus.ids",0);
				}
				
	
			}
		}
		
		return true;
	}


	/**
	 * Do something onAfterRender
	 */
	function onAfterRender()
	{
	
	
			
		$option = JFactory::getApplication()->input->get('option', '', 'get'); 
		//$option = '';
		$view = JFactory::getApplication()->input->get('view', '', 'get'); 
		$application = JFactory::getApplication();
		$db = JFactory::getDBO();
		if (!$application->isAdmin())
		{


			
		}else{
			//backend


			$option = JFactory::getApplication()->input->get('option', '', 'get'); 
			$sub_task = JFactory::getApplication()->input->get('sub_task', '');
			$pageType = JFactory::getApplication()->input->get('pageType', '');

			$task = JFactory::getApplication()->input->get('task');

			$layout = JFactory::getApplication()->input->get('layout');
			if( $option == 'com_content' && ($task == 'edit' || ($view == 'article' && $layout == 'edit') ) )
			{
				$body = JResponse::getBody();
				if(file_exists(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php'))
				{
					require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagesanditems.php');
				}
				else
				{
					return;
				}
				$config = PagesAndItemsHelper::getConfig();
				//&& !$config['plugin_system_disable_content']
				if(!$config['plugin_system_hidde_button']  && !$config['plugin_system_add_button'])
				{
					return;
				}

				$add_button = $config['plugin_system_add_button'] ? 1 : 0;
				$hidde_button  = $config['plugin_system_hidde_button'] ? 1 : 0;


				$id = null;
				$ids = JFactory::getApplication()->input->get('cid',null); 
				if(isset($ids) && $task == 'edit')
				{
					$id = $ids[0];
				}
				else
				{
					$id = JFactory::getApplication()->input->get('id',null); 
				}

				$item_type = false;

				if($id)
				{
					$database = JFactory::getDBO();
					$database->setQuery("SELECT itemtype FROM #__pi_item_index WHERE item_id='$id' LIMIT 1");
					$rows = $database->loadObjectList();

					if($rows)
					{
						$itemrow = $rows[0];
						$item_type = $itemrow->itemtype;
					}
				}
				//only if item has been made with PI
				if($item_type!='')
				{
					$lang = JFactory::getLanguage();
					//$lang->load('com_pagesanditems', JPATH_ADMINISTRATOR, null, false, false);
					$extension = 'com_pagesanditems';
					$lang->load(strtolower($extension), JPATH_ADMINISTRATOR, null, false, false) || $lang->load(strtolower($extension), JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);

					//$body = JResponse::getBody();
					$nl = "\n";
					$script_add = '';
					$style = '';
					$version = new JVersion();
					$joomlaVersion = $version->getShortVersion();
					if($add_button)
					{
						$style .= '<style type="text/css">'.$nl;
						$style .= '.icon-32-PI';
						$style .= '{';
						$dirIcons = JUri::root(true).'/'.str_replace(DIRECTORY_SEPARATOR,'/',str_replace(JPATH_ROOT.DIRECTORY_SEPARATOR,'',realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'media').DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'icons'));
						$style .= 'background-image:url("'.$dirIcons.'/icon-32-pi.png");';
						$style .= '}'.$nl;
						$style .= '</style>'.$nl;
						$database = JFactory::getDBO();

						$database->setQuery("SELECT * FROM #__content WHERE id='$id' LIMIT 1");
						$contentrow = $database->loadObject();
						if($contentrow)
						{
							//$link = 'index.php?option=com_pagesanditems&view=item&sub_task=edit&pageId='.$contentrow->catid.'&itemId='.$id.'&pageType=content_article&checkin=1';
							$link = 'index.php?option=com_pagesanditems&view=item&sub_task=edit&itemId='.$id.'&checkin=1&categoryId='.$contentrow->catid;
							//TODO checkin the article

							$script_add = '';
							if($joomlaVersion < '1.6')
							{
								$script_add .= 'var elementTD = new Element(\'td\');' .$nl;
								$script_add .= 'elementTD.setProperty(\'class\',\'button\');' .$nl;
								$script_add .= 'elementTD.setProperty(\'id\',\'PI_'.$contentrow->catid.'\');' .$nl;
								$script_add .= 'var elementA = new Element(\'a\');' .$nl;
								$script_add .= 'elementA.setProperty(\'class\',\'toolbar\');' .$nl;

								$script_add .= 'elementA.innerHTML = \''.JText::_('edit').'\';' .$nl;

								$script_add .= 'elementA.setProperty(\'href\',\''.$link.'\');' .$nl;
								$script_add .= 'elementA.injectInside(elementTD);' .$nl;

								$script_add .= 'var elementSpan = new Element(\'span\');' .$nl;
								$script_add .= 'elementSpan.setProperty(\'class\',\'icon-32-PI\');' .$nl;

								$script_add .= 'elementSpan.setProperty(\'Title\',\''.JText::_('COM_PAGESANDITEMS_PLG_SYSTEM_PAGES_AND_ITEMS_OPEN_IN_PI').'\');' .$nl;

								$script_add .= 'elementSpan.injectTop(elementA);' .$nl;

								$script_add .= 'var elementApply = $(\'toolbar-apply\');' .$nl;
								$script_add .= 'elementTD.injectAfter(elementApply);' .$nl;
							}
							else
							{
								$script_add .= 'var elementTD = new Element(\'li\');' .$nl;
								$script_add .= 'elementTD.setProperty(\'class\',\'button\');' .$nl;
								$script_add .= 'elementTD.setProperty(\'id\',\'PI_'.$contentrow->catid.'\');' .$nl;
								$script_add .= 'var elementA = new Element(\'a\');' .$nl;
								$script_add .= 'elementA.setProperty(\'class\',\'toolbar\');' .$nl;

								$script_add .= 'elementA.innerHTML = \''.JText::_('edit').'\';' .$nl;


								$script_add .= 'elementA.setProperty(\'href\',\''.$link.'\');' .$nl;
								$script_add .= 'elementA.injectInside(elementTD);' .$nl;

								$script_add .= 'var elementSpan = new Element(\'span\');' .$nl;
								$script_add .= 'elementSpan.setProperty(\'class\',\'icon-32-PI\');' .$nl;

								$script_add .= 'elementSpan.setProperty(\'Title\',\''.JText::_('COM_PAGESANDITEMS_PLG_SYSTEM_PAGES_AND_ITEMS_OPEN_IN_PI').'\');' .$nl;

								$script_add .= 'elementSpan.injectTop(elementA);' .$nl;

								$script_add .= 'var elementCancel = $(\'toolbar-cancel\');' .$nl;
								$script_add .= 'elementTD.injectBefore(elementCancel);' .$nl;
							}
						}
					}


				$script_remove = '';
				//delete save and apply Button ?
				if($hidde_button && $item_type!='text')
				{
					if($joomlaVersion < '1.6')
					{
						$script_remove .= '$(\'toolbar-save\').remove();';
						$script_remove .= '$(\'toolbar-apply\').remove();';
					}
					else
					{
						$script_remove .= '$(\'toolbar-save\').destroy();';
						$script_remove .= '$(\'toolbar-apply\').destroy();';
						$script_remove .= '$(\'toolbar-save-new\').destroy();';
						$script_remove .= '$(\'toolbar-save-copy\').destroy();';
					}
				}

				// create the javascript
				// We can't use $document, because it's already rendered

				$script_onload = '';
				$script_disable = '';
				
					$script = $nl. '<!-- system plugin Pages and items -->' .$nl.$style.
					'<script type="text/javascript">' .$nl.
					'// <!--' .$nl.
					'window.addEvent(\'domready\', function()' .$nl.
					'{' .$nl.
						$script_add.
						$script_remove.
						//$script_disable.
					'});' .$nl.

					'window.addEvent(\'load\', function()' .$nl.
					'{' .$nl.
						$script_onload.
					'});' .$nl.
					'// -->' .$nl.
					'</script>' .$nl.
					'<!-- / system plugin Pages and items -->';
					$body = str_replace('</head>', $script.'</head>', $body);
					
				}
				
				
				
				
				JResponse::setBody($body);
			}
			
			/*
			//fix category select while editting an article in PI
			//as that goes wrong if just using jform in Joomla 3
			$version = new JVersion;
			if($option=='com_pagesanditems' && $view=='item' && $version->RELEASE >= '3.0'){
				$buffer = JResponse::getBody();
				
				$buffer = $this->set_cat_id_in_buffer($buffer);
				
				JResponse::setBody($buffer);
			}
			*/
			
		}
		
		//fix category select while editting an article in PI
		//as that goes wrong if just using jform in Joomla 3
		$version = new JVersion;
		if($option=='com_pagesanditems' && $view=='item' && $version->RELEASE >= '3.0'){
			$buffer = JResponse::getBody();
			
			$buffer = $this->set_cat_id_in_buffer($buffer);
			
			JResponse::setBody($buffer);
		}
		
		return;
	}
	
	function set_cat_id_in_buffer($buffer){
	
		$db = JFactory::getDBO();
		
		$current_category = 0;
		
		//override category if parsed via url
		$current_category = JRequest::getVar('category', 0);
	
		//get current category		
		$item_id = JRequest::getVar('itemId', '');
		$query = $db->getQuery(true);
		$query->select('catid');
		$query->from('#__content');				
		$query->where('id='.$db->q($item_id));				
		$rows = $db->setQuery($query);				
		$rows = $db->loadObjectList();
		foreach($rows as $row){		
			$current_category = $row->catid;
		}
		
		//new item or instance of item
		if((!$item_id && JRequest::getVar('pageType', '')=='content_category_blog') || (JRequest::getVar('item_type', '')=='other_item')){					
			
			//get the category id from the menu-item
			$pageId = JRequest::getVar('pageId', '');
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__menu');
			$query->where('id='.$pageId);						
			$rows = $db->setQuery($query);				
			$rows = $db->loadObjectList();
			
			$url = '';
			foreach($rows as $row){		
				$url = $row->link;							
			}
			
			if(strpos($url, 'index.php?option=com_content&view=category&layout=blog') !== FALSE){			
				$current_category = str_replace('index.php?option=com_content&view=category&layout=blog&id=','',$url);
			}
			elseif(strpos($url, 'index.php?option=com_content&view=category') !== FALSE){
				$current_category = str_replace('index.php?option=com_content&view=category&id=','',$url);
			}else{
				$current_category = 0;
			}
							
		}
		
		
		
		//get categories		
		$query = $db->getQuery(true);
		$query->select('id, title, level');
		$query->from('#__categories');
		$query->where('parent_id > 0');
		$query->where('extension='.$db->q('com_content'));				
		$query->order('lft');
		$rows = $db->setQuery($query);				
		$rows = $db->loadObjectList();
		
		//build category select options
		$category_select_new = '';
		foreach($rows as $row){							
			$title = str_repeat('- ',$row->level).$row->title;
			$category_select_new .= '<option value="'.$row->id.'"';
			if($row->id==$current_category){
				$category_select_new .= ' selected="selected"';
			}
			$category_select_new .= '>'.$title.'</option>';
		}			
		
		//replace the old category select with the new one
		$regex = "/\<select id=\"jform_catid\"(.*?)\<\/select\>/is";						
		preg_match_all($regex, $buffer, $matches);									
		if(isset($matches[1][0])){				
			$ori = $matches[1][0];
			$buffer = str_replace($ori, $ori.$category_select_new, $buffer);
		}
		return $buffer;
	}
	
}
?>