<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

//No direct access
defined('_JEXEC') or die;

//jimport('joomla.application.component.controller');
jimport('joomla.client.helper');
jimport('joomla.application.component.controller');

if (!class_exists('PagesAndItemsControllerBase')) {
    if (interface_exists('JController')) {

        abstract class PagesAndItemsControllerBase extends JControllerLegacy {
            
        }

    } else {

        abstract class PagesAndItemsControllerBase extends JController {
            
        }

    }
}

require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'models/base.php');

class PagesAndItemsController extends PagesAndItemsControllerBase
{

	//public $joomlaVersion;
	public $app;

	function __construct( $default = array())
	{
		parent::__construct($default);
		require_once(dirname(__FILE__).'/helpers/pagesanditems.php');

		//$version = new JVersion();
		//$this->joomlaVersion = $version->getShortVersion();
		$this->app = JFactory::getApplication();
		/*
		$lang = JFactory::getLanguage();
		*/
		/*
		load sys language
		*/
		/*
		if($isSuperAdmin = PagesAndItemsHelper::getIsSuperAdmin())
		{
			$lang		= JFactory::getLanguage();
			// Load extension-local file.
			$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, null, false, false)
			||	$extension = 'com_joomfish';
		$lang = JFactory::getLanguage();
		$lang->load(strtolower($extension), JPATH_ADMINISTRATOR, null, false, false);
		}
		*/
	}

	function display($cachable = false, $urlparams = false)
	{
		/*
		$lang = JFactory::getLanguage();
		*/
		$isAdmin = PagesAndItemsHelper::getIsAdmin();
		$isSuperAdmin = PagesAndItemsHelper::getIsSuperAdmin();
		if($isAdmin)
		{
			$view = JFactory::getApplication()->input->get('view'); 
			$layout = JFactory::getApplication()->input->get('layout'); 
			$menutype = JFactory::getApplication()->input->get('menutype',0); 
			$pageId = JFactory::getApplication()->input->get('pageId',0); 
			$sub_task = JFactory::getApplication()->input->get('sub_task',''); 
			$subsub_task = JFactory::getApplication()->input->get('subsub_task',''); 
//check if view is set, and if not, redirect to default
			if($view==''){
				$config = PagesAndItemsHelper::getConfigAsRegistry();
				if($config->get('default_admin_page')=='page'){	
					JFactory::getApplication()->input->set('layout', 'root');
					$layout = 'root';
				}
				JFactory::getApplication()->input->set('view', $config->get('default_admin_page'));
				$view = $config->get('default_admin_page');
			}
			
			//switch session between articles modus and non-articles modus
			$app = JFactory::getApplication();			
			if($view=='articles'){				
				$app->setUserState( "com_pagesanditems.articles_modus", 'yes');
			}elseif($view=='item' || $view=='itemtypeselect'){
				//do nothing
			}else{
				$app->setUserState( "com_pagesanditems.articles_modus", '');				
			}
			
			//$redirect_url = $app->getUserState( "com_pagesanditems.articles_modus", '' );
			//echo $redirect_url;
			
			if($view=='articles'){
				//normal MVC model
				parent::display();
				return true;
			}
			
			//here i test an new method
			/*
			if(!$view || $view == 'page')
			{
				require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'page.php');
				PagesAndItemsHelperPage::onBeforeView();
			}
			*/
			//here i test an new method
			if(!$view || ($view == 'page' && $layout == 'root' && !$menutype && $sub_task != 'new' && $sub_task != 'newMenutype'))
			{
				$currentMenutype = PagesAndItemsHelper::getCurrentMenutype();
				$msg = null;
				if($currentMenutype)
				{
					$menutype = $currentMenutype;
				}
				else
				{
					$app = JFactory::getApplication();
					//no menutypes display an message?
					$msg = JText::_('COM_PAGESANDITEMS_NO_MENUS_SELECTED');
					$app->enqueueMessage($msg);
					$menutype = '';
					//ms: add if we have in root edit/create menutype JFactory::getApplication()->input->set('subtask', 'new');
				}
				JFactory::getApplication()->input->set('view', 'page');
				JFactory::getApplication()->input->set('layout', 'root');
				
				//JInput::set('some','some');
			}
			elseif($view == 'page' && $pageId && !$menutype)
			{
				if($row = PagesAndItemsHelper::getMenuitem($pageId))
				{
					$menutype = $row->menutype;
				}
			}
			$input = new JInput;
			$menutype ? $input->set('menutype', $menutype) : 0;
			
			
			// redirect to ... if no view is set
			//if(!$view || ($view == 'page' && $layout == 'root' && !$menutype ) || ($view == 'page'  && $layout != 'root' && !$pageId ))
			/*
			if(!$view || ($view == 'page' && $layout == 'root' && !$menutype && $sub_task != 'new' && $subsub_task != 'menutype')) //ms: add if we have in root edit/create menutype && $sub_task != 'new' ))
			{
				
				if ($modelPage =$this->getModel('Page','PagesAndItemsModel'))
				{
					//$currentMenutype = $modelPage->getCurrentMenutype();
					//if($currentMenutype)
					//{
					//	$url = 'index.php?option=com_pagesanditems&view=page&layout=root&menutype='.$currentMenutype;
					//	$msg = '';
					//}
					//else
					{//
					//	//no menutypes display an message?
					//	$msg = JText::_(COM_PAGESANDITEMS_NO_MENUS_SELECTED);
					//	$url = 'index.php?option=com_pagesanditems&view=page&layout=root&menutype=mainmenu';
					//}
					//$this->app->redirect($url,$msg);
					$currentMenutype = PagesAndItemsHelper::getCurrentMenutype();
					$msg = null;
					if($currentMenutype)
					{
						$menutype = $currentMenutype;
					}
					else
					{
						//no menutypes display an message?
						$msg = JText::_('COM_PAGESANDITEMS_NO_MENUS_SELECTED');
						$this->app->enqueueMessage($msg);
						$menutype = '';
						//ms: add if we have in root edit/create menutype JFactory::getApplication()->input->set('subtask', 'new');
					}
					JFactory::getApplication()->input->set('view', 'page');
					JFactory::getApplication()->input->set('layout', 'root');
					JFactory::getApplication()->input->set('menutype', $menutype);
				}
			}*/
		}//end if is admin

		$vName = strtolower(JFactory::getApplication()->input->get('view', 'page'));
		$modelName = array();
		$helperPath = array();
		$helperName = array();
		switch ($vName)
		{
			case 'config':
			case 'config_itemtype':
			case 'config_custom_itemtype_field':
				//$modelName[] = 'Page';
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );

			break;
			
			case 'config_custom_itemtype':
				$modelName[] = 'customitemtype';
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );

			break;


			case 'extension':
			//case 'page':
			case 'item':
			case 'instance_select':
			case 'item_move_select':
			case 'page_move_select':
					$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );
			break;
			
			case 'page':
				require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'item.php');
				require_once(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'menutypes.php');
				if(PagesAndItemsHelper::getIsJoomlaVersion('<','1.6'))
				{
					$helperName[] = 'helper';
				}
				else
				{
					$helperName[] = 'menus';
				}
				$helperPath[] = JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'helpers';
				$this->addModelPath( JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_menus'.DIRECTORY_SEPARATOR.'models' );
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );
				
				
				//TODO RootMenutype
				if($vLayout == 'root' && $sub_task != 'new')
				{
					//$modelName[] = 'RootMenutype';
					$modelName[] = 'Menutype';
				}
				else
				{
					$modelName[] = 'Page';
					
					
				}
			break;
			
			/*
			case 'categorie':
				$modelName[] = 'Categorie';
				$this->addModelPath(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_categories'.DIRECTORY_SEPARATOR.'models'); //???
				$vLayout = 'default';
			break;
			*/
			case 'category':
				$modelName[] = 'Category';
				//$this->addModelPath(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_categories'.DIRECTORY_SEPARATOR.'models'); //???
				$vLayout = 'default';
			
			break;

			case 'element':
				$modelName[] = 'element';
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );
			break;

			case 'managers':
				
				if($isSuperAdmin)
				{
					/*
					//in view managers the com_pagesanditems.sys language is not load
					$lang		= JFactory::getLanguage();
					// Load extension-local file.
					$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, null, false, false)
					||	$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
					*/
				}
				//$modelName[] = 'Base';
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );
			break;
			
			default:
				//$modelName[] = 'Base';
				$vLayout = JFactory::getApplication()->input->get( 'layout', 'default' );
			break;
		}
		$document = JFactory::getDocument();
		$vType = $document->getType();

		// Get/Create the view
		$view =$this->getView( $vName, $vType);


		if(is_array($modelName))
		{
			for($mn = 0; $mn < count($modelName); $mn++)
			{
				//we need $model[$mn] as unique in J1.6
				if($model[$mn] =$this->getModel($modelName[$mn],'PagesAndItemsModel'))
				{
					// Push the model into the view (not as default)
					$view->setModel($model[$mn], false);
				}
			}
		}
		//in views the com_pagesanditems.sys language is not load
		$lang		= JFactory::getLanguage();
		// Load extension-local file.
		$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, null, false, false)
		||	$lang->load('com_pagesanditems.sys', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);





		/*
		add here for releaseEditId and checkin in all views
		//where we must set the old?
		in all links avaible in view page? (not layout==root)
		and not link (or redirect) to the view page or change the pageId
		
			1. pagetree here we can change the pageId
			2. submenu here we go to other views
			3. ?
			
			but if we make an new Browser tab/window and link to another view?????
		
		*/
		/*
		//ms: com_menus.edit.item.id
		$app	= JFactory::getApplication();
		$oldPageId = $app->getUserState("com_pagesanditems.page.edit.item.id"); //.oldPageId");
		if($pageId)
		{
			$context = 'com_pagesanditems.page.edit.item'; //.oldPageId';
			$app->setUserState('com_pagesanditems.page.edit.item.id',$pageId);
			if($vName == 'page' && $vLayout == 'default')
			{
				$context = 'com_menus.edit.item';
				$this->holdEditId($context, $pageId);
				$context = 'com_pagesanditems.page.edit.item';
				$this->holdEditId($context, $pageId);
			}
		}
		if($oldPageId && $pageId != $oldPageId)
		{
			$context = 'com_menus.edit.item';
			$this->releaseEditId($context, $oldPageId);
			
			$model = $this->getModel( 'Item' ,'MenusModel');
			$model->checkin($oldPageId);
		}
		//ms: end com_menus.edit.item.id
		*/
		
		if(is_array($helperPath))
		{
			for($hp = 0; $hp < count($helperPath); $hp++)
			{
				$view->addHelperPath($helperPath[$hp]);
			}
		}
		if(is_array($helperName))
		{
			for($hn = 0; $hn < count($helperPath); $hn++)
			{
				$view->loadHelper($helperName[$hn]);//make sure we have not use helpers/helper.php in pi??
			}
		}
		/*
		if($vName == 'categorie')
		{
			if($modelCategory = $this->getModel('CategoriesCategory','PagesAndItemsModel'))
			//if($modelCategory = $this->getModel('Category','CategoriesModel'))
			{
				// Push the model into the view
				$view->setModel($modelCategory, false);
			}
		}
		*/
		if($vName == 'category')
		{
			if($modelCategory = $this->getModel('CategoriesCategory','PagesAndItemsModel'))
			//if($modelCategory = $this->getModel('Category','CategoriesModel'))
			{
				// Push the model into the view
				$view->setModel($modelCategory, false);
			}
		}
		
		
		$view->loadHelper('pagesanditems');

		if($isAdmin)
		{
			require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagesanditems.php');
			//TODO set the toolbar in each view not PagesAndItemsHelper::addToolbar?
			PagesAndItemsHelper::addToolbar($vName,$vLayout);
			if($vName == 'item')
			{
				//$view->addTemplatePath(JPATH_COMPONENT_SITE.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$vName.DIRECTORY_SEPARATOR.'tmpl');
			}
		}
		else
		{
			$view->addHelperPath(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers');
			if($vName == 'item')
			{
				//$view->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$vName.DIRECTORY_SEPARATOR.'tmpl');
			}
			$view->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.$vName.DIRECTORY_SEPARATOR.'tmpl');
		}

		

		if($isSuperAdmin && $isAdmin)
		{
			// Load the submenu only for super admins
			require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagesanditems.php');
			PagesAndItemsHelper::addSubmenu(JFactory::getApplication()->input->get('view', 'page'));
		}
		elseif($isAdmin)
		{
			
			$config = PagesAndItemsHelper::getConfigAsRegistry();
			if($config->get('enabled_view_category'))
			{
				require_once(JPATH_COMPONENT_ADMINISTRATOR.'/helpers/pagesanditems.php');
				PagesAndItemsHelper::addSubmenuFirst(JFactory::getApplication()->input->get('view', 'page'));
			}
		}

		// Set the layout
		$view->setLayout($vLayout);
		$view->display();

		


	}

	//redirect to view page
	function cancel()
	{

		$pageId = JFactory::getApplication()->input->get('pageId', 0); 
		$categoryId = JFactory::getApplication()->input->get('categoryId', 0); 
		$menutype = JFactory::getApplication()->input->get('menutype', 0); 
		$menutypeString = $menutype ? '&menutype='.$menutype : '';
		$pageIdString = $pageId ? '&pageId='.$pageId : '';
		
		if(!$pageId)
		{
			if($categoryId)
			{
				$this->app->redirect("index.php?option=com_pagesanditems&view=category&categoryId=".$categoryId, JText::_('COM_PAGESANDITEMS_ACTION_CANCELED'));
			}
			else
			{
				$this->app->redirect("index.php?option=com_pagesanditems&view=page&layout=root".$menutypeString, JText::_('COM_PAGESANDITEMS_ACTION_CANCELED'));
			}
			
		}
		else
		{
			$this->app->redirect("index.php?option=com_pagesanditems&view=page&sub_task=edit&pageId=".$pageId.$menutypeString, JText::_('COM_PAGESANDITEMS_ACTION_CANCELED'));
		}
	}

	function ajax_version_checker(){
		//$helper = new PagesAndItemsHelper();
		$message = JText::_('COM_PAGESANDITEMS_VERSION_CHECKER_NOT_AVAILABLE');
		$url = 'http://www.pages-and-items.com/latest_version.php?extension=pagesanditems';
		$file_object = @fopen($url, "r");
		if($file_object == TRUE){
			$version = fread($file_object, 1000);
			$message = JText::_('COM_PAGESANDITEMS_LATEST_VERSION').' = '.$version;
			if(PagesAndItemsHelper::getPagesAndItemsVersion() != $version){
				$message .= '<div><span class="warning">'.JText::_('COM_PAGESANDITEMS_NEWER_VERSION').'</span>.</div>';
				$download_url = 'http://www.pages-and-items.com/extensions/pages-and-items';
				$message .= '<div><a href="'.$download_url.'" target="_blank">'.JText::_('COM_PAGESANDITEMS_DOWNLOAD').'</a> <a href="index.php?option=com_installer&view=update">'.PagesAndItemsHelper::pi_strtolower(JText::_('JLIB_INSTALLER_UPDATE')).'</a></div>';				
			}else{
				$message .= '<div><span style="color: #5F9E30;">'.JText::_('COM_PAGESANDITEMS_IS_LATEST_VERSION').'</span>.</div>';
			}
			fclose($file_object);
		}

		//reset version checker session
		$app = JFactory::getApplication();
		$app->setUserState( "com_pagesanditems.latest_version_message", '' );

		echo $message;
		exit;
	}

	function ajax_update_cit_item(){

		//check token
		JSession::checkToken( 'get' ) or die( '<span style="color: red;">Invalid Token</span>' );

		$itemtype = intval(JFactory::getApplication()->input->get('itemtype','')); 
		$item_id = intval(JFactory::getApplication()->input->get('item_id','')); 
		$itemtype_name = 'custom_'.$itemtype;

		require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_pagesanditems'.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'itemtypehelper.php');
		ExtensionItemtypeHelper::importExtension(null, array('custom','other_item'),true,null,true);
		$dispatcher =JDispatcher::getInstance();

		$dispatcher->trigger('update_content_table_from_custom_itemtype',array($item_id, $itemtype_name));
		//check if the saved item has other_items linked to it, if so, update those
		$dispatcher->trigger('update_other_items_if_needed',array($item_id));

		echo '<span style="color: #5F9E30;">'.JText::_('COM_PAGESANDITEMS_UPDATED').'</span>';
		exit;
	}
	
	function downloaddocument(){		
			
		$db = JFactory::getDBO();	
		$ds = DIRECTORY_SEPARATOR;
		
		$id = JRequest::getVar('id');
		$unpacked = base64_decode($id);
		$unpacked_array = explode('--------', $unpacked);
		$field = $unpacked_array[0];
		$src = $unpacked_array[1];
		
		if(!$field || !$src){			
			exit;
		}
		;
		//get value from field to check if the src is valid
		$query = $db->getQuery(true);
		$query->select('value, field_id');
		$query->from('#__pi_custom_fields_values');
		$query->where('id='.$db->q($field));		
		$rows = $db->setQuery($query, 0, 1);		
		$rows = $db->loadObjectList();
		
		$value = '';	
		foreach($rows as $row){		
			$value = $row->value;	
			$field_id = $row->field_id;
		}
		
		//check if src is in the field value at all
		if(!strpos('bogus'.$value, $src)){
			exit;
		}	
		
		//get dir from field config
		$query = $db->getQuery(true);
		$query->select('params');
		$query->from('#__pi_custom_fields');
		$query->where('id='.$db->q($field_id));		
		$rows = $db->setQuery($query, 0, 1);		
		$rows = $db->loadObjectList();
			
		foreach($rows as $row){		
			$params = $row->params;	
		}
		
		$pos_start = strpos($params, 'document_dir-=-');
		$temp = substr($params, $pos_start+15, strlen($params));		
		$pos_end = strpos($temp, '[;-)# ]');		
		$dir = substr($temp, 0, $pos_end);			
		
		//do download
		if(file_exists(JPATH_ROOT.$ds.$dir.$src)){
			$mime_type = $this->get_mime(JPATH_ROOT.$ds.$dir.$src);
			$file_size = filesize(JPATH_ROOT.$ds.$dir.$src);	
					
			//do download
			ob_end_clean();	
			header("Cache-Control: public, must-revalidate");
			header('Cache-Control: pre-check=0, post-check=0, max-age=0');
			header("Pragma: no-cache");
			header("Expires: 0"); 
			header("Content-Description: File Transfer");
			header("Expires: Sat, 30 Dec 1990 07:07:07 GMT");		
			header("Content-Type: " . (string)$mime_type);
			header("Content-Length: ". (string)$file_size);
			header('Content-Disposition: attachment; filename="'.$src.'"');
			header("Content-Transfer-Encoding: binary\n");	
			@readfile(JPATH_ROOT.$ds.$dir.$src);
		}	
				
		exit;
		
	}
	
	function get_mime($file) {
		if (function_exists("finfo_file")) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($finfo, $file);
			finfo_close($finfo);
			return $mime;
		} else if (function_exists("mime_content_type")) {
			return mime_content_type($file);
		} else if (!stristr(ini_get("disable_functions"), "shell_exec")) {			
			$file = escapeshellarg($file);
			$mime = shell_exec("file -bi " . $file);
			return $mime;
		} else {
			return false;
		}
	}
	
	function ajax_make_menu_alias_unique(){

		$db = JFactory::getDBO();
		$updated = 0;

		$menu_item_id = intval(JRequest::getVar('menu_item_id', ''));

		$menu_alias_ori = '';
		//get aliasses
		$db->setQuery("SELECT id, alias "
			."FROM #__menu "
		);
		$rows = $db->loadObjectList();
		$aliasses = array();
		foreach($rows as $row){
			if($row->id==$menu_item_id){
				$menu_alias_ori = $row->alias;
			}else{
				$aliasses[] = $row->alias;
			}
		}
		$menu_alias = $menu_alias_ori;
		if(in_array($menu_alias, $aliasses)){
			$j = 2;
			while (in_array($menu_alias."-".$j, $aliasses)){
				$j = $j + 1;
			}
			$menu_alias = $menu_alias."-".$j;
		}

		if($menu_alias!=$menu_alias_ori){
			//do update
			$db->setQuery( "UPDATE #__menu SET alias='$menu_alias' WHERE id='$menu_item_id' ");
			$db->query();
			$updated = 1;
		}

		$message = JText::_('COM_PAGESANDITEMS_ALIAS_IS_OK');
		if($updated){
			$message = JText::_('COM_PAGESANDITEMS_UPDATED').': '.$menu_alias;
		}

		echo '<span style="color: ';
		if($updated){
			echo 'red';
		}else{
			echo '#5F9E30';
		}
		echo ';">'.$message.'</span>';
		exit;
	}

}
?>