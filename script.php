<?php
/**
* @version		3.2.0
* @package		PagesAndItems com_pagesanditems
* @copyright	Copyright (C) 2006-2018 Carsten Engel. All rights reserved.
* @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @author		www.pages-and-items.com
*/

/*
This is the special installer addon created by Andrew Eddie and the team of jXtended.
We thank for this cool idea of extending the installation process easily
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class Com_PagesandItemsInstallerScript{

function install($parent) {

$app = JFactory::getApplication();

//give it time
@set_time_limit(240);

//give it memory
$max_memory = trim(@ini_get('memory_limit'));
if($max_memory){
	$end =strtolower($max_memory{strlen($max_memory) - 1});
	switch($end) {
		case 'g':
			$max_memory	*=	1024;
		case 'm':
			$max_memory	*=	1024;
		case 'k':
			$max_memory	*=	1024;
	}
	if ( $max_memory < 16000000 ) {
		@ini_set( 'memory_limit', '16M' );
	}
	if ( $max_memory < 32000000 ) {
		@ini_set( 'memory_limit', '32M' );
	}
	if ( $max_memory < 48000000 ) {
		@ini_set( 'memory_limit', '48M' );
	}
}
ignore_user_abort(true);


$version = new JVersion();
$joomlaVersion = $version->getShortVersion();
$JVersion = '1.5';
if($joomlaVersion >= '1.6')
{
	$JVersion = '1.6';
	if(!isset($parent))
	{
		$parent = $this->parent;
	}
	else
	{
		$parent = $parent->getParent();
	}
}
else
{
	if(!isset($parent))
	{
		$parent = $this->parent;
	}

}

$status = new JObject();

$database = JFactory::getDBO();

/*
*********
* BEGIN *
*********
* table for custom itemtypes
*/

	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_customitemtypes (
  `id` int(11) NOT NULL auto_increment,
  `name` tinytext NOT NULL,
  `read_more` varchar(1) NOT NULL,
  `template_intro` text NOT NULL,
  `template_full` text NOT NULL,
  `editor_id` INT NOT NULL,
  `html_after` text NOT NULL,
  `html_before` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` TINYINT( 3 ) NOT NULL DEFAULT '1',
  `params` TEXT NOT NULL ,
  PRIMARY KEY  (`id`)
)");

	$database->query();	
	
	$arr_columns = $database->getTableColumns("#__pi_customitemtypes",false);
	$columns = array();
	if($arr_columns)
	{
		foreach($arr_columns as $column => $row)
		{
			$columns[] = $column;
		}
	
	
	
	if(!in_array('editor_id', $columns)){
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `editor_id` INT NOT NULL AFTER `template_full`");
		$database->query();
	}
	if(!in_array('html_after', $columns)){
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `html_after` text NOT NULL AFTER `editor_id`");
		$database->query();
	}
	if(!in_array('html_before', $columns)){
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `html_before` text NOT NULL AFTER `html_after`");
		$database->query();
	}
	if(!in_array('state', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `state` TINYINT( 3 ) NOT NULL DEFAULT '1'");
		$database->query();
	}
	if(!in_array('checked_out', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `checked_out` int(10) unsigned NOT NULL default '0'");
		$database->query();
	}

	if(!in_array('checked_out_time', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00'");
		$database->query();
	}
	if(!in_array('params', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_customitemtypes ADD `params` text NOT NULL");
		$database->query();
	}
	}
/*
* table for custom itemtypes
*******
* END *
*******
*/

/*
*********
* BEGIN *
*********
* table for custom itemtype fields
*/
	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_custom_fields (
  `id` int(11) NOT NULL auto_increment,
  `name` tinytext NOT NULL,
  `type_id` int(11) NOT NULL,
  `plugin` tinytext NOT NULL,
  `ordering` int(11) NOT NULL,
  `params` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
)");
	$database->query();

	$arr_columns = $database->getTableColumns("#__pi_custom_fields",false);
	$columns = array();
	if($arr_columns)
	{
		foreach($arr_columns as $column => $row)
		{
			$columns[] = $column;
		}
	if(!in_array('state', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields ADD `state` TINYINT( 3 ) NOT NULL DEFAULT '1'");
		$database->query();
	}
	if(!in_array('checked_out', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields ADD `checked_out` int(10) unsigned NOT NULL default '0'");
		$database->query();
	}

	if(!in_array('checked_out_time', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields ADD `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00'");
		$database->query();
	}
	}
/*
* table for custom itemtype fields
*******
* END *
*******
*/

/*
*********
* BEGIN *
*********
* table for custom itemtype fields-values
*/

	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_custom_fields_values (
  `id` int(11) NOT NULL auto_increment,
  `field_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
)");
	$database->query();

	$arr_columns = $database->getTableColumns("#__pi_custom_fields_values",false);
	$columns = array();
	if($arr_columns)
	{
		foreach($arr_columns as $column => $row)
		{
			$columns[] = $column;
		}
	if(!in_array('state', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields_values ADD `state` TINYINT( 3 ) NOT NULL DEFAULT '1'");
		$database->query();
	}
		if(!in_array('checked_out', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields_values ADD `checked_out` int(10) unsigned NOT NULL default '0'");
		$database->query();
	}

	if(!in_array('checked_out_time', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_custom_fields_values ADD `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00'");
		$database->query();
	}
	}
/*
* table for custom itemtype fields-values
*******
* END *
*******
*/

/*
*********
* BEGIN *
*********
* table for item index
*/

	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_item_index (
  `id` int(11) NOT NULL auto_increment,
  `item_id` int(11) NOT NULL,
  `itemtype` tinytext NOT NULL,
  `show_title` tinyint(4) NOT NULL,
  `checked_out` int(10) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
) ");
$database->query();

	$arr_columns = $database->getTableColumns("#__pi_item_index",false);
	$columns = array();
	if($arr_columns)
	{
		foreach($arr_columns as $column => $row)
		{
			$columns[] = $column;
		}
	if(!in_array('state', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_index ADD `state` TINYINT( 3 ) NOT NULL DEFAULT '1'");
		$database->query();
	}
		if(!in_array('checked_out', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_index ADD `checked_out` int(10) unsigned NOT NULL default '0'");
		$database->query();
	}

	if(!in_array('checked_out_time', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_index ADD `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00'");
		$database->query();
	}
	}
/*
* table for item index
*******
* END *
*******
*/

/*
*********
* BEGIN *
*********
* table for itemtype other_item
*/
	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_item_other_index (
  `id` int(11) NOT NULL auto_increment,
  `item_id` int(11) NOT NULL,
  `other_item_id` int(11) NOT NULL,
  `checked_out` int(10) unsigned NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `state` TINYINT( 3 ) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`)
)");
	$database->query();

	$arr_columns = $database->getTableColumns("#__pi_item_other_index",false);
	$columns = array();
	if($arr_columns)
	{
		foreach($arr_columns as $column => $row)
		{
			$columns[] = $column;
		}
	if(!in_array('state', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_other_index  ADD `state` TINYINT( 3 ) NOT NULL DEFAULT '1'");
		$database->query();
	}
		if(!in_array('checked_out', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_other_index ADD `checked_out` int(10) unsigned NOT NULL default '0'");
		$database->query();
	}

	if(!in_array('checked_out_time', $columns))
	{
		$database->setQuery("ALTER TABLE #__pi_item_other_index ADD `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00'");
		$database->query();
	}
	}
/*
* table for itemtype other_item
*******
* END *
*******
*/

/*
*********
* BEGIN *
*********

*****************
* CONFIGURATION *
*****************

* table for configuration
*/

	$database->setQuery("CREATE TABLE IF NOT EXISTS #__pi_config (
  `id` varchar(255) NOT NULL,
  `config` text NOT NULL,
  PRIMARY KEY  (`id`)
)");
	$database->query();

/*
* table for configuration
*/

	//check if config is empty, if so insert default config
	$database->setQuery("SELECT * FROM #__pi_config WHERE id='pi' ");
	$rows = $database -> loadObjectList();
	$pi_config = '';
	if(count($rows) > 0)
	{
		$pirow = $rows[0];
		$pi_config = $pirow->id;		
	}

	if($pi_config=='')
	{
		/*
		change for menu_types id
		and set all avaible menutypes in pi config
		*/
			$database->setQuery("SELECT title, menutype,id FROM #__menu_types ORDER BY title ASC"  );			
			$menutypes = array();
			foreach($database->loadObjectList() as $menutype)
			{
				$menutypes[] = $menutype->menutype.';'.$menutype->title; //.';'.$menutype->id;
			
			}
			$menus = implode(',',$menutypes);
		
	
		
		//language=en-GB
		$configuration = 'useCheckedOut=false
delete_tables_on_uninstall=true
plugin_system_add_button=false
plugin_system_hidde_button=false
showSlider=-1
enabled_view_category=true
use_pi_frontend_editting=true
menus='.$menus.'
cit=1
item_props_publish=true
item_show_frontpage_option=true
item_props_alias=true
plugin_syntax_cheatcheat=
item_save_redirect=item
make_article_alias_unique=true
create_sef_urls=
sef_url_cat=true
sef_url_id=true
enabled_view_articlemanager=true
default_admin_page=page
sef_url_ext=
item_save_redirect_url=
item_props_hideforsuperadmin=true
item_props_details=true
item_props_title=true
item_props_alias=true
item_props_category=true
item_props_status=true
item_props_access=true
item_props_featured=true
item_props_language=true
item_props_id=true
item_props_articletext=true
item_props_publishingoptions=true
item_props_createdby=true
item_props_createdbyalias=true
item_props_createddate=true
item_props_start=true
item_props_finish=true
item_props_modified_by=true
item_props_modified=true
item_props_revision=true
item_props_hits=true
item_props_articleoptions=true
item_props_show_title=true
item_props_link_titles=true
item_props_show_intro=true
item_props_show_category=true
item_props_link_category=true
item_props_show_parent_category=true
item_props_link_parent_category=true
item_props_show_author=true
item_props_link_author=true
item_props_show_create_date=true
item_props_show_modify_date=true
item_props_show_publish_date=true
item_props_show_item_navigation=true
item_props_show_icons=true
item_props_show_print_icon=true
item_props_show_email_icon=true
item_props_show_vote=true
item_props_show_hits=true
item_props_show_noauth=true
item_props_alternative_readmore=true
item_props_article_layout=true
item_props_metadataoptions=true
item_props_desc=true
item_props_keywords=true
item_props_robots=true
item_props_author=true
item_props_rights=true
item_props_xreference=true
item_props_pioptions=true
item_props_instance=true
item_props_pishowtitle=true
item_props_permissions=true
item_new_show_title=true
item_type_select_frontend=false
inherit_from_parent=true
sections_from_db=true
inherit_from_parent_move=true
child_inherit_from_parent_move=true
child_inherit_from_parent_change=true
make_page_alias_unique=
truncate_item_title=0
page_props_hideforsuperadmin=true
page_props_id=true
page_props_type=true
page_props_title=true
page_props_alias=true
page_props_note=true
page_props_link=true
page_props_published=true
page_props_access=true
page_props_menutype=true
page_props_parent_id=true
page_props_browserNav=true
page_props_home=true
page_props_language=true
page_props_template_style_id=true
page_props_linktype_options=true
page_props_link_title_attri=true
page_props_link_css=true
page_props_link_image=true
page_props_add_title=true
page_props_metadata_options=true
page_props_meta_desc=true
page_props_meta_keys=true
page_props_robots=true
page_props_secure=true
page_props_page_display_options=true
page_props_browser_page=true
page_props_show_page_heading=true
page_props_page_heading=true
page_props_page_class=true
page_props_modules=true
page_props_required_settings=true
page_props_category_options=true
page_props_cat_title=true
page_props_cat_desc=true
page_props_cat_img=true
page_props_cat_levels=true
page_props_cat_empty=true
page_props_cat_no_art_mess=true
page_props_cat_subcat_desc=true
page_props_cat_artincat=true
page_props_cat_subheading=true
page_props_blog_options=true
page_props_blog_leading=true
page_props_blog_intro=true
page_props_blog_cols=true
page_props_blog_links=true
page_props_blog_multicolorder=true
page_props_blog_incsubcat=true
page_props_blog_catorder=false
page_props_blog_artorder=true
page_props_blog_dateorder=true
page_props_blog_pagination=true
page_props_blog_results=true
page_props_article_options=true
page_props_art_title=true
page_props_art_linkedtitles=true
page_props_art_introtext=true
page_props_art_cat=true
page_props_art_catlink=true
page_props_art_parent=true
page_props_art_parentlink=true
page_props_art_author=true
page_props_art_authorlink=true
page_props_art_create=true
page_props_art_modify=true
page_props_art_pub=true
page_props_art_nav=true
page_props_art_vote=true
page_props_art_read=true
page_props_art_readtitle=true
page_props_art_icons=true
page_props_art_print=true
page_props_art_email=true
page_props_art_hits=true
page_props_art_unauthorised=true
page_props_integration_options=true
page_props_int_feed=true
page_props_int_each=true
page_new_publish_category=1
page_new_publish_menu=true
page_new_access_menu=0
page_new_access_category=0
itemtypes=
version_checker=true
page_trash_cat=
page_trash_items=
page_delete_cat=
page_delete_items=
item_props_tags=true
item_props_revision=true
item_props_keyref=true
item_props_show_tags=true
item_props_show_position=true
item_props_show_poslinks=true
';


		//make sure no empty lines
		$configParams = array();
		$configurationParams = explode( "\n", $configuration);
		for($n = 0; $n < count($configurationParams); $n++)
		{
			$var = '';
			$temp = explode('=',$configurationParams[$n]);
			$var = trim($temp[0]);
			$value = '';
			if(count($temp)==2){
				$value = trim($temp[1]);
				if($value=='false'){
					$value = false;
				}
				if($value=='true'){
					$value = true;
				}
			}
			if($var != '')
			{
				$configParams[] = $var.'='.$value;
			}
		}
		
		
		$configuration = addslashes(implode( "\n", $configParams));
		$database->setQuery( "INSERT INTO #__pi_config SET id='pi', config='$configuration'");
		$database->query();
	}
	else
	{

		$config_needs_updating = 0;
		$updated_config = $pirow->config;		
		
		if(strpos($pirow->config, 'plugin_system_add_button=') === false){
			$config_needs_updating = 1;
			$updated_config .= '
plugin_system_add_button=false
';
		}
		if(strpos($pirow->config, 'plugin_system_hidde_button=') === false){
			$config_needs_updating = 1;
			$updated_config .= '
plugin_system_hidde_button=false
';
		}
		
		
		if(strpos($pirow->config, 'useCheckedOut=') === false){
			//added in version 2.1.0
			$config_needs_updating = 1;
			$updated_config .= '
useCheckedOut=false
';
		}
		
		
		if(strpos($pirow->config, 'showSlider=') === false){
			//added in version 2.1.0
			$config_needs_updating = 1;
			$updated_config .= '
showSlider=-1
';
		}
		if(strpos($pirow->config, 'delete_tables_on_uninstall=') === false){
			//added in version 2.1.0
			$config_needs_updating = 1;
			$updated_config .= '
delete_tables_on_uninstall=true
';
		}
		
		if(strpos($pirow->config, 'enabled_view_category=') === false){
			//added in version 2.1.0
			$config_needs_updating = 1;
			$updated_config .= '
enabled_view_category=true
';
		}
		if(strpos($pirow->config, 'enabled_view_articlemanager=') === false){
			//added in version 2.2.0
			$config_needs_updating = 1;
			$updated_config .= '
enabled_view_articlemanager=true
default_admin_page=page
';
		}		
		if(strpos($pirow->config, 'version_checker=') === false){
			//added in version 2.0.0
			$config_needs_updating = 1;
			$updated_config .= '
version_checker=true
item_props_details=true
item_props_title=true
item_props_alias=true
item_props_category=true
item_props_status=true
item_props_access=true
item_props_featured=true
item_props_language=true
item_props_id=true
item_props_articletext=true
item_props_publishingoptions=true
item_props_createdby=true
item_props_createdbyalias=true
item_props_createddate=true
item_props_start=true
item_props_finish=true
item_props_modified_by=true
item_props_modified=true
item_props_revision=true
item_props_hits=true
item_props_articleoptions=true
item_props_show_title=true
item_props_link_titles=true
item_props_show_intro=true
item_props_show_category=true
item_props_link_category=true
item_props_show_parent_category=true
item_props_link_parent_category=true
item_props_show_author=true
item_props_link_author=true
item_props_show_create_date=true
item_props_show_modify_date=true
item_props_show_publish_date=true
item_props_show_item_navigation=true
item_props_show_icons=true
item_props_show_print_icon=true
item_props_show_email_icon=true
item_props_show_vote=true
item_props_show_hits=true
item_props_show_noauth=true
item_props_alternative_readmore=true
item_props_article_layout=true
item_props_metadataoptions=true
item_props_desc=true
item_props_keywords=true
item_props_robots=true
item_props_author=true
item_props_rights=true
item_props_xreference=true
item_props_pioptions=true
item_props_instance=true
item_props_pishowtitle=true
item_props_permissions=true
page_props_id=true
page_props_type=true
page_props_title=true
page_props_alias=true
page_props_note=true
page_props_link=true
page_props_published=true
page_props_access=true
page_props_menutype=true
page_props_parent_id=true
page_props_browserNav=true
page_props_home=true
page_props_language=true
page_props_template_style_id=true
page_props_linktype_options=true
page_props_link_title_attri=true
page_props_link_css=true
page_props_link_image=true
page_props_add_title=true
page_props_metadata_options=true
page_props_meta_desc=true
page_props_meta_keys=true
page_props_robots=true
page_props_secure=true
page_props_page_display_options=true
page_props_browser_page=true
page_props_show_page_heading=true
page_props_page_heading=true
page_props_page_class=true
page_props_modules=true
page_props_required_settings=true
page_props_category_options=true
page_props_cat_title=true
page_props_cat_desc=true
page_props_cat_img=true
page_props_cat_levels=true
page_props_cat_empty=true
page_props_cat_no_art_mess=true
page_props_cat_subcat_desc=true
page_props_cat_artincat=true
page_props_cat_subheading=true
page_props_blog_options=true
page_props_blog_leading=true
page_props_blog_intro=true
page_props_blog_cols=true
page_props_blog_links=true
page_props_blog_multicolorder=true
page_props_blog_incsubcat=true
page_props_blog_catorder=false
page_props_blog_artorder=true
page_props_blog_dateorder=true
page_props_blog_pagination=true
page_props_blog_results=true
page_props_article_options=true
page_props_art_title=true
page_props_art_linkedtitles=true
page_props_art_introtext=true
page_props_art_cat=true
page_props_art_catlink=true
page_props_art_parent=true
page_props_art_parentlink=true
page_props_art_author=true
page_props_art_authorlink=true
page_props_art_create=true
page_props_art_modify=true
page_props_art_pub=true
page_props_art_nav=true
page_props_art_vote=true
page_props_art_read=true
page_props_art_readtitle=true
page_props_art_icons=true
page_props_art_print=true
page_props_art_email=true
page_props_art_hits=true
page_props_art_unauthorised=true
page_props_integration_options=true
page_props_int_feed=true
page_props_int_each=true
page_trash_cat=
page_trash_items=
page_delete_cat=
page_delete_items=
';
}
	
	
		if(strpos($pirow->config, 'enabled_view_articlemanager=') === false){
			//added in version 3.0.0
			$config_needs_updating = 1;
			$updated_config .= '
item_props_tags=true
item_props_revision=true
item_props_keyref=true
item_props_show_tags=true
item_props_show_position=true
item_props_show_poslinks=true
';
		}	

		if($config_needs_updating){
			//make sure no empty lines
			$configParams = array();
			$configurationParams = explode( "\n", $updated_config);
			for($n = 0; $n < count($configurationParams); $n++)
			{
				$var = '';
				$temp = explode('=',$configurationParams[$n]);
				$var = trim($temp[0]);
				$value = '';
				if(count($temp)==2){
					$value = trim($temp[1]);
					if($value=='false'){
						$value = false;
					}
					if($value=='true'){
						$value = true;
					}
				}
				if($var != '')
				{
					$configParams[] = $var.'='.$value;
				}
			}
			$updated_config = implode( "\n", $configParams);
			$database->setQuery( "UPDATE #__pi_config SET config='$updated_config' WHERE id='pi' ");
			$database->query();
		}


	}

/*
*****************
* CONFIGURATION *
*****************
*******
* END *
*******
*/

	if($joomlaVersion < '1.6')
	{
		//do icon
		$icon_path = 'components';
		$database->setQuery("UPDATE #__components SET admin_menu_img='$icon_path/com_pagesanditems/images/icon.gif' WHERE link='option=com_pagesanditems'");
		$database->query();
	}

/*
*****************
* PI EXTENSIONS *
*****************
*/
$componentPath = $parent->getPath('extension_administrator');
require_once( $componentPath.DIRECTORY_SEPARATOR.'install'.DIRECTORY_SEPARATOR.'install.piextensions.php' );
$piExtensions = new piExtensions();

// table extensions
$piExtensions->createTable();
$piExtensions->installLanguage($parent);

// install extensions
$status->extensions = $piExtensions->installExtensions($parent);

/*
*********************
* END PI EXTENSIONS *
*********************
*/

/***********************************************************************************************
* ---------------------------------------------------------------------------------------------
* PLUGIN INSTALLATION SECTION
* ---------------------------------------------------------------------------------------------
***********************************************************************************************/
require_once( $componentPath.DIRECTORY_SEPARATOR.'install'.DIRECTORY_SEPARATOR.'install.plugins.php' );
$install_plugins = new installPlugins();

$status->plugins = array();
$status->plugins = $install_plugins->installUseXML($parent);

/***********************************************************************************************
* ---------------------------------------------------------------------------------------------
* PLUGIN INSTALLATION SECTION
* ---------------------------------------------------------------------------------------------
***********************************************************************************************/

/***********************************************************************************************
* ---------------------------------------------------------------------------------------------
* DELETE UNUSED FILES SECTION
* ---------------------------------------------------------------------------------------------
***********************************************************************************************/
//jimport('joomla.filesystem.file');

//clean up deprecated files from previous install
$ds = DIRECTORY_SEPARATOR;
$deprecated_files = array();
$deprecated_files[] = JPATH_ROOT.$ds.'administrator'.$ds.'components'.$ds.'com_pagesanditems'.$ds.'models'.$ds.'fields'.$ds.'menutype.php';
$deprecated_files[] = JPATH_ROOT.$ds.'administrator'.$ds.'components'.$ds.'com_pagesanditems'.$ds.'models'.$ds.'fields'.$ds.'menuparent.php';
$deprecated_files[] = JPATH_ROOT.$ds.'administrator'.$ds.'components'.$ds.'com_pagesanditems'.$ds.'install.php';
$deprecated_files[] = JPATH_ROOT.$ds.'administrator'.$ds.'components'.$ds.'com_pagesanditems'.$ds.'uninstall.php';
$latest_version_css = 3;
for($n = 1; $n < $latest_version_css; $n++){			
	$deprecated_files[] = JPATH_ROOT.$ds.'administrator'.$ds.'components'.$ds.'com_pagesanditems'.$ds.'css'.$ds.'pagesanditems'.$n.'.css';
}		
foreach($deprecated_files as $deprecated_file){
	if(file_exists($deprecated_file)){
		JFile::delete($deprecated_file);
	}
}

//reset version checker session var		
$app->setUserState( "com_pagesanditems.latest_version_message", '' );


/***********************************************************************************************
* ---------------------------------------------------------------------------------------------
* OUTPUT TO SCREEN
* ---------------------------------------------------------------------------------------------
***********************************************************************************************/
$rows = 0;
?>

<div style="width: 500px; text-align: left;">
	<h2>Pages and Items</h2>
	<p>
		Thank you for using the Pages-and-Items framework for Joomla.
	</p>
	<p>
		Check <a href="http://www.pages-and-items.com" target="_blank">www.pages-and-items.com</a> for:
		<ul>
			<li><a href="http://www.pages-and-items.com/extensions/pages-and-items" target="_blank">updates</a></li>
			<li><a href="http://www.pages-and-items.com/extensions/pages-and-items/faqs" target="_blank">FAQs</a></li>
			<li><a href="http://www.pages-and-items.com/forum/8-pages-and-items" target="_blank">forum</a></li>			
			<li><a href="http://www.pages-and-items.com/my-account/email-update-notifications" target="_blank">email notification service for updates</a></li>
			<li><a href="http://www.pages-and-items.com" target="_blank">RSS feed notification of updates</a></li>
		</ul>
	</p>
	<p>
		Component Pages-and-Items is only fully functional with the content and system plugin installed and enabled.
	</p>
	<p>
		Follow us on <a href="http://www.twitter.com/PagesAndItems" target="_blank">Twitter</a> (update notifications).
	</p>
</div>
<?php

if(is_array($status->plugins))
{
?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="title" colspan="3"><?php echo JText::_('Extension'); ?></th>
			<th width="30%" colspan="1"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="4"></td>
		</tr>
	</tfoot>
	<tbody>
		<tr class="row0">
			<td class="key" colspan="3"><strong><?php echo 'Pages and Items '?></strong></td>
			<td><strong><?php echo JText::_('Installed'); ?></strong></td>
		</tr>
<?php if (count($status->plugins)) : ?>
		<tr>
			<th><?php echo JText::_('Plugin'); ?></th>
			<th><?php echo JText::_('Group'); ?></th>
			<th><?php echo JText::_('Name'); ?></th>
			<th></th>
		</tr>
	<?php foreach ($status->plugins as $plugin) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?>">
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['element']); ?></td>
			<td><strong><?php echo $plugin['installed'] ? JText::_('Installed') : JText::_('Not').' '.JText::_('Installed'); ?></strong></td>
		</tr>
	<?php endforeach;
endif; ?>
	</tbody>
</table>
<?php
}


if(is_array($status->extensions))
{
$rows = 0;
?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="title" colspan="3"><?php echo JText::_('PiExtension'); ?></th>
			<th width="30%" colspan="1"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="4"></td>
		</tr>
	</tfoot>
	<tbody>
<?php if (count($status->extensions)) : ?>
		<tr>
			<th><?php echo JText::_('Type'); ?></th>
			<th><?php echo JText::_('Folder'); ?></th>
			<th><?php echo JText::_('Name'); ?></th>
			<th></th>
		</tr>
	<?php foreach ($status->extensions as $plugin) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?>">
			<td class="key"><?php echo ucfirst($plugin['type']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['folder']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td><strong><?php echo $plugin['installed'] ? JText::_('Installed') : JText::_('Not').' '.JText::_('Installed'); ?></strong></td>
		</tr>
	<?php endforeach;
endif; ?>
	</tbody>
</table>
<?php
}
}
function update($parent) {
		return $this->install($parent);
	}

function uninstall(){
	$database = JFactory::getDBO();

	//delete content plugin
	$plugin_php = JPATH_PLUGINS.DIRECTORY_SEPARATOR.'content'.DIRECTORY_SEPARATOR.'pagesanditems'.DIRECTORY_SEPARATOR.'pagesanditems.php';
	$plugin_xml = JPATH_PLUGINS.DIRECTORY_SEPARATOR.'content'.DIRECTORY_SEPARATOR.'pagesanditems'.DIRECTORY_SEPARATOR.'pagesanditems.xml';
	$content_plugin_success = 0;
	if(file_exists($plugin_php) && file_exists($plugin_xml)){
		$content_plugin_success = JFile::delete($plugin_php);
		JFile::delete($plugin_xml);
	}
	if($content_plugin_success){
		echo '<p style="color: #5F9E30;">content plugin succesfully uninstalled</p>';
	}else{
		echo '<p style="color: red;">could not uninstall content plugin</p>';
	}
	$database->setQuery("DELETE FROM #__extensions WHERE type='plugin' AND folder='content' AND element='pagesanditems' LIMIT 1");
	$database->query();

	//delete system plugin
	$plugin_php = JPATH_PLUGINS.DIRECTORY_SEPARATOR.'system'.DIRECTORY_SEPARATOR.'pagesanditems'.DIRECTORY_SEPARATOR.'pagesanditems.php';
	$plugin_xml = JPATH_PLUGINS.DIRECTORY_SEPARATOR.'system'.DIRECTORY_SEPARATOR.'pagesanditems'.DIRECTORY_SEPARATOR.'pagesanditems.xml';
	$content_plugin_success = 0;
	if(file_exists($plugin_php) && file_exists($plugin_xml)){
		$content_plugin_success = JFile::delete($plugin_php);
		JFile::delete($plugin_xml);
	}
	if($content_plugin_success){
		echo '<p style="color: #5F9E30;">system plugin succesfully uninstalled</p>';
	}else{
		echo '<p style="color: red;">could not uninstall system plugin</p>';
	}
	$database->setQuery("DELETE FROM #__extensions WHERE type='plugin' AND folder='system' AND element='pagesanditems' LIMIT 1");
	$database->query();

	//at uninstall empty the pi extensions table
	$database->setQuery("TRUNCATE TABLE #__pi_extensions ");
	$database->query();

	//uninstall all pi core language files
	$lang_array = array(array('en','en-GB','English'),array('ar','ar-EG','Arabic'),array('bg','bg-BG','Bulgarian'),array('zh-CN','zh-CN','Chinese (Simplified)'),array('zh-TW','zh-TW','Chinese (Traditional)'),array('hr','hr-HR','Croatian'),array('cs','cs-CZ','Czech'),array('da','da-DK','Danish'),array('nl','nl-NL','Dutch'),array('fi','fi-FI','Finnish'),array('fr','fr-FR','French'),array('de','de-DE','German'),array('el','el-GR','Greek'),array('hi','hi-IN','Hindi'),array('it','it-IT','Italian'),array('ja','ja-JP','Japanese'),array('ko','ko-KR','Korean'),array('no','no-NO','Norwegian'),array('pl','pl-PL','Polish'),array('pt','pt-PT','Portuguese'),array('ro','ro-RO','Romanian'),array('ru','ru-RU','Russian'),array('es','es-ES','Spanish'),array('sv','sv-SE','Swedish'),array('ca','ca-ES','Catalan'),array('tl','tl-PH','Filipino'),array('iw','he-IL','Hebrew'),array('id','id-ID','Indonesian'),array('lv','lv-LV','Latvian'),array('lt','lt-LT','Lithuanian'),array('sr','sr-RS','Serbian'),array('sk','sk-SK','Slovak'),array('sl','sl-SI','Slovenian'),array('uk','uk-UA','Ukrainian'),array('vi','vi-VN','Vietnamese'),array('sq','sq-AL','Albanian'),array('et','et-EE','Estonian'),array('gl','gl-ES','Galician'),array('hu','hu-HU','Hungarian'),array('mt','mt-MT','Maltese'),array('th','th-TH','Thai'),array('tr','tr-TR','Turkish'),array('fa','fa-IR','Persian'),array('af','af-ZA','Afrikaans'),array('ms','ms-MY','Malay'),array('sw','sw-TZ','Swahili'),array('ga','ga-IE','Irish'),array('cy','cy-GB','Welsh'),array('be','be-BE','Belarusian'),array('is','is-IS','Icelandic'),array('mk','mk-MK','Macedonian'),array('yi','yi-IL','Yiddish'),array('ht','ht-NG','Haitian Creole'),array('pt','pt-BR','Portuguese Brasil'));
	foreach($lang_array as $language){
		$lang = $language[1];
		if(file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$lang.'.com_pagesanditems.ini')){
			JFile::delete(JPATH_ROOT.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$lang.'.com_pagesanditems.ini');
		}
		if(file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$lang.'.com_pagesanditems.sys.ini')){
			JFile::delete(JPATH_ROOT.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'language'.DIRECTORY_SEPARATOR.$lang.DIRECTORY_SEPARATOR.$lang.'.com_pagesanditems.sys.ini');
		}

	}
	//TODO check in config
//check if config is empty, if so insert default config
	$database->setQuery("SELECT * FROM #__pi_config WHERE id='pi' ");
	$rows = $database -> loadObjectList();
	$pi_config = '';
	$delete_tables = true;
	if(count($rows) > 0)
	{
		$pirow = $rows[0];
		$pi_config = $pirow->id;
		//$pi_config = $pirow->config; ??
		$configParams = array();
		$configurationParams = explode( "\n", $pirow->config);
		for($n = 0; $n < count($configurationParams); $n++)
		{
			$var = '';
			$temp = explode('=',$configurationParams[$n]);
			$var = trim($temp[0]);
			$value = '';
			if(count($temp)==2){
				$value = trim($temp[1]);
				if($value=='false'){
					$value = false;
				}
				if($value=='true'){
					$value = true;
				}
			}
			if($var != '')
			{
				if($var == 'delete_tables_on_uninstall' && !$value)
				{
					$delete_tables = false;
				}
				$configParams[] = $var.'='.$value;
			}
		}
	}


	//delete tables
	if($delete_tables)
	{
		$tables_to_drop = array();
		$tables_to_drop[] = '#__pi_config';
		$tables_to_drop[] = '#__pi_customitemtypes';
		$tables_to_drop[] = '#__pi_custom_fields';
		$tables_to_drop[] = '#__pi_custom_fields_values';
		$tables_to_drop[] = '#__pi_extensions';
		$tables_to_drop[] = '#__pi_item_index';
		$tables_to_drop[] = '#__pi_item_other_index';
		$tables_to_drop[] = '#__pi_subitem_googlevideo';
		$tables_to_drop[] = '#__pi_subitem_image_gallery';
		$tables_to_drop[] = '#__pi_subitem_image_gallery_images';
		$tables_to_drop[] = '#__pi_subitem_moduleposition';
		$tables_to_drop[] = '#__pi_subitem_php';
		$tables_to_drop[] = '#__pi_subitem_youtube';	
		for($n = 0; $n < count($tables_to_drop); $n++){
			$query = $database->getQuery(true);
			$query = 'DROP TABLE IF EXISTS '.$database->quoteName($tables_to_drop[$n]);
			$database->setQuery((string)$query);
			$database->query();
		}
	}

	

}

}
?>