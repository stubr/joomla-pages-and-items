# Pages and Items #
This repo contains a modified and bugfixed version of the current Pages and Items-Extension for Joomla (v3.1.1) (www.pagesanditems.com).
Please note, that there is a new versioning in place.

## Installation ##
Download the repository (https://bitbucket.org/stubr/joomla-pages-and-items/downloads/), and install the ZIP-File throught the install mananger of Joomla. 

## Copyright ##
Carsten Engels (www.pages-and-items.com)
Sturm und Br�m (www.sturmundbraem.ch)